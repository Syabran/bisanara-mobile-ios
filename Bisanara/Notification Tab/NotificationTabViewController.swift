//
//  NotificationTabViewController.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 21/10/21.
//

import UIKit

class NotificationTabViewController: UIViewController {

    @IBOutlet weak var notificationTableView: UITableView!
    
    var notifications: [Notification] = [
        .init(id: "1", title: "Report Received", detail: "The report you send for product Wallet are on progress. Click for more detail", date: "7 November 2021"),
        .init(id: "2", title: "Feedback Sent", detail: "Your feedback for product Wallet are sent, Thank you. Click for more detail", date: "1 November 2021")
    ]
    
    var blue = UIColor(red: 73.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customNavBar()
        registerCells()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationItem.largeTitleDisplayMode = .always
        hideBackBar()
    }
    
    private func registerCells() {
        notificationTableView.register(UINib(nibName: NotificationListTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: NotificationListTableViewCell.identifier)
    }
    
    func customNavBar() {
        title = "Notification"
        navigationController?.navigationBar.barTintColor = blue // nav bar background color
        
        navigationController?.navigationBar.largeTitleTextAttributes = [.foregroundColor: UIColor.white] // large title color
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white] // normal title color
        navigationController?.navigationBar.tintColor = .white // item tint color
        
        self.view.backgroundColor = blue
        
    }
    private func hideBackBar() {
        self.navigationItem.hidesBackButton = true
    }
    

}
extension NotificationTabViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NotificationListTableViewCell.identifier, for: indexPath) as! NotificationListTableViewCell
        cell.setup(notify: notifications[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // 
    }
}
