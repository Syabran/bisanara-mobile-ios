//
//  NotificationListTableViewCell.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 08/11/21.
//

import UIKit

class NotificationListTableViewCell: UITableViewCell {
    
    static let identifier = String(describing: NotificationListTableViewCell.self)
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(notify: Notification) {
        titleLabel.text = notify.title ?? ""
        detailLabel.text = notify.detail ?? ""
    }
    
}
