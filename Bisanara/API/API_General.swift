//
//  API_General.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 12/05/22.
//

import Foundation
import SwiftyJSON
import Alamofire

extension APIBisanara {
    // GET DATA GENERAL
    
    func getFAQ(callback: @escaping(_ result: Bool, _ jsonResult: JSON?)-> Void) {
        self.access(method: .get, url: "api/faq", params: [:]) { (status, response) in
            if response == nil {
                print("get faq failed")
                callback(false, response)
            } else {
                callback(true, response)
            }
        }
    }
    
}
