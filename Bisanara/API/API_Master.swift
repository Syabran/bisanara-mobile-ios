//
//  API_Master.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 14/02/22.
//

import Foundation
import SwiftyJSON
import Alamofire

extension APIBisanara {
    // GET DATA MASTER
    
    func getSocialMedia(callback: @escaping(_ result: Bool, _ jsonResult: JSON?)-> Void) {
        self.access(method: .get, url: "api/social_media", params: [:]) { (status, response) in
            if response == nil {
                print("get social media failed")
                callback(false, response)
            } else {
                callback(true, response)
            }
        }
    }
    
    func getCategories(idCategory: String?, callback: @escaping(_ result: Bool, _ jsonResult: JSON?) -> Void) {
        self.access(method: .get, url: "api/master/category", params: ["id":idCategory!]) { (status, response) in
            if response == nil {
                print("get master categories failed")
                callback(false, response)
            } else {
                callback(true, response)
            }
        }
    }
    
    func getCategoriesReport(callback: @escaping(_ result: Bool, _ jsonResult: JSON?) -> Void) {
        self.access(method: .get, url: "api/master/category-report", params: [:]) { (status, response) in
            if response == nil {
                print("get master categories report failed")
                callback(false, response)
            } else {
                callback(true, response)
            }
        }
    }
    
}

