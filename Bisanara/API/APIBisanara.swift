//
//  APIBisanara.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 22/12/21.
//

import Alamofire
import SwiftyJSON

class APIBisanara {
    
    var token: String?
    var dispatchGroup = DispatchGroup()
//    var baseURL = "https://api.bisanara.com/"
    var baseURL = "http://10.240.200.98/api-bisanara/"
    var Headers: HTTPHeaders = ["appkey": "BLD9rZ80N1SknMDLY0b177uBzbBe6JSW7iJCKio3:mobile",
                                "Authorization": UserDefaults.standard.value(forKey: "authorization") as? String ?? "",
                                "token": UserDefaults.standard.value(forKey: "token") as? String ?? "",
                                "type": UserDefaults.standard.value(forKey: "type") as? String ?? ""]
    
    static let shared =  APIBisanara()
    
    func setToken(authorization: String?, token: String?, type: String?) {
        self.token = token
        self.Headers["Authorization"] = authorization
        self.Headers["token"] = token
        self.Headers["type"] = type
    }
    
    // PARENT REQUEST
    func access(method: HTTPMethod, url: String?, params: [String:Any], callback: @escaping(_ result: Bool, _ jsonResult: JSON?) -> Void) {
        
        let completeURL = self.baseURL + url!
        
        AF.request(completeURL, method: method, parameters: params, encoding: URLEncoding.queryString, headers: Headers).validate(statusCode: 200..<600).responseJSON() { (response) in
//            print(completeURL)
            print("Alamofire Online Login")
            
            switch response.result {
            case .success(let value):
                let reJson = JSON(value)
                print("Login JSON: \(reJson) \n \(self.Headers)")
                callback(true, reJson)
            case .failure(let error):
                print("AF token login error: \(error) \n \(completeURL), \(self.Headers)")
                let reJson = JSON(error)
                callback(false, reJson)
            }
        }    }
    
    // TOKEN GUEST
    func getGuestData(callback: @escaping(_ result: Bool, _ jsonResult: JSON?) -> Void) {
        self.access(method: .get, url: "api/token", params: [:]) { (result, response) in
            if response == nil {
                print("get guest token failure")
                callback(false, response)
            } else {
                callback(true, response)
            }
        }
    }
    
    // LOGIN
    func login(username: String, password: String, callback: @escaping(_ result:Bool, _ jsonResult: JSON?) -> Void) {
        
        self.access(method: .post, url: "api/login", params: ["username" : username, "password": password]) { (result, response) in
            
//            debugPrint(response!)
            
            if response == nil {
                print("Login Failure")
                callback(false, response)
            } else {
//                print("Login Success")
                callback(true, response)
            }
        }
    }
    
    // LOGOUT
    func logout(callback: @escaping(_ result: Bool, _ jsonResult: JSON?) -> Void) {
        
        self.access(method: .get, url: "api/logout", params: [:]) { (result, response) in
            
            if response == nil {
                print("Logout Failure")
                callback(false, response)
            } else {
                callback(true, response)
            }
        }
    }
    
    // LIST SHOP
    func getListShop(page: Int, limit: Int, keyword: String, callback: @escaping(_ status: Bool, _ jsonResult: JSON?) -> Void){
        DispatchQueue.global().asyncAfter(deadline: .now() +  1, execute: {
            
            self.access(method: .get, url: "api/shop", params: ["page": page, "limit": limit, "keyword": keyword]) { (status, response) in
                
                if response == nil {
                    print("Get List Shop Data Failure")
                    callback(false, response)
                } else {
                    callback(true, response)
                }
            }
        })
    }
    
    // GET DETAIL SHOP
    func getDetailShop(slug: String, callback: @escaping(_ status: Bool, _ jsonResult: JSON?) -> Void) {
        self.access(method: .get, url: "api/shop/\(slug)", params: [:]) { (status, response) in
            
            if response == nil {
                print("Shop detail failure")
                callback(false, response)
            } else {
                callback(true, response)
            }
        }
    }
    
    // GET LIST PRODUCT BY SHOP
    func getListProductByShop(shopSlug: String, callback: @escaping(_ status: Bool, _ jsonResult: JSON?) -> Void){
        DispatchQueue.global().asyncAfter(deadline: .now() + 1, execute: {
            
            self.access(method: .get, url: "api/shop/\(shopSlug)/products", params: [:]) { (status, response) in
                
                if response == nil {
                    print("product by shop is Failure")
                    callback(false, response)
                } else {
                    callback(true, response)
                }
            }
        })
    }

    // LIST PRODUCT
    func getListProduct(limit: Int, page: Int, keyword: String, sort: String, category: Int, callback: @escaping(_ status: Bool, _ jsonResult: JSON?) -> Void){
        DispatchQueue.global().asyncAfter(deadline: .now() + 1, execute: {
            
            self.access(method: .get, url: "api/product", params: ["limit": limit, "page": page, "keyword": keyword, "sort": sort, "category": category]) { (status, response) in
                
                if response == nil {
                    print("Get list product Data Failure")
                    callback(false, response)
                } else {
                    callback(true, response)
                }
            }
        })
        
    }
    
    // GET DETAIL PRODUCT
    func getDetailProduct(productSlug: String, callback: @escaping(_ status: Bool, _ jsonResult: JSON?) -> Void) {
        self.access(method: .get, url: "api/product/\(productSlug)", params: [:]) { (status, response) in
            
            if response == nil {
                print("Product detail failure")
                callback(false, response)
            } else {
                callback(true, response)
            }
        }
    }
    
    // LIST MOST VIEWS
    func getListMostViewProduct(keyword: String, sort: String, category: Int, callback: @escaping(_ status: Bool, _ jsonResult: JSON?) -> Void){
        DispatchQueue.global().asyncAfter(deadline: .now() + 1, execute: {
            
            self.access(method: .get, url: "api/product/most", params: ["keyword": keyword, "sort": sort, "category": category]) { (status, response) in
                
                if response == nil {
                    print("Get list most views Failure")
                    callback(false, response)
                } else {
                    callback(true, response)
                }
            }
        })
    }

    // GET LIST SHOP OWNER
    func getListShopOwner(callback: @escaping(_ status: Bool, _ jsonResult: JSON?) -> Void) {
        self.access(method: .get, url: "api/manage/shop", params: [:]) { (status, response) in
            
            if response == nil {
                print("Get List Shop Owner Failure")
                callback(false, response)
            } else {
                callback(true, response)
            }
            
        }
    }
    
    // GET REVIEW BY PRODUCT
    func getReviewByProduct(productSlug: String, page: Int, callback: @escaping(_ status: Bool, _ jsonResult: JSON?) -> Void) {
        DispatchQueue.global().asyncAfter(deadline: .now() + 1, execute: {
            
            self.access(method: .get, url: "api/product/reviews/\(productSlug)", params: ["page": page]) { (status, response) in
                
                if response == nil {
                    print("Get list reviews Data Failure")
                    callback(false, response)
                } else {
                    callback(true, response)
                }
            }
        })
    }
    
    // CREATE REVIEW
    func createReviewProduct(productId:String, nameReviewer:String?, mailReviewer: String?, userId: String?, isAnonymous:Bool, comments:String, media: [UIImage]?, callback: @escaping(_ jsonResult: JSON?) -> Void) {
        let headers: HTTPHeaders = [
            "appkey": "BLD9rZ80N1SknMDLY0b177uBzbBe6JSW7iJCKio3:mobile",
            "Authorization": UserDefaults.standard.value(forKey: "authorization") as? String ?? "" ,
            "token": UserDefaults.standard.value(forKey: "token") as? String ?? "" ,
            "type": UserDefaults.standard.value(forKey: "type") as? String ?? "guest" ,
            "Content-Type": "application/json;charset=utf-8"]
        
        let body = ["product_id":productId, "reviewer":nameReviewer, "mail":mailReviewer, "user_id":userId, "is_anonymous":String(isAnonymous), "comment":comments]
        
        AF.upload(multipartFormData: { multiPart in
            for (key, keyValue) in body {
                if let keyData = keyValue?.data(using: .utf8) {
                    multiPart.append(keyData, withName: key)
                }
            }
            
            if media != nil {
                print("isi mediaa: \(String(describing: media))")
                
                media?.indices.forEach {
                    multiPart.append((media?[$0].jpegData(compressionQuality: 0.5)!)!, withName: "files[\($0)]", fileName: "media\($0).jpeg", mimeType: "image/jpeg/png")
                    
                    print("=== \(String(describing: media?[$0]))")
                }
            }
        }, to: baseURL + "api/product/reviews", method: .post, headers: headers).responseJSON() { response in
            
            switch response.result {
            case .success(let result):
                let apiDict = response.value as? [String:Any]
                print("created review: \(apiDict!)")
                let reJson = JSON(result)
                callback(reJson)
                
            case .failure(let error):
                print("error: create review \(error)")
                let err = JSON(error)
                callback(err)
            }
        }
    }
    
    // CREATE REPORT
    func createReportProduct(productId:String, name:String?, mail: String, reportType: String, comments:String, media: [UIImage]?, callback: @escaping(_ jsonResult: JSON?) -> Void) {
        let headers: HTTPHeaders = [
            "appkey": "BLD9rZ80N1SknMDLY0b177uBzbBe6JSW7iJCKio3:mobile",
            "Authorization": UserDefaults.standard.value(forKey: "authorization") as? String ?? "" ,
            "token": UserDefaults.standard.value(forKey: "token") as? String ?? "" ,
            "type": UserDefaults.standard.value(forKey: "typeLogin") as? String ?? "guest" ,
            "Content-Type": "application/json;charset=utf-8"]
        
        let body = ["product_id":productId, "name":name, "mail":mail, "report_type_id":reportType, "comment":comments]
        
        AF.upload(multipartFormData: { multiPart in
            for (key, keyValue) in body {
                if let keyData = keyValue?.data(using: .utf8) {
                    multiPart.append(keyData, withName: key)
                }
            }
            
            if media != nil {
                print("isi mediaa: \(String(describing: media))")
                
                media?.indices.forEach {
                    multiPart.append((media?[$0].jpegData(compressionQuality: 0.5)!)!, withName: "files[\($0)]", fileName: "media\($0).jpeg", mimeType: "image/jpeg/png")
                    
                    print("=== \(String(describing: media?[$0]))")
                }
            }
        }, to: baseURL + "api/product/report", method: .post, headers: headers).responseJSON() { response in
            
            switch response.result {
            case .success(let result):
                let apiDict = response.value as? [String:Any]
                print("created report: \(apiDict!)")
                let reJson = JSON(result)
                callback(reJson)
                
            case .failure(let error):
                print("error: create report \(error)")
                let err = JSON(error)
                callback(err)
            }
        }
    }

}
