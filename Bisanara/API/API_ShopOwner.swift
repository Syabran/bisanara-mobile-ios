//
//  API_ShopOwner.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 24/01/22.
//

import Foundation
import Alamofire
import SwiftyJSON

extension APIBisanara {
    
    // DETAIL SHOP OWNER
    func getDetailShopOwner(slug: String, callback: @escaping(_ status: Bool, _ jsonResult: JSON?) -> Void) {
        self.access(method: .get, url: "api/manage/shop/\(slug)", params: [:]) { (status, response) in
            
            if response == nil {
                print("Shop detail owner failure")
                callback(false, response)
            } else {
                callback(true, response)
            }
        }
    }
    
    // GET TNC
    func getTncShopOwner(callback: @escaping(_ result: Bool,_ jsonResult: JSON?) -> Void) {
        self.access(method: .get, url: "api/manage/owner/tnc", params: [:]) { (status, response) in
            if response == nil {
                print("get tnc shop failed")
                callback(false, response)
            } else {
                callback(true,response)
            }
        }
    }
    
    // GET LIST PRODUCT OWNER
    func getListProductOwner(shopSlug:String, callback: @escaping(_ status: Bool, _ jsonResult: JSON?) -> Void) {
        self.access(method: .get, url: "api/manage/product/\(shopSlug)/list", params: [:]) { (status, response) in
            
            if response == nil {
                print("Get List Product Owner Failure")
                callback(false, response)
            } else {
                callback(true, response)
            }
            
        }
    }
    
    func changeBannerShop(slug: String, file: Data?, callback: @escaping(_ result: Bool) -> Void) {
        
        let headers: HTTPHeaders = [
            "appkey": "BLD9rZ80N1SknMDLY0b177uBzbBe6JSW7iJCKio3:mobile",
            "Authorization": UserDefaults.standard.value(forKey: "authorization") as? String ?? "" ,
            "Content-Type": "application/json;charset=utf-8"]
        
        AF.upload(multipartFormData: { (multipartFormData) in
            if let data = file {
                multipartFormData.append(data, withName: "file", fileName: "\(Date.init().timeIntervalSinceNow).jpeg", mimeType: "image/jpeg")
            }
            
        }, to: baseURL + "api/manage/shop/\(slug)/banner", headers: headers).responseJSON() { response in
            
            switch response.result {
            case .success(_):
                let banner = response.value as? String
                print("banner uploaded \(banner ?? "")")
                callback(true)
                
            case .failure(let error):
                print("failed change banner shop \(error)")
                callback(false)
            }
        }
    }
    
    func changeLogoShop(slug: String, file: Data?, callback: @escaping(_ result: Bool) -> Void) {
        
        let headers: HTTPHeaders = [
            "appkey": "BLD9rZ80N1SknMDLY0b177uBzbBe6JSW7iJCKio3:mobile",
            "Authorization": UserDefaults.standard.value(forKey: "authorization") as? String ?? "" ,
            "Content-Type": "application/json;charset=utf-8"]
        
        AF.upload(multipartFormData: { (multipartFormData) in
            if let data = file {
                multipartFormData.append(data, withName: "file", fileName: "\(Date.init().timeIntervalSinceNow).jpeg", mimeType: "image/jpeg")
            }
            
        }, to: baseURL + "api/manage/shop/\(slug)/logo", headers: headers).responseJSON() { response in
            
            switch response.result {
            case .success(_):
                let banner = response.value as? String
                print("logoe uploaded \(banner ?? "")")
                callback(true)
                
            case .failure(let error):
                print("failed change logo shop \(error)")
                callback(false)
            }
        }
    }
    
    func updateShopGeneral(slug:String, number1:String, number2:String, mail1:String, mail2:String, desc:String, address:String, postalCode:String, callback: @escaping(_ result: Bool)-> Void) {
        
        let headers: HTTPHeaders = [
            "appkey": "BLD9rZ80N1SknMDLY0b177uBzbBe6JSW7iJCKio3:mobile",
            "Authorization": UserDefaults.standard.value(forKey: "authorization") as? String ?? "" ,
            "Content-Type": "application/json;charset=utf-8"]
        
        let params = ["cpnumber1":number1, "cpnumber2":number2, "cpmail1":mail1, "cpmail2":mail2, "description":desc, "businessAddress":address, "postalCode":postalCode]
        
        AF.upload(multipartFormData: { multiPart in
            for (key, keyValue) in params {
                if let keyData = keyValue.data(using: .utf8) {
                    multiPart.append(keyData, withName: key)
                }
            }
            
        }, to: baseURL + "api/manage/shop/\(slug)", headers: headers).responseJSON() { response in
            
            switch response.result {
            case .success(_):
                let apiDict = response.value as? [String:Any]
                print("update shop: \(apiDict!)")
                callback(true)
                
            case .failure(let error):
                print("error: \(error)")
                callback(false)
            }
        }
    }
    
    func addUpdateSosmedShop(slug:String, sosmedId: String, url: String, sosmedType: String, callback: @escaping(_ result: Bool)-> Void) {
        let headers: HTTPHeaders = [
            "appkey": "BLD9rZ80N1SknMDLY0b177uBzbBe6JSW7iJCKio3:mobile",
            "Authorization": UserDefaults.standard.value(forKey: "authorization") as? String ?? "" ,
            "Content-Type": "application/json;charset=utf-8"]
        
        let params = ["shop_social_media_id":sosmedId, "social_media_id":sosmedType, "url":url]
        
        AF.upload(multipartFormData: { multiPart in
            for (key, keyValue) in params {
                if let keyData = keyValue.data(using: .utf8) {
                    multiPart.append(keyData, withName: key)
                }
            }
        }, to: baseURL + "api/manage/shop/social_media/\(slug)", headers: headers).responseJSON() { response in
            
            switch response.result {
            case .success(let json):
                let reJson = JSON(json)
                if reJson["status"].stringValue == "success" {
                    callback(true)
                }
                
            case .failure(let error):
                print("error add/update sosmed shop: \(error)")
                callback(false)
            }
        }
    }
    
    func deleteShopSosmed(slug:String, sosmedId: String, callback: @escaping(_ result: Bool) -> Void) {
        let headers: HTTPHeaders = [
            "appkey": "BLD9rZ80N1SknMDLY0b177uBzbBe6JSW7iJCKio3:mobile",
            "Authorization": UserDefaults.standard.value(forKey: "authorization") as? String ?? "" ,
            "Content-Type": "application/json;charset=utf-8"]
        
        let body = ["shop_social_media_id":sosmedId]
        
        AF.upload(multipartFormData: { multiPart in
            for (key, keyValue) in body {
                if let keyData = keyValue.data(using: .utf8) {
                    multiPart.append(keyData, withName: key)
                }
            }
        }, to: baseURL + "api/manage/shop/social_media/delete/\(slug)", headers: headers).responseJSON() { response in
            
            switch response.result {
            case .success(let json):
                let reJson = JSON(json)
                if reJson["status"].stringValue == "success" {
                    callback(true)
                }
                
            case .failure(let error):
                print("error delete shop sosmed: \(error)")
                callback(false)
            }
        }
    }
    
    func memberConfirmation(slug:String, callback: @escaping(_ result: Bool) -> Void) {
        let headers: HTTPHeaders = [
            "appkey": "BLD9rZ80N1SknMDLY0b177uBzbBe6JSW7iJCKio3:mobile",
            "Authorization": UserDefaults.standard.value(forKey: "authorization") as? String ?? "" ,
            "Content-Type": "application/json;charset=utf-8"]
        
        let body = ["slug":slug]
        
        AF.upload(multipartFormData: { multiPart in
            for (key, keyValue) in body {
                if let keyData = keyValue.data(using: .utf8) {
                    multiPart.append(keyData, withName: key)
                }
            }
        }, to: baseURL + "api/manage/owner/confirm", headers: headers).responseJSON() { response in
            
            switch response.result {
            case .success(let json):
                let reJson = JSON(json)
                if reJson["status"].stringValue == "success" {
                    callback(true)
                }
                
            case .failure(let error):
                print("error confirm owner: \(error)")
                callback(false)
            }
        }
    }
}
