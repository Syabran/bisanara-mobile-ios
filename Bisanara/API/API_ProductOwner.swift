//
//  API_ProductOwner.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 24/02/22.
//

import Foundation
import Alamofire
import SwiftyJSON
import UIKit

extension APIBisanara {
    
    // ADD NEW PRODUCT
    func addNewProduct(name: String, price: String, spec: String?, desc: String?, category: String, condition: String, callback: @escaping(_ jsonResult: JSON?) -> Void) {
        let headers: HTTPHeaders = [
            "appkey": "BLD9rZ80N1SknMDLY0b177uBzbBe6JSW7iJCKio3:mobile",
            "Authorization": UserDefaults.standard.value(forKey: "authorization") as? String ?? "" ,
            "Content-Type": "application/json;charset=utf-8"]
        
        let body = ["name": name, "price": price, "specification": spec, "description": desc, "category": category, "condition": condition]
        
        AF.upload(multipartFormData: {
            multiPart in
            for (key, keyValue) in body {
                if let keyData = keyValue?.data(using: .utf8) {
                    multiPart.append(keyData, withName: key)
                }
            }
        }, to: baseURL + "api/manage/product/add", headers: headers).responseJSON() { response in
            
            switch response.result {
            case .success(let jsonResponse):
                let apiDict = response.value as? [String:Any]
                print("added new product: \(String(describing: apiDict))")
                
                let reJson = JSON(jsonResponse)
                callback(reJson)
                
            case .failure(let error):
                print("error add product: \(error)")
                let err = JSON(error)
                callback(err)
            }
        }
    }

    // UPDATE NEW PRODUCT
    func updateProduct(slug: String, name: String, price: String, spec: String?, desc: String?, category: String, condition: String, callback: @escaping(_ jsonResult: JSON?) -> Void) {
        let headers: HTTPHeaders = [
            "appkey": "BLD9rZ80N1SknMDLY0b177uBzbBe6JSW7iJCKio3:mobile",
            "Authorization": UserDefaults.standard.value(forKey: "authorization") as? String ?? "" ,
            "Content-Type": "application/json;charset=utf-8"]
        
        let body = ["slug": slug, "name": name, "price": price, "specification": spec, "description": desc, "category": category, "condition": condition]
        
        AF.upload(multipartFormData: {
            multiPart in
            for (key, keyValue) in body {
                if let keyData = keyValue?.data(using: .utf8) {
                    multiPart.append(keyData, withName: key)
                }
            }
        }, to: baseURL + "api/manage/product/update/\(slug)", headers: headers).responseJSON() { response in
            
            switch response.result {
            case .success(let jsonResponse):
                let apiDict = response.value as? [String:Any]
                print("updated product: \(String(describing: apiDict))")
                
                let reJson = JSON(jsonResponse)
                callback(reJson)
                
            case .failure(let error):
                print("error update product: \(error)")
                let err = JSON(error)
                callback(err)
            }
        }
    }

    
    // GET DETAIL PRODUCT OWNER
    func getDetailProductOwner(productSlug: String, callback: @escaping(_ status: Bool, _ jsonResult: JSON?) -> Void) {
        self.access(method: .get, url: "api/manage/product/view/\(productSlug)", params: [:]) { (status, response) in
            
            if response == nil {
                print("Product detail owner failure")
                callback(false, response)
            } else {
                callback(true, response)
            }
        }
    }
    
    // CHANGE STATUS OF PRODUCT OWNER
    func changeStatusProduct(slug: String, statusProduct: String, callback: @escaping(_ statusJson: Bool) -> Void) {
        let headers: HTTPHeaders = [
            "appkey": "BLD9rZ80N1SknMDLY0b177uBzbBe6JSW7iJCKio3:mobile",
            "Authorization": APIBisanara.shared.Headers["Authorization"] ?? "",
            "Content-Type": "application/json;charset=utf-8"]
        
        let body = ["slug":slug, "status":statusProduct]
        
        AF.upload(multipartFormData: { multiPart in
            for (key, keyValue) in body {
                if let keyData = keyValue.data(using: .utf8) {
                    multiPart.append(keyData, withName: key)
                }
            }
        }, to: baseURL + "api/manage/product/status", headers: headers).responseJSON() { response in
            
            switch response.result {
            case .success(_):
                let apiDict = response.value as? [String:Any]
                print("status product: \(apiDict!)")
                callback(true)
                
            case .failure(let error):
                print("error change: \(error)")
                callback(false)
            }
        }
    }
    
    // DELETE PRODUCT OWNER
    func deleteProductOwner(slug:String, productId: String, callback: @escaping(_ result: Bool) -> Void) {
        let headers: HTTPHeaders = [
            "appkey": "BLD9rZ80N1SknMDLY0b177uBzbBe6JSW7iJCKio3:mobile",
            "Authorization": UserDefaults.standard.value(forKey: "authorization") as? String ?? "" ,
            "Content-Type": "application/json;charset=utf-8"]
        
        let body = ["product_id":productId]
        
        AF.upload(multipartFormData: { multiPart in
            for (key, keyValue) in body {
                if let keyData = keyValue.data(using: .utf8) {
                    multiPart.append(keyData, withName: key)
                }
            }
        }, to: baseURL + "api/manage/product/delete", headers: headers).responseJSON() { response in
            
            switch response.result {
            case .success(let json):
                let reJson = JSON(json)
                if reJson["status"].stringValue == "success" {
                    callback(true)
                }
                
            case .failure(let error):
                print("error delete product owner: \(error)")
                callback(false)
            }
        }
    }
    
    // ADD OR UPDATE PRODUCT SOSMED
    func addSosmedProduct(slug:String, productId: String, url: String, sosmedType: String, callback: @escaping(_ jsonResult: JSON?)-> Void) {
        let headers: HTTPHeaders = [
            "appkey": "BLD9rZ80N1SknMDLY0b177uBzbBe6JSW7iJCKio3:mobile",
            "Authorization": UserDefaults.standard.value(forKey: "authorization") as? String ?? "" ,
            "Content-Type": "application/json;charset=utf-8"]
        
        let params = ["product_id":productId, "social_media_id":sosmedType, "url":url]
        
        AF.upload(multipartFormData: { multiPart in
            for (key, keyValue) in params {
                if let keyData = keyValue.data(using: .utf8) {
                    multiPart.append(keyData, withName: key)
                }
            }
        }, to: baseURL + "api/manage/product/social-media/add", headers: headers).responseJSON() { response in // UPDATE SOSMED NOT READY
            
            switch response.result {
            case .success(let result):
                let apiDict = response.value as? [String:Any]
                print("add sosmed: \(apiDict!)")
                let reJson = JSON(result)
                callback(reJson)
                
            case .failure(let error):
                print("error: \(error)")
                let err = JSON(error)
                callback(err)
            }
        }
    }
    
    // DELETE SOSMED PRODUCT
    func deleteProductSosmed(slug:String, productSosmedId: String, callback: @escaping(_ result: Bool) -> Void) {
        let headers: HTTPHeaders = [
            "appkey": "BLD9rZ80N1SknMDLY0b177uBzbBe6JSW7iJCKio3:mobile",
            "Authorization": UserDefaults.standard.value(forKey: "authorization") as? String ?? "" ,
            "Content-Type": "application/json;charset=utf-8"]
        
        let body = ["product_social_media_id":productSosmedId]
        
        AF.upload(multipartFormData: { multiPart in
            for (key, keyValue) in body {
                if let keyData = keyValue.data(using: .utf8) {
                    multiPart.append(keyData, withName: key)
                }
            }
        }, to: baseURL + "api/manage/product/social-media/delete", headers: headers).responseJSON() { response in
            
            switch response.result {
            case .success(let json):
                let reJson = JSON(json)
                if reJson["status"].stringValue == "success" {
                    callback(true)
                }
                
            case .failure(let error):
                print("error delete product sosmed: \(error)")
                callback(false)
            }
        }
    }
    
    // SET IMAGE/MEDIA AS COVER/DEFAULT
    func setImageAsCoverProduct(productMediaId: String, callback: @escaping(_ status: Bool, _ jsonResult: JSON?) -> Void) {
        self.access(method: .get, url: "api/manage/product/media/cover/\(productMediaId)", params: [:]) { (status, response) in
            
            if response == nil {
                print("set image as cover failure")
                callback(false, response)
            } else {
                callback(true, response)
            }
        }
    }
    
    // DELETE IMAGE/MEDIA PRODUCT OWNER
    func deleteImageProduct(productMediaId: String, callback: @escaping(_ status: Bool, _ jsonResult: JSON?) -> Void) {
        self.access(method: .get, url: "api/manage/product/media/delete/\(productMediaId)", params: [:]) { (status, response) in
            
            if response == nil {
                print("delete image/media failure")
                callback(false, response)
            } else {
                callback(true, response)
            }
        }
    }
    
    // ADD IMAGE/MEDIA
    func addMediaProduct(slug:String, media: [UIImage]?, callback: @escaping(_ jsonResult: JSON?) -> Void) {
        let headers: HTTPHeaders = [
            "appkey": "BLD9rZ80N1SknMDLY0b177uBzbBe6JSW7iJCKio3:mobile",
            "Authorization": UserDefaults.standard.value(forKey: "authorization") as? String ?? "" ,
            "Content-Type": "application/json;charset=utf-8"]
        
        AF.upload(multipartFormData: { multiPart in
            if media != nil {
                media?.indices.forEach {
                    multiPart.append((media?[$0].jpegData(compressionQuality: 0.5)!)!, withName: "files[\($0)]", fileName: "media\($0).jpeg", mimeType: "image/jpeg/png")
                    
                    print("=== \(String(describing: media?[$0]))")
                }
            }
        }, to: baseURL + "api/manage/product/media/add/\(slug)", headers: headers).responseJSON() { response in
            
            switch response.result {
            case .success(let result):
                let apiDict = response.value as? [String:Any]
                print("add media: \(apiDict!)")
                let reJson = JSON(result)
                callback(reJson)
                
            case .failure(let error):
                print("error: \(error)")
                let err = JSON(error)
                callback(err)
            }
        }
    }

}
