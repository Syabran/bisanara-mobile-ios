//
//  MasterSocialMedia.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 14/02/22.
//

import Foundation
import SwiftyJSON

// FOR MODEL SOSMED
struct ModelSocialMedia {
    var id: String?
    var name: String?
    var icon: String?
    
    init(response: JSON) {
        id = response["id"].stringValue
        name = response["name"].stringValue
        icon = response["icon"].stringValue
    }
}
