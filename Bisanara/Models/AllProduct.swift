//
//  Product.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 13/07/21.
//

import Foundation
import SwiftyJSON

struct ProductData {
    var id_product: String?
    var name: String?
    var slug: String?
    var price: String?
    var desc: String?
    var spec: String?
    var condition: String?
    var status: String?
    var statusDesc: String?
    var cover: String?
    var media: MediaProduct?
    var sosmed: Sosmed?
    var category: Category?
    var shop: ProductShop?
    var isDiscount: Bool?
    var discount: String?
    
    init(response: JSON) {
        id_product = response["id_product"].stringValue
        name = response["name"].stringValue
        slug = response["slug"].stringValue
        price = response["price"].stringValue
        desc = response["description"].stringValue
        spec = response["specification"].stringValue
        condition = response["condition"].stringValue
        status = response["status"].stringValue
        statusDesc = response["status_desc"].stringValue
        cover = response["cover"].stringValue
        isDiscount = response["is_discount"].boolValue
        discount = response["discount"].stringValue
        
        let mediaResponse = response["media"];
        media = MediaProduct(response: mediaResponse)
        
        let categoryResponse = response["category"];
        category = Category(response: categoryResponse)
        
        let shopResponse = response["shop"];
        shop = ProductShop(response: shopResponse)
    }
}

struct MediaProduct {
    var mediaId: String?
    var productId: String?
    var mediaUrl: String?
    var mediaType: String?
    var isCover: String?
    
    init(response: JSON) {
        mediaId = response["product_media_id"].stringValue
        productId = response["product_id"].stringValue
        mediaUrl = response["path"].stringValue
        mediaType = response["media_type"].stringValue
        isCover = response["is_cover"].stringValue
    }
}

struct Sosmed {
    var productSosmedId: String?
    var sosmedId: String?
    var productId: String?
    var sosmed: SosmedType?
    var url: String?
    
    init(response: JSON) {
        productSosmedId = response["product_social_media_id"].stringValue
        sosmedId = response["social_media_id"].stringValue
        productId = response["product_id"].stringValue
        url = response["url"].stringValue
        
        let sosmedResponse = response["social_media"];
        sosmed = SosmedType(response: sosmedResponse)
    }
}

struct SosmedType {
    var name: String?
    var icon: String?
    
    init(response: JSON) {
        name = response["name"].stringValue
        icon = response["icon"].stringValue
    }
}

struct Category {
    var id_category: Int?
    var name: String?
    var name_english: String?
    var slug: String?
    
    init(response: JSON) {
        id_category = response["id_category"].intValue
        name = response["name"].stringValue
        name_english = response["name_english"].stringValue
        slug = response["slug"].stringValue
    }
}

struct ProductShop {
    var id_shop: String?
    var name: String?
    var slug: String?
    var logo: String?
    
    init(response: JSON) {
        id_shop = response["id_shop"].stringValue
        name = response["name"].stringValue
        slug = response["slug"].stringValue
        logo = response["logo"].stringValue
    }
}

