//
//  Shops.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 16/07/21.
//

import Foundation
import SwiftyJSON

struct ShopData {
    var id_shop: String?
    var name: String?
    var slug: String?
    var address: String?
    var description: String?
    var logo: String?
    var phone: String?
    var email: String?
    var banner: String?
    
    init(response: JSON) {
        id_shop = response["id_shop"].stringValue
        name = response["name"].stringValue
        slug = response["slug"].stringValue
        address = response["address"].stringValue
        description = response["description"].stringValue
        logo = response["logo"].stringValue
        phone = response["phone_number"].stringValue
        email = response["email_primary"].stringValue
        banner = response["banner"].stringValue
    }
}

struct Owner {
    var ownerId: String?
    var userId: String?
    var strm: String?
    var name: String?
    var address: String?
    var phoneNumber: String?
    var roleName: String?
    var roleDesc: String?
    var status: String?
    var statusDesc: String?
    
    init(response: JSON) {
        ownerId = response["owner_id"].stringValue
        userId = response["user_id"].stringValue
        strm = response["strm"].stringValue
        name = response["name"].stringValue
        address = response["address"].stringValue
        phoneNumber = response["phone_number"].stringValue
        roleName = response["role_name"].stringValue
        roleDesc = response["role_desc"].stringValue
        status = response["status"].stringValue
        statusDesc = response["status_desc"].stringValue
    }
}

// SOCIAL MEDIA SHOP OWNER
struct ShopSosmed {
    var id: String?
    var name: String?
    var sosmedId: String?
    var logo: String?
    var url: String?
    var click: String?
    
    init(response: JSON) {
        id = response["id"].stringValue
        name = response["name"].stringValue
        sosmedId = response["social_media_id"].stringValue
        click = response["click"].stringValue
        url = response["url"].stringValue
        logo = response["logo"].stringValue
    }
}
