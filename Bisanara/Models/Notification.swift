//
//  Notification.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 08/11/21.
//

import Foundation

// FEATURE DISABLE
struct Notification {
    
    var id: String?
    var title: String?
    var detail: String?
    var date: String?
}
