//
//  FAQ.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 12/05/22.
//

import Foundation
import SwiftyJSON

struct FAQ {
    var faqId: String?
    var question: String?
    var answer: String?
    var createdAt: String?
    var createdBy: String?
    var createdIP: String?
    var updatedAt: String?
    var updatedBy: String?
    var updatedIP: String?
    
    init(response: JSON) {
        faqId = response["faqId"].stringValue
        question = response["question"].stringValue
        answer = response["answer"].stringValue
        createdAt = response["answer"].stringValue
        createdBy = response["createdBy"].stringValue
        createdIP = response["createdIP"].stringValue
        updatedAt = response["updatedAt"].stringValue
        updatedBy = response["updatedBy"].stringValue
        updatedIP = response["updatedIP"].stringValue
    }
}
