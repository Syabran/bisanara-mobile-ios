//
//  Account.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 27/10/21.
//

import Foundation
import SwiftyJSON

struct Account {
    
    var id: String?
    var username: String?
    var email: String?
    var additionalEmail: String?
    var profilePicture: String?
    var password: String?
    var newPassword: String?
    
    init(response: JSON) {
        id = response["id"].stringValue
        username = response["name"].stringValue
        email = response["mail"].stringValue
        additionalEmail = response["additionalEmail"].stringValue
        profilePicture = response["profilePicture"].stringValue
        password = response["password"].stringValue
        newPassword = response["newPassword"].stringValue
    }
}
