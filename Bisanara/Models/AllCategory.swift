//
//  AllCategory.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 12/07/21.
//

import Foundation
import SwiftyJSON

struct ModelCategory {
    var id: Int?
    var idParents: String?
    var name: String?
    var nameEng: String?
    var slug: String?
    var icon: String?
    
    init(response: JSON) {
        id = response["id_category"].intValue
        idParents = response["id_parent_category"].stringValue
        name = response["name"].stringValue
        nameEng = response["name_english"].stringValue
        slug = response["slug"].stringValue
        icon = response["icon"].stringValue
    }
}


struct CategoryReport {
    var typeId: Int?
    var title: String?
    var desc: String?
    var flagInput: String?
    
    init(response: JSON) {
        typeId = response["reportTypeId"].intValue
        title = response["title"].stringValue
        desc = response["description"].stringValue
        flagInput = response["flagInput"].stringValue
    }
}
