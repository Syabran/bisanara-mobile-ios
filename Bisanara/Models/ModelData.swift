//
//  ModelData.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 27/12/21.
//

import Foundation
import SwiftyJSON

struct ResponseData {
    var code: Int?
    var message: String?
    var result: UserData?
    
    init(response: JSON) {
        let oriResult = response["result"];
        
        code = response["code"].intValue
        message = response["message"].stringValue
        result = UserData(response: oriResult)
        
    }
}

struct UserData {
    var mail: String
    var name: String
    var type: String
    var profileId: String
    var userId: String
    var authorization: String
    
    init(response: JSON) {
        mail = response["mail"].stringValue
        name = response["name"].stringValue
        type = response["type"].stringValue
        profileId = response["profileId"].stringValue
        userId = response["userId"].stringValue
        authorization = response["token"].stringValue
        
    }
}
