//
//  GeneralDataAccount.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 27/10/21.
//

import Foundation

struct GeneralDataAccount {
    
    var id: String?
    var nim: String?
    var campusLocation: String?
    var typeId: String?
    var numberId: String?
    var cardId: String?
    var fullName: String?
    var phone: String?
    var dateOfBirth: String?
    var address: String?
    var country: String?
    var state: String?
    var city: String?
    var postalCode: String?
    
}
