//
//  Post.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 14/07/21.
//

import Foundation

struct Post {
    
    let status:String
    let data: [String: Any]
    let message:String
    
}

extension Post {
    init?(json: [String: Any]) {
        guard let status = json["status"] as? String,
        let data = json["data"] as? [String: Any],
        let message = json["message"] as? String
        else {
            return nil }
        
        self.status = status
        self.data = data
        self.message = message
    }
}


/*
 http://202.58.182.171/api-bisanara/api/test
 
{
 "status":"success",
 "data":
        {"endpoint":"http:\/\/202.58.182.171\/api-bisanara\/api\/test"
 
        },
 "message":"Berhasil Hit API"
 }
*/
