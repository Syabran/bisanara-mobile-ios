//
//  Feed.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 02/11/21.
//

import Foundation

// FEATURE DISABLE
struct Feed {
    
    var shopLogo: String?
    var shopName: String?
    var postDescription: String?
    var productId: String?
    var name: String?
    var image1: String?
    var image2: String?
    var image3: String?
    
}
