//
//  ProductData.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 14/02/22.
//

import Foundation
import SwiftyJSON

struct ProductOwner {
    var productId: String?
    var name: String?
    var productSlug: String?
    var price: String?
    var description: String?
    var spec: String?
    var status: String?
    var statusDesc: String?
    var cover: String?
    var isDiscount: Bool?
    var discount: String?
    var condition: String?
//    var productTransactionId: String?
//    var shopId: String?
//    var categoryProductId: String?
//    var fileLocId: String?
    var media: ProductMedia?
    var socialMedia: SosMedProduct?
    var category: ProductCategory?
    
    init(response: JSON) {
        productId = response["id_product"].stringValue
        name = response["name"].stringValue
        productSlug = response["slug"].stringValue
        price = response["price"].stringValue
        description = response["description"].stringValue
        spec = response["specification"].stringValue
        status = response["status"].stringValue
        // status desc
        cover = response["cover"].stringValue
        condition = response["condition"].stringValue
//        productTransactionId = response["productTransactionId"].stringValue
//        shopId = response["shopId"].stringValue
//        categoryProductId = response["categoryProductId"].stringValue
//        fileLocId = response["fileLocationId"].stringValue
        
        let mediaResponse = response["media"];
        media = ProductMedia(response: mediaResponse)
        
        let categoryResponse = response["category"];
        category = ProductCategory(response: categoryResponse)
        
        let sosmedResponse = response["sosmed"];
        socialMedia = SosMedProduct(response: sosmedResponse)
    }
}

struct ProductMedia {
    var mediaId: String?
    var productId: String?
    var pathUrl: String?
    var mediaType: String?
    var isCover: String?
    
    init(response: JSON) {
        mediaId = response["product_media_id"].stringValue
        productId = response["product_id"].stringValue
        pathUrl = response["path"].stringValue
        mediaType = response["media_type"].stringValue
        isCover = response["is_cover"].stringValue
    }
}

// SOSMED PRODUCT OWNER
struct SosMedProduct {
    var productSosmedId: String?
    var sosmedId: String?
    var productId: String?
    var socialMedia: SocialMediaType?
    var url: String?
    var counter: String?
    
    init(response: JSON) {
        productSosmedId = response["product_social_media_id"].stringValue
        sosmedId = response["social_media_id"].stringValue
        productId = response["product_id"].stringValue
        url = response["url"].stringValue
        counter = response["counter"].stringValue
        
        let sosmedDetail = response["social_media"];
        socialMedia = SocialMediaType(response: sosmedDetail)
    }
}

struct SocialMediaType {
    var name: String?
    var icon: String?
    
    init(response: JSON) {
        name = response["name"].stringValue
        icon = response["icon"].stringValue
    }
}

struct ProductCategory {
    var categoryProductId: Int?
    var idParentCategory: String?
    var name: String?
    var nameEnglish: String?
    var slug: String?
    var icon: String?
//    var status: String?
//    var fileLocationId: String?
    
    init(response: JSON) {
        categoryProductId = response["id_category"].intValue
        idParentCategory = response["id_parent_category"].stringValue
        name = response["name"].stringValue
        nameEnglish = response["name_english"].stringValue
        slug = response["slug"].stringValue
        icon = response["icon"].stringValue
        
//        status = response["status"].stringValue
//        image = response["image"].stringValue
//        fileLocationId = response["fileLocationId"].stringValue
    }
}

struct ProductOwnerShop {
    var idShop: String?
    var name: String?
    var slug: String?
    var logo: String?
    
    init(response: JSON) {
        idShop = response["id_shop"].stringValue
        name = response["name"].stringValue
        slug = response["slug"].stringValue
        logo = response["logo"].stringValue
    }
}
