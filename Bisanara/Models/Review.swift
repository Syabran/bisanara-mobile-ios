//
//  Review.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 22/11/21.
//

import Foundation
import SwiftyJSON

struct Review {
    var reviewId: String?
    var typeReviewer: String?
    var studentId: String?
    var binusianId: String?
    var name: String?
    var email: String?
    var reviewComment: String?
    var isAnonymous: String?
    var media: MediaReview?
    
    init(response: JSON) {
        reviewId = response["review_id"].stringValue
        typeReviewer = response["type_reviewer"].stringValue
        studentId = response["student_id"].stringValue
        binusianId = response["binusian_id"].stringValue
        name = response["name"].stringValue
        email = response["email"].stringValue
        reviewComment = response["review"].stringValue
        isAnonymous = response["is_anonymous"].stringValue
        
        let mediaResponse = response["media"];
        media = MediaReview(response: mediaResponse)
    }
}

struct MediaReview {
    var attachId: String?
    var path: String?
    var type: String?
    
    init(response: JSON) {
        attachId = response["review_attachment_id"].stringValue
        path = response["path"].stringValue
        type = response["type"].stringValue
    }
}
