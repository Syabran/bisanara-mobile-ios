//
//  FilterProductVC.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 28/04/22.
//

import Foundation
import UIKit

class FilterProductVC: UIViewController {
    
    @IBOutlet weak var sortTxtField: UITextField!
    @IBOutlet weak var parentsCategoryTxtField: UITextField!
    @IBOutlet weak var subCategoryTxtField: UITextField!
    private var pickerSort = UIPickerView()
    private var pickerCategory = UIPickerView()
    private var pickerSubCategory = UIPickerView()
    
    var blue = UIColor(red: 73.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0)
    
    var dataCategory: [ModelCategory]? = []
    var categoryName: [String]? = []
    var dataSubCategory: [ModelCategory]? = []
    var subCategoryName: [String]? = []
    var dataSort = ["Newest", "Highest Price", "Lowest Price"]
    
    var sendSort = "ne"
    var getParentCategory = "0"
    var sendCategoryId = 0
    
    private var dispatchGroup = DispatchGroup()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customNavBar()
        hideKeyboardWhenTappedAround()
        loadAfterSave()
        getCategory()
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        if self.subCategoryName != [] {
//            self.subCategoryTxtField.isEnabled = true
//            addPickerSubCategory()
//        } else {
//            self.subCategoryTxtField.isEnabled = false
////            self.subCategoryTxtField.inputView = UIView()
//        }
        addPickerCategory()
        addPickerSort()
    }
    
    func loadAfterSave() {
        self.parentsCategoryTxtField.text = UserDefaults.standard.value(forKey: "parentCategoryTxt") as? String ?? ""
        self.subCategoryTxtField.text = UserDefaults.standard.value(forKey: "subCategoryTxt") as? String ?? ""
        self.getParentCategory = UserDefaults.standard.value(forKey: "idParentCategory") as? String ?? ""
        if self.getParentCategory != "0" {
            getSubCategory()
            self.subCategoryTxtField.isEnabled = true
            addPickerSubCategory()
        }
        self.sendSort = UserDefaults.standard.value(forKey: "sortProduct") as? String ?? "ne"
//        print("get \(sendSort), \(getParentCategory)")
        
        if self.sendSort == "ne" {
            sortTxtField.text = "Newest"
        } else if self.sendSort == "hp" {
            sortTxtField.text = "Highest Price"
        } else if self.sendSort == "lp" {
            sortTxtField.text = "Lowest Price"
        }
    }
    
    // FOR PICKER PARENT CATEGORY
    func getCategory() {
        dispatchGroup.enter()
        APIBisanara.shared.getCategories(idCategory: "0") { (status, response) in
            
            if status == true {
                for item in response!["data"].arrayValue {
                    self.dataCategory?.append(ModelCategory(response: item))
                    self.categoryName?.append(item["name"].stringValue)
                    
                    let parent = item["id_category"].intValue
                    if self.getParentCategory == "\(parent)" {
                        self.parentsCategoryTxtField.text = item["name"].stringValue
                        self.getSubCategory()
                        print("\(self.getParentCategory) === \(parent)")
                    }
                }
            }
        }
        dispatchGroup.leave()
    }
    func addPickerCategory() {
        pickerCategory.delegate = self
        pickerCategory.dataSource = self
        parentsCategoryTxtField.inputView = pickerCategory
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let done = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(doneTappedCategory))
        toolbar.items = [flexible, done]
        
        parentsCategoryTxtField.inputView = pickerCategory
        parentsCategoryTxtField.inputAccessoryView = toolbar
    }
    @objc func doneTappedCategory() {
        self.view.endEditing(true)
        
        for item in dataCategory! {
            if parentsCategoryTxtField.text == item.name?.trimHTMLTags() {
                getParentCategory = "\(item.id!)"
//                print("category picked: \(getParentCategory)")
                
                getSubCategory()
            }
        }
        self.subCategoryTxtField.text = ""
        self.subCategoryTxtField.isEnabled = true
        addPickerSubCategory()
    }
    
    // FOR PICKER SUB CATEGORY
    func getSubCategory() {
        print("sub category loaded, id: \(self.getParentCategory)")
        self.dataSubCategory = [ModelCategory]()
        self.subCategoryName = [String]()
        
        if self.getParentCategory != "0" {
            dispatchGroup.enter()
            APIBisanara.shared.getCategories(idCategory: self.getParentCategory) { (status, response) in
                
                if status == true {
                    print("data sub= \(response!["data"].arrayValue)")
                    
                    for item in response!["data"].arrayValue {
                        self.dataSubCategory?.append(ModelCategory(response: item))
                        self.subCategoryName?.append(item["name"].stringValue)
                        self.subCategoryTxtField.isEnabled = true
                    }
//                    print("get sub \(String(describing: self.subCategoryName))")
                }
            }
            dispatchGroup.leave()
            
        } else {
            let alertFailed = UIAlertController(title: "", message: "please select first the category above to get sub category", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default)
            alertFailed.addAction(ok)
            self.present(alertFailed, animated: true, completion: nil)
        }
    }
    func addPickerSubCategory() {
        pickerSubCategory.delegate = self
        pickerSubCategory.dataSource = self
        subCategoryTxtField.inputView = pickerSubCategory
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let done = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(doneTappedSubCategory))
        toolbar.items = [flexible, done]
        
        subCategoryTxtField.inputView = pickerSubCategory
        subCategoryTxtField.inputAccessoryView = toolbar
    }
    @objc func doneTappedSubCategory() {
        self.view.endEditing(true)
        
        for item in dataSubCategory! {
            if subCategoryTxtField.text == item.name?.trimHTMLTags() {
                sendCategoryId = 0
                sendCategoryId = item.id!
                print("sub category picked: \(sendCategoryId)")
                
            }
            if sendCategoryId == 0 {
                let alertFailed = UIAlertController(title: "", message: "please select Sub Category again", preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style: .default) { action in
                    self.subCategoryTxtField.text = ""
                }
                alertFailed.addAction(ok)
                self.present(alertFailed, animated: true, completion: nil)
            }
        }
    }
    
    func addPickerSort() {
        pickerSort.delegate = self
        pickerSort.dataSource = self
        sortTxtField.inputView = pickerSort
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let done = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(doneTappedSort))
        toolbar.items = [flexible, done]
        sortTxtField.inputView = pickerSort
        sortTxtField.inputAccessoryView = toolbar
    }
    @objc func doneTappedSort() {
        self.view.endEditing(true)
        
        for item in dataSort {
            if sortTxtField.text == item {
                if item == "Newest" {
                    self.sendSort = "ne"
                } else if item == "Highest Price" {
                    self.sendSort = "hp"
                } else if item == "Lowest Price" {
                    self.sendSort = "lp"
                }
                
                print("picked sort \(sortTxtField.text ?? "")")
            }
            
            if self.sendSort == "" {
                let alertFailed = UIAlertController(title: "", message: "please select sort again to filter the product", preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style: .default)
                alertFailed.addAction(ok)
                self.present(alertFailed, animated: true, completion: nil)
            }
        }
    }
    
    func customNavBar() {
        title = "Filter"
        self.navigationItem.largeTitleDisplayMode = .never
        self.view.backgroundColor = blue
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
    }
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
        sortTxtField.endEditing(true)
        parentsCategoryTxtField.endEditing(true)
        subCategoryTxtField.endEditing(true)
        
    }
    
    @IBAction func saveTapped(_ sender: Any) {
        if self.sendSort != "ne" {
            if self.parentsCategoryTxtField.text == "" {
                UserDefaults.standard.set(self.sendSort, forKey: "sortProduct")
                
                self.performSegue(withIdentifier: "unwindToListProduct", sender: self)
                
            } else if self.parentsCategoryTxtField.text != "" {
                if (self.sendCategoryId != 0 && self.subCategoryTxtField.text != "") {
                    UserDefaults.standard.set(self.parentsCategoryTxtField.text, forKey: "parentCategoryTxt")
                    UserDefaults.standard.set(self.getParentCategory, forKey: "idParentCategory")
                    UserDefaults.standard.set(self.subCategoryTxtField.text, forKey: "subCategoryTxt")
                    UserDefaults.standard.set(self.sendCategoryId, forKey: "idCategory")
                    UserDefaults.standard.set(self.sendSort, forKey: "sortProduct")
                    
                    self.performSegue(withIdentifier: "unwindToListProduct", sender: self)
                    
                } else {
                    let alertFailed = UIAlertController(title: "", message: "please select Sub Category to filter the product", preferredStyle: .alert)
                    let ok = UIAlertAction(title: "OK", style: .default)
                    alertFailed.addAction(ok)
                    self.present(alertFailed, animated: true, completion: nil)
                }
            }
        } else if (self.sendCategoryId != 0 && subCategoryTxtField.text != "" && parentsCategoryTxtField.text != "") {
            UserDefaults.standard.set(self.parentsCategoryTxtField.text, forKey: "parentCategoryTxt")
            UserDefaults.standard.set(self.getParentCategory, forKey: "idParentCategory")
            UserDefaults.standard.set(self.subCategoryTxtField.text, forKey: "subCategoryTxt")
            UserDefaults.standard.set(self.sendCategoryId, forKey: "idCategory")
            
            self.performSegue(withIdentifier: "unwindToListProduct", sender: self)
            
        } else {
            let alertFailed = UIAlertController(title: "", message: "please select Sort By and/or Category to filter the product", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default)
            alertFailed.addAction(ok)
            self.present(alertFailed, animated: true, completion: nil)
        }
    }
}

extension FilterProductVC: UIPickerViewDelegate, UIPickerViewDataSource, UITextViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView {
        case pickerCategory:
            return categoryName?.count ?? 0
            
        case pickerSubCategory:
            return subCategoryName?.count ?? 0
            
        case pickerSort:
            return dataSort.count
            
        default: return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView {
        case pickerCategory:
            let name = categoryName?[row] ?? ""
            return name.trimHTMLTags()
            
        case pickerSubCategory:
            let name = subCategoryName?[row] ?? ""
            return name.trimHTMLTags()
            
        case pickerSort:
            return dataSort[row]
            
        default: return "Choose 1"
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == pickerCategory {
            let name = categoryName?[row]
            parentsCategoryTxtField.text = name?.trimHTMLTags()
            
        } else if pickerView == pickerSubCategory {
            if getParentCategory != "0" {
                let name = subCategoryName?[row]
                subCategoryTxtField.text = name?.trimHTMLTags()
            }
            
        } else if pickerView == pickerSort {
            sortTxtField.text = dataSort[row]
        }
    }

}
