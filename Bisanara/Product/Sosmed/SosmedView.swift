//
//  SosmedView.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 12/11/21.
//

import UIKit

class SosmedView: UIView {
    
    @IBOutlet weak var sosmedStackView: UIStackView!
    @IBOutlet weak var sosmedButton: UIButton!
    
    var urlAccount = ""
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
    }
    
    @IBAction func sosmedTapped(_ sender: Any) {
        UIApplication.shared.open(URL(string: "\(urlAccount)")! as URL)
    }
}
