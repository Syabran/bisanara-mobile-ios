//
//  ProductViewController.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 29/09/21.
//

import UIKit
import SwiftyJSON
import Kingfisher
import AVFoundation
import AVKit

class ProductDetailViewController: UIViewController {

    @IBOutlet weak var mediaView: UIView!
    @IBOutlet weak var mediaListCollectionView: UICollectionView!
    
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var discountLbl: UILabel!
    @IBOutlet weak var productRatingImage: UIImageView! // hidden
    @IBOutlet weak var productRatingLbl: UILabel! // hidden
    
    @IBOutlet weak var sosmedViewStackView: UIStackView!
    @IBOutlet weak var sosmedUrlView: UIView!
    @IBOutlet weak var sosmedStackView: UIStackView!
    @IBOutlet weak var sosmedNoneView: UIView!
    
    @IBOutlet weak var shopNameLbl: UILabel!
    @IBOutlet weak var shopLogoBtn: UIButton!
    @IBOutlet weak var shopRatingImage: UIImageView!
    @IBOutlet weak var shopRatingLbl: UILabel!
    
    @IBOutlet weak var descViewStackView: UIStackView!
    @IBOutlet weak var descNoneView: UIView!
    @IBOutlet weak var descSpecView: UIView!
    @IBOutlet weak var descOrSpecLbl: UILabel!
    @IBOutlet weak var descTitleLbl: UILabel!
    @IBOutlet weak var specTitleLbl: UILabel!
    @IBOutlet weak var readMoreLessLbl: UILabel!
    
    @IBOutlet weak var reviewViewStackView: UIStackView!
    @IBOutlet weak var reviewUiView: UIView!
    @IBOutlet weak var reviewListStackView: UIStackView!
    @IBOutlet weak var readMoreReviewView: UIView!
    @IBOutlet weak var reviewNoneView: UIView!
    
    @IBAction func unwindToProductView(_ sender: UIStoryboardSegue) {
//        self.getDataFromAPI()
        print("back to product detail view")
    }
    
    var blue = UIColor(red: 73.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0)
    
    var mediaProduct = [ProductMedia]()
    var sosmed = [JSON?]()
    var dataReview = [JSON?]()
    
    var desc = ""
    var spec = ""
    
    var productId = ""
    var productSlug = ""
    var shopSlug = ""
    var categoryId = 0
    var discount: String?
    var words = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customNavBar()
        customView()
//        getDataFromAPI()
        registerCells()
        uiTapGestureList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        for view in sosmedStackView.arrangedSubviews{
              sosmedStackView.removeArrangedSubview(view)
              view.removeFromSuperview()
        }
        for view in reviewListStackView.arrangedSubviews{
              reviewListStackView.removeArrangedSubview(view)
              view.removeFromSuperview()
        }
        self.mediaProduct = [ProductMedia]()
        getDataFromAPI()
    }
    
    func uiTapGestureList() {
        // tapped desc
        let tapDesc = UITapGestureRecognizer(target: self, action: #selector(self.tapDescription(sender:)))
        descTitleLbl.isUserInteractionEnabled = true
        descTitleLbl.addGestureRecognizer(tapDesc)
        // tapped spec
        let tapSpec = UITapGestureRecognizer(target: self, action: #selector(self.tapSpecification(sender:)))
        specTitleLbl.isUserInteractionEnabled = true
        specTitleLbl.addGestureRecognizer(tapSpec)
        // tapped read more/less
        let tapRead = UITapGestureRecognizer(target: self, action: #selector(self.tapReadMoreLess(sender:)))
        readMoreLessLbl.isUserInteractionEnabled = true
        readMoreLessLbl.addGestureRecognizer(tapRead)
    }
    
    func customNavBar() {
        title = "Product"
        self.navigationItem.largeTitleDisplayMode = .never
        self.view.backgroundColor = blue
    }
    
    func customView() {
        mediaView.layer.borderColor = UIColor.lightGray.cgColor
        mediaView.layer.borderWidth = 1
        descViewStackView.layer.borderWidth = 1
        descViewStackView.layer.borderColor = UIColor.lightGray.cgColor
        reviewViewStackView.layer.borderWidth = 1
        reviewViewStackView.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    func getDataFromAPI() {
        if self.productSlug != "" {
            APIBisanara.shared.getDetailProduct(productSlug: self.productSlug) { (status, response) in
                if status == true {
                    let json = JSON(response!)
                    
                    if json["status"].stringValue == "success" {
                        let dataDict = json["data"].dictionaryValue
                        
                        let product = ProductOwner(response: dataDict["product"]!)
                        
                        self.productId = product.productId ?? ""
                        self.productNameLbl.text = product.name ?? ""
                        self.priceLbl.text = (product.price ?? "").currencyFormatterString()
                        self.priceLbl.textColor = .red
                        self.discountLbl.text = (product.discount ?? "").currencyFormatterString()
                        if product.isDiscount == false {
                            self.discountLbl.isHidden = true
                        } else {
                            self.discountLbl.isHidden = false
                            self.discountLbl.textColor = .red
                        }
                        
                        self.desc = product.description ?? ""
                        self.spec = product.spec ?? ""
                        self.descTitleLbl.textColor = self.blue
                        self.descOrSpecLbl.text = self.desc.trimHTMLTags()
                        if self.descOrSpecLbl.text == "" {
                            self.descSpecView.isHidden = true
                            self.descNoneView.isHidden = false
                        } else {
                            self.descNoneView.isHidden = true
                            self.descSpecView.isHidden = false
                        }
                        
                        let chararacterSet = CharacterSet.whitespacesAndNewlines.union(.punctuationCharacters)
                        let components = self.descOrSpecLbl.text?.components(separatedBy: chararacterSet)
                        self.words = components!.filter { !$0.isEmpty }
                        
                        self.readMoreLessLbl.isHidden = true
                        if self.words.count <= 5 {
                            self.readMoreLessLbl.isHidden =  true
                        } else { self.readMoreLessLbl.isHidden = false }
                        
                        let productDict = dataDict["product"]?.dictionaryValue
                        
                        // GET CATEGORY
                        let category = ProductCategory(response: productDict!["category"]!)
                        DispatchQueue.main.async {
                            let categoryName = category.name ?? ""
                            self.categoryLbl.text = categoryName.trimHTMLTags()
                            self.categoryId = category.categoryProductId ?? 0
//                            self.categoryImg.image = UIImage(named: "\(category.slug ?? "").svg")
                        }
                        
                        // GET SHOP BY PRODUCT
                        let shop = ProductOwnerShop(response: productDict!["shop"]!)
                        DispatchQueue.main.async {
                            self.shopNameLbl.text = shop.name ?? ""
                            self.shopLogoBtn.kf.setImage(with: shop.logo?.asUrl, for: .normal, placeholder: UIImage(named: "icon-shop"))
                            self.shopSlug = shop.slug ?? ""
                        }
                        
                        // GET MEDIA
                        if let media = productDict!["media"]?.arrayValue {
                            for data in media {
                                let media = ProductMedia(response: data)
                                self.mediaProduct.append(media)
                                DispatchQueue.main.async {
                                    self.mediaListCollectionView.reloadData()
    //                                self.noneImageView.isHidden = true
                                }
                            }
                        }
                                                
                        // EACH SOSMED
                        self.sosmed = productDict!["sosmed"]!.arrayValue
                        if self.sosmed.isEmpty != true {
                            self.sosmedUrlView.isHidden = false
//                            var sizeSosmed = 60
                            
                            for data in self.sosmed {
                                let sosmedData = SosMedProduct(response: data!)

                                DispatchQueue.main.async {
                                    self.sosmedStackView.reloadInputViews()

                                    if let card = UINib(nibName: String(describing: PlatformUrl.self), bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? PlatformUrl {

                                        self.sosmedStackView.addArrangedSubview(card)
                                        
                                        card.url = sosmedData.url ?? ""
                                        if sosmedData.socialMedia?.name == "facebook" {
                                            let img = UIImage(named: "facebook")
                                            card.platformImage.setBackgroundImage(img, for: .normal)
                                        } else if sosmedData.socialMedia?.name == "instagram" {
                                            let img = UIImage(named: "instagram")
                                            card.platformImage.setBackgroundImage(img, for: .normal)
                                        } else if sosmedData.socialMedia?.name == "twitter" {
                                            let img = UIImage(named: "twitter")
                                            card.platformImage.setBackgroundImage(img, for: .normal)
                                        } else if sosmedData.socialMedia?.name == "line" {
                                            let img = UIImage(named: "line")
                                            card.platformImage.setBackgroundImage(img, for: .normal)
                                        } else if sosmedData.socialMedia?.name == "whatsapps" || sosmedData.socialMedia?.name == "whatsapp" {
                                            let img = UIImage(named: "whatsapp")
                                            card.platformImage.setBackgroundImage(img, for: .normal)
                                        }

                                        card.platformImage.layer.cornerRadius = 5
//                                        card.platformImage.contentHorizontalAlignment = .left
                                    }
//                                    self.sosmedStackView.frame.size.width = CGFloat(sizeSosmed)
//                                    sizeSosmed += 60
                                    
                                    self.sosmedNoneView.isHidden = true
                                }
                            }
                        } else {
                            self.sosmedUrlView.isHidden = true
                        }
                        
                        // GET REVIEWS
                        let reviewsDict = dataDict["reviews"]?.dictionaryValue
                        self.dataReview = reviewsDict!["data"]!.arrayValue
                        
//                        self.readMoreReviewView.isHidden = true
                        for data in self.dataReview {
                            let review = Review(response: data!)

                            DispatchQueue.main.async {
                                self.reviewListStackView.reloadInputViews()
                                if let cell = UINib(nibName: String(describing: ReviewCellView.self), bundle: nil).instantiate(withOwner: nil, options: nil)[0] as?  ReviewCellView {

                                    self.readMoreReviewView.isHidden = false
                                    self.reviewListStackView.addArrangedSubview(cell)

                                    cell.reviewId = review.reviewId ?? ""
                                    let anonym = review.isAnonymous ?? ""
                                    if anonym == "0" {
                                        cell.nameLbl.text = review.name ?? ""
                                        cell.emailLbl.text = review.email ?? ""
                                    } else {
                                        cell.nameLbl.text = "Anonymous User"
                                        cell.nameLbl.textColor = .red
                                        cell.emailLbl.text = "Anonymous Mail"
                                    }
                                    
                                    cell.feedbackLbl.text = review.reviewComment ?? ""
                                    cell.typeReviewerLbl.text = review.typeReviewer ?? ""

                                    cell.cardView.layer.cornerRadius = 5
                                    cell.cardView.layer.borderWidth = 0.5
                                    cell.cardView.layer.borderColor = UIColor.lightGray.cgColor

                                    // MEDIA REVIEW
                                    let mediaArray = data!["media"].arrayValue
//                                        var widthMedia: CGFloat = 0
//                                        cell.feedbackImageStackView.widthAnchor.constraint(equalToConstant: widthMedia).isActive = true
//                                        cell.feedbackImageStackView.leadingAnchor.constraint(equalTo: cell.mediaView.leadingAnchor, constant: 60).isActive = true
                                    for media in mediaArray {
//                                            widthMedia+=50
                                        let media = MediaReview(response: media)

                                        if media.attachId != "" {
                                            DispatchQueue.main.async {
                                                cell.feedbackImageStackView.reloadInputViews()
                                                if let mediaView = UINib(nibName: String(describing: ReviewMediaView.self), bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? ReviewMediaView {
                                                    cell.feedbackImageStackView.addArrangedSubview(mediaView)

                                                    mediaView.mediaId = media.attachId ?? ""

                                                    let image = media.path ?? ""
                                                    mediaView.mediaImageView.kf.setImage(with: image.asUrl, placeholder: UIImage(systemName: "cube.box"))
                                                    mediaView.mediaImageView.layer.cornerRadius = 8
                                                    
                                                    mediaView.segueViewButton = {
                                                        let cellImg = UIStoryboard(name: "ReviewProduct", bundle: nil).instantiateViewController(withIdentifier: "ReviewProductModalVC") as? ReviewProductModalVC
                                                        self.present(cellImg!, animated: true, completion: nil)
                                                        
                                                        cellImg?.bigImageView.kf.setImage(with: image.asUrl, placeholder: UIImage(systemName: "cube.box"))
                                                        
                                                        cellImg?.closeButton = {
                                                            cellImg?.dismiss(animated: true)
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            cell.mediaView.isHidden =  true
                                        }
                                    }
                                }
                                self.reviewNoneView.isHidden = true
                            }
                        }
                    }
                    
                } else {
                    let error = JSON(response!)
                    print("status false api: \(error)")
                }
            }
        } else {
            print("product slug nil, \(self.productSlug)")
            
        }
    }
    
    private func registerCells() {
        mediaListCollectionView.register(UINib(nibName: ProductImageCollectionViewCell.identifier, bundle: nil), forCellWithReuseIdentifier: ProductImageCollectionViewCell.identifier)
    }
    
    @IBAction func shopDetailTapped(_ sender: Any) {
        let vc = UIStoryboard(name: "ShopView", bundle: nil).instantiateViewController(withIdentifier: "ShopDetailViewController") as? ShopDetailViewController
        vc?.shopSlug = self.shopSlug
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func readReviewTapped(_ sender: Any) {
        // home tab-setting-manage shop-manage product-ManageProduct StoryBoard
        let vc = UIStoryboard(name: "ManageProduct", bundle: nil).instantiateViewController(withIdentifier: "ReviewProductVC") as? ReviewProductVC
        self.navigationController?.pushViewController(vc!, animated: true)
        vc?.productSlug = self.productSlug
        vc?.productId = self.productId
        vc?.isOwner = false
    }
    
    @IBAction func tapDescription(sender: UITapGestureRecognizer) {
        descOrSpecLbl.text = self.desc.trimHTMLTags()
        if self.descOrSpecLbl.text == "" {
            self.descSpecView.isHidden = true
            self.descNoneView.isHidden = false
        } else {
            self.descNoneView.isHidden = true
            self.descSpecView.isHidden = false
        }
        
        let chararacterSet = CharacterSet.whitespacesAndNewlines.union(.punctuationCharacters)
        let components = self.descOrSpecLbl.text?.components(separatedBy: chararacterSet)
        self.words = components!.filter { !$0.isEmpty }
        descTitleLbl.textColor = self.blue
        specTitleLbl.textColor = .black
        if self.words.count > 5 {
            self.readMoreLessLbl.isHidden =  false
        } else { self.readMoreLessLbl.isHidden = true }
    }
    
    @IBAction func tapSpecification(sender: UITapGestureRecognizer) {
        descOrSpecLbl.text = (self.spec).trimHTMLTags()
        if self.descOrSpecLbl.text == "" {
            self.descSpecView.isHidden = true
            self.descNoneView.isHidden = false
        } else {
            self.descNoneView.isHidden = true
            self.descSpecView.isHidden = false
        }
        
        let chararacterSet = CharacterSet.whitespacesAndNewlines.union(.punctuationCharacters)
        let components = self.descOrSpecLbl.text?.components(separatedBy: chararacterSet)
        self.words = components!.filter { !$0.isEmpty }
        specTitleLbl.textColor = self.blue
        descTitleLbl.textColor = .black
        if self.words.count > 5 {
            self.readMoreLessLbl.isHidden =  false
        } else { self.readMoreLessLbl.isHidden = true }
        
    }
    
    @IBAction func tapReadMoreLess(sender: UITapGestureRecognizer) {
        if self.words.count > 5 {
            if self.descOrSpecLbl.numberOfLines == 0 {
                self.descOrSpecLbl.numberOfLines = 2
                self.readMoreLessLbl.text = "Read More"
            } else if self.descOrSpecLbl.numberOfLines == 2 {
                self.descOrSpecLbl.numberOfLines = 0
                self.readMoreLessLbl.text = "Read Less"
            }
            
        } else {
            self.descOrSpecLbl.numberOfLines = 2
            self.readMoreLessLbl.isHidden = true
        }

    }
    
}

extension ProductDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case mediaListCollectionView:
            print("total media: \(mediaProduct.count)")
            return mediaProduct.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case mediaListCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductImageCollectionViewCell.identifier, for: indexPath) as! ProductImageCollectionViewCell
            cell.setup(item: mediaProduct[indexPath.row])
            
            return cell
        default:
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductImageCollectionViewCell.identifier, for: indexPath) as! ProductImageCollectionViewCell
        
        cell.setup(item: mediaProduct[indexPath.row])
        if cell.isVideo == true {
            self.present(cell.avPlayerVC, animated: true) { cell.avPlayerVC.player?.play() }
        }
    }
}

extension String {
    func strikeThrough() -> NSAttributedString {
        let attributeString =  NSMutableAttributedString(string: self)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSUnderlineStyle.single.rawValue, range: NSMakeRange(0,attributeString.length))
        return attributeString
    }
}
