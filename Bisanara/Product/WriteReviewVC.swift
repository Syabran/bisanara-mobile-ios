//
//  ReviewProductVC.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 29/11/21.
//

import UIKit
import SwiftyJSON
import OpalImagePicker

// list product on: home tab-setting-manage shop-manage product-ManageProduct StoryBoard
// then on click create review
class WriteReviewVC: UIViewController, OpalImagePickerControllerDelegate {
    
    @IBOutlet weak var nameTxtField: UITextField!
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var errorEmailLbl: UILabel!
    @IBOutlet weak var hideNameBtn: UIButton!
    @IBOutlet weak var commentsTxtView: UITextView!
    @IBOutlet weak var addMediaBtn: UIButton!
    @IBOutlet weak var mediaCollectionView: UICollectionView!
    @IBOutlet weak var saveReviewBtn: UIButton!
    
    var loginToken = ""
    var userId = ""
    var productId = ""
    var hideName: Bool?
    var imageArray: [UIImage]? = []
    
    var blue = UIColor(red: 73.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customNavBar()
        customTextView()
        registerCollectionViewCells()
        hideKeyboardWhenTappedAround()
        self.errorEmailLbl.text = ""
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.loginToken = APIBisanara.shared.Headers["Authorization"] ?? ""
        if self.loginToken == "" {
            nameTxtField.text = ""
            nameTxtField.isEnabled = true
            emailTxtField.text = ""
            emailTxtField.isEnabled = true
            
            txtFieldEditing()
        } else {
            nameTxtField.text = UserDefaults.standard.value(forKey: "UserName") as? String ?? ""
            nameTxtField.isEnabled = false
            emailTxtField.text = UserDefaults.standard.value(forKey: "UserMail") as? String ?? ""
            emailTxtField.isEnabled = false
            userId = UserDefaults.standard.value(forKey: "UserId") as? String ?? ""
            errorEmailLbl.text = ""
        }

    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
    }
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
        nameTxtField.endEditing(true)
        emailTxtField.endEditing(true)
        commentsTxtView.endEditing(true)
        
    }

    func customNavBar() {
        title = "Review and Feedback"
        self.navigationItem.largeTitleDisplayMode = .never
        self.view.backgroundColor = blue
    }
    
    func registerCollectionViewCells() {
        mediaCollectionView.register(UINib(nibName: ImageProductAddedCollectionViewCell.identifier, bundle: nil), forCellWithReuseIdentifier: ImageProductAddedCollectionViewCell.identifier)
    }
    
    private func customTextView() {
        commentsTxtView.layer.borderWidth = 0.5
        commentsTxtView.layer.borderColor = UIColor.gray.cgColor
        commentsTxtView.layer.cornerRadius = 2
        
        addMediaBtn.layer.cornerRadius = 5
        saveReviewBtn.layer.cornerRadius = 0.5
    }
    
    @IBAction func hideNameTapped(_ sender: UIButton) {
        if sender.isSelected == true {
            hideNameBtn.setBackgroundImage(UIImage(named: "blank-check-box"), for: .normal)
            self.hideName = false
            print("cek hide name \(String(describing: self.hideName))")
            sender.isSelected = false
            
        } else {
            if nameTxtField.text != "" && emailTxtField.text != "" {
                if errorEmailLbl.text == "" {
                    hideNameBtn.setBackgroundImage(UIImage(named: "checkbox"), for: .normal)
                    self.hideName = true
                    print("cek hide name \(String(describing: self.hideName))")
                    sender.isSelected = true
                } else {
                    let alert = UIAlertController(title: "Not Valid Email", message: "Your email url are not valid, please fill the correct input", preferredStyle: .alert)
                    let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
                    alert.addAction(ok)
                    self.present(alert, animated: true, completion: nil)
                }
           
            } else {
                let alert = UIAlertController(title: "Failed CheckMark!", message: "Please fill all the requirements", preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
                alert.addAction(ok)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func txtFieldEditing() {
        emailTxtField.addTarget(self, action: #selector(handleEmailChange), for: .editingChanged)
    }
    @objc func handleEmailChange() {
        guard let email = emailTxtField.text else { return }
        if emailTxtField.isEnabled == true {
            if email.isValidEmail {
                errorEmailLbl.text = ""
            } else {
                errorEmailLbl.text = "Your email is not valid"
            }
        } else {
            errorEmailLbl.text = ""
        }
    }
    
    @IBAction func addMediaTapped(_ sender: Any) {
        let imagePicker = OpalImagePickerController()
        imagePicker.imagePickerDelegate = self
        imagePicker.selectionTintColor = UIColor.white.withAlphaComponent(0.7)
        imagePicker.selectionImageTintColor = UIColor.black
        imagePicker.selectionImage = UIImage(systemName: "checkmark")
        imagePicker.statusBarPreference = UIStatusBarStyle.lightContent
        imagePicker.maximumSelectionsAllowed = 10
        
        let config = OpalImagePickerConfiguration()
        config.maximumSelectionsAllowedMessage = NSLocalizedString("You cannot select that many images, sorry", comment: "")
        imagePicker.configuration = config
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage]) {
        self.imageArray?.append(contentsOf: images)
        print("image array: \(String(describing: imageArray))")
        DispatchQueue.main.async {
            self.mediaCollectionView.reloadData()
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerDidCancel(_ picker: OpalImagePickerController) {
        picker.dismiss(animated: true)
    }
    
    @IBAction func saveReviewTapped(_ sender: Any) {
        print("user id = \(self.userId)")
        if commentsTxtView.text != "" {
            APIBisanara.shared.createReviewProduct(productId: self.productId, nameReviewer: self.nameTxtField.text ?? "", mailReviewer: self.emailTxtField.text ?? "", userId: self.userId, isAnonymous: self.hideName ?? false, comments: self.commentsTxtView.text ?? "", media: self.imageArray) { response in
                if response != nil {
                    let json = JSON(response!)
                    
                    let data = json["data"].dictionaryValue
                    let message = json["message"].stringValue
                    if message == "" {
                        let alertSuccess = UIAlertController(title: "Success Save", message: "\(message)",preferredStyle: .alert)
                        let ok = UIAlertAction(title: "OK", style: .default) { (action) in
                            self.performSegue(withIdentifier: "unwindToProductView", sender: self)
                        }
                        alertSuccess.addAction(ok)
                        self.present(alertSuccess, animated: true, completion: nil)
                        
                    } else {
                        let alertFailed = UIAlertController(title: "Failed to Upload", message: "\(message), data: \(data)",preferredStyle: .alert)
                        let ok = UIAlertAction(title: "OK", style: .default) { (action) in
//                                self.performSegue(withIdentifier: "unwindToProductView", sender: self)
                        }
                        alertFailed.addAction(ok)
                        self.present(alertFailed, animated: true, completion: nil)
                    }
                    
                }
            }
        } else {
            print("ada data kosong")
            let alertFailed = UIAlertController(title: "Failed to Upload", message: "comments must be fill",preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default) { (action) in
//                                self.performSegue(withIdentifier: "unwindToProductView", sender: self)
            }
            alertFailed.addAction(ok)
            self.present(alertFailed, animated: true, completion: nil)
        }
    }
    
}

extension WriteReviewVC: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageProductAddedCollectionViewCell.identifier, for: indexPath) as! ImageProductAddedCollectionViewCell

        cell.productImage.image = self.imageArray?[indexPath.row]
        cell.cellView.layer.cornerRadius = 5
        cell.cellView.layer.borderWidth = 0.5
        cell.cellView.layer.borderColor = UIColor.lightGray.cgColor
        cell.segueButtonDelete = {
            self.imageArray?.remove(at: indexPath.item)
            self.mediaCollectionView.reloadData()
        }
        
        return cell
    }
}
