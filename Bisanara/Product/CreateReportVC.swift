//
//  CreateReportVC.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 29/04/22.
//

import UIKit
import SwiftyJSON
import OpalImagePicker

class CreateReportVC: UIViewController, OpalImagePickerControllerDelegate {
    
    var blue = UIColor(red: 73.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0)
    
    @IBOutlet weak var nameTxtField: UITextField!
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var errorEmailLbl: UILabel!
    @IBOutlet weak var commentTxtView: UITextView!
    @IBOutlet weak var reportTypeTxtField: UITextField!
    @IBOutlet weak var addMediaBtn: UIButton!
    @IBOutlet weak var saveReviewBtn: UIButton!
    @IBOutlet weak var mediaCollectionView: UICollectionView!
    
    private var pickerCategoryReport = UIPickerView()
    var dataCategoryReport: [CategoryReport]? = []
    var typeName: [String]? = []
    
    var sendTypeId = ""
    var loginToken = ""
    var userId = ""
    var productId = ""
    var imageArray: [UIImage]? = []
    var dataMedia: [Data]? = nil
    
    private var dispatchGroup = DispatchGroup()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        customNavBar()
        customTextView()
        registerCollectionViewCells()
        getCategoryReport()
        self.errorEmailLbl.text = ""
    }
    
    override func viewWillAppear(_ animated: Bool) {
        addPickerCategoryReport()
        
        self.loginToken = APIBisanara.shared.Headers["Authorization"] ?? ""
        if self.loginToken == "" {
            nameTxtField.text = ""
            nameTxtField.isEnabled = true
            emailTxtField.text = ""
            emailTxtField.isEnabled = true
            
            txtFieldEditing()
        } else {
            nameTxtField.text = UserDefaults.standard.value(forKey: "UserName") as? String ?? ""
            nameTxtField.isEnabled = false
            emailTxtField.text = UserDefaults.standard.value(forKey: "UserMail") as? String ?? ""
            emailTxtField.isEnabled = false
            userId = UserDefaults.standard.value(forKey: "UserId") as? String ?? ""
            errorEmailLbl.text = ""
        }

    }
    
    func getCategoryReport() {
        dispatchGroup.enter()
        APIBisanara.shared.getCategoriesReport() { (status, response) in
            if status == true {
                for item in response!["data"].arrayValue {
                    self.dataCategoryReport?.append(CategoryReport(response: item))
                    self.typeName?.append(item["title"].stringValue)
                }
            }
        }
        dispatchGroup.leave()
    }
    func addPickerCategoryReport() {
        pickerCategoryReport.delegate = self
        pickerCategoryReport.dataSource = self
        reportTypeTxtField.inputView = pickerCategoryReport
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let done = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(doneTappedCategoryReport))
        toolbar.items = [flexible,done]
        
        reportTypeTxtField.inputView = pickerCategoryReport
        reportTypeTxtField.inputAccessoryView = toolbar
    }
    @objc func doneTappedCategoryReport() {
        self.view.endEditing(true)
        
        for item in dataCategoryReport! {
            if reportTypeTxtField.text == item.title?.trimHTMLTags() {
                sendTypeId = "\(item.typeId!)"
                print("type id picked: \(sendTypeId)")
            }
        }
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
    }
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
        nameTxtField.endEditing(true)
        emailTxtField.endEditing(true)
        commentTxtView.endEditing(true)
    }
    
    func customNavBar() {
        title = "Review and Feedback"
        self.navigationItem.largeTitleDisplayMode = .never
        self.view.backgroundColor = blue
    }
    
    private func customTextView() {
        commentTxtView.layer.borderWidth = 0.5
        commentTxtView.layer.borderColor = UIColor.gray.cgColor
        commentTxtView.layer.cornerRadius = 2
        
        addMediaBtn.layer.cornerRadius = 5
        saveReviewBtn.layer.cornerRadius = 0.5
    }
    
    func registerCollectionViewCells() {
        mediaCollectionView.register(UINib(nibName: ImageProductAddedCollectionViewCell.identifier, bundle: nil), forCellWithReuseIdentifier: ImageProductAddedCollectionViewCell.identifier)
    }
    
    func txtFieldEditing() {
        emailTxtField.addTarget(self, action: #selector(handleEmailChange), for: .editingChanged)
    }
    @objc func handleEmailChange() {
        guard let email = emailTxtField.text else { return }
        if emailTxtField.isEnabled == true {
            if email.isValidEmail {
                errorEmailLbl.text = ""
            } else {
                errorEmailLbl.text = "Your email is not valid"
            }
        } else {
            errorEmailLbl.text = ""
        }
    }
    
    @IBAction func addMediaTapped(_ sender: Any) {
        let imagePicker = OpalImagePickerController()
        imagePicker.imagePickerDelegate = self
        imagePicker.selectionTintColor = UIColor.white.withAlphaComponent(0.7)
        imagePicker.selectionImageTintColor = UIColor.black
        imagePicker.selectionImage = UIImage(systemName: "checkmark")
        imagePicker.statusBarPreference = UIStatusBarStyle.lightContent
        imagePicker.maximumSelectionsAllowed = 10
        
        let config = OpalImagePickerConfiguration()
        config.maximumSelectionsAllowedMessage = NSLocalizedString("You cannot select that many images, sorry", comment: "")
        imagePicker.configuration = config
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage]) {
        self.imageArray?.append(contentsOf: images)
        print("image array: \(String(describing: imageArray))")
        DispatchQueue.main.async {
            self.mediaCollectionView.reloadData()
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerDidCancel(_ picker: OpalImagePickerController) {
        picker.dismiss(animated: true)
    }
    
    @IBAction func saveReportTapped(_ sender: Any) {
        print("user id = \(self.userId)")
        if commentTxtView.text != "" && self.reportTypeTxtField.text != "" {
            if self.imageArray != [] {
                APIBisanara.shared.createReportProduct(productId: self.productId, name: self.nameTxtField.text ?? "", mail: self.emailTxtField.text ?? "", reportType: self.sendTypeId, comments: self.commentTxtView.text ?? "", media: self.imageArray) { response in
                    if response != nil {
                        let json = JSON(response!)
                        
                        let data = json["data"].dictionaryValue
                        let message = json["message"].stringValue
                        if message == "" {
                            let alertSuccess = UIAlertController(title: "Success Save", message: "\(message)",preferredStyle: .alert)
                            let ok = UIAlertAction(title: "OK", style: .default) { (action) in
                                self.performSegue(withIdentifier: "unwindToProductView", sender: self)
                            }
                            alertSuccess.addAction(ok)
                            self.present(alertSuccess, animated: true, completion: nil)
                            
                        } else {
                            let alertSuccess = UIAlertController(title: "Failed to Upload", message: "\(message), data: \(data)",preferredStyle: .alert)
                            let ok = UIAlertAction(title: "OK", style: .default) { (action) in
    //                                self.performSegue(withIdentifier: "unwindToProductView", sender: self)
                            }
                            alertSuccess.addAction(ok)
                            self.present(alertSuccess, animated: true, completion: nil)
                        }
                    }
                }
            } else {
                let alertSuccess = UIAlertController(title: "Failed to Upload", message: "you must add min 1 image/media for your report",preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style: .default)
                alertSuccess.addAction(ok)
                self.present(alertSuccess, animated: true, completion: nil)
            }
                
        } else {
            print("ada data kosong")
            let alertSuccess = UIAlertController(title: "Failed to Upload", message: "comments and report type must be fill",preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default)
            alertSuccess.addAction(ok)
            self.present(alertSuccess, animated: true, completion: nil)
        }
    }
    
}

extension CreateReportVC: UICollectionViewDelegate, UICollectionViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource, UITextViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageProductAddedCollectionViewCell.identifier, for: indexPath) as! ImageProductAddedCollectionViewCell

        cell.productImage.image = self.imageArray?[indexPath.row]
        cell.cellView.layer.cornerRadius = 5
        cell.cellView.layer.borderWidth = 0.5
        cell.cellView.layer.borderColor = UIColor.lightGray.cgColor
        cell.segueButtonDelete = {
            self.imageArray?.remove(at: indexPath.item)
            self.mediaCollectionView.reloadData()
        }
        
        return cell
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView {
        case pickerCategoryReport:
            return typeName?.count ?? 0
            
        default: return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView {
        case pickerCategoryReport:
            let name = typeName?[row] ?? ""
            return name.trimHTMLTags()
            
        default: return "Choose Any"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == pickerCategoryReport {
            let name = typeName?[row]
            reportTypeTxtField.text = name?.trimHTMLTags()
        }
    }
}

