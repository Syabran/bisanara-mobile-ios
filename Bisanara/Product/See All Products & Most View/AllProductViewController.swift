//
//  ProductListViewController.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 01/10/21.
//

import UIKit
import SwiftyJSON

class AllProductViewController: UIViewController, UISearchBarDelegate {

    @IBOutlet weak var productListCollectionView: UICollectionView!
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var productSearchBar: UISearchBar!
    @IBOutlet weak var noneProductView: UIView!
    @IBOutlet weak var filterBtn: CustomButton!
    
    @IBAction func unwindToListProduct(_ sender: UIStoryboardSegue) {
        self.idCategory = UserDefaults.standard.value(forKey: "idCategory") as? Int ?? 0
        self.sort = UserDefaults.standard.value(forKey: "sortProduct") as? String ?? "ne"
        print("get \(sort), \(idCategory)")
        
        products = [ProductData]()
        noneProductView.isHidden = false
        getDataFromAPI(keyword: self.searchText ?? "", page: 1, category: self.idCategory, sort: self.sort)
//        productListCollectionView.reloadData()
    }
    
    var categories: ModelCategory!
    var products: [ProductData] = []
    
    var shopSlug = ""
    
    var isMostViews: Bool?
    var isAllProduct: Bool?
    var productByCategory: Bool?
    var isShopProduct: Bool?
    
    private var searchText: String? // keyword
    var totalItem = 0
    var sort = "ne"
    var idCategory = 0
    var limit = 0
    var page = 1
    var lastPage = 1
    
    var blue = UIColor(red: 73.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customNavBar()
        registerCells()
        productSearchBar.delegate = self
        getDataFromAPI(keyword: self.searchText ?? "", page: 1, category: self.idCategory, sort: self.sort)
        hideKeyboardWhenTappedAround()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        if self.productByCategory == true {
            self.filterBtn.isHidden = true
            self.isAllProduct = true
        } else {
            self.filterBtn.isHidden = false
        }
    }
    
    func getDataFromAPI(keyword: String, page: Int, category: Int, sort: String) {
        print("most view:\(self.isMostViews ?? false), all product:\(self.isAllProduct ?? false), product by shop:\(self.isShopProduct ?? false)")
        
        if isAllProduct == true {
            isMostViews = false
            isShopProduct = false
            APIBisanara.shared.getListProduct(limit: self.limit, page: page, keyword: keyword, sort: sort, category: category) { (status, response) in
                if response != nil {
                    if response!["status"].stringValue == "Success" {
                        
                        // parsong data from json
                        let json = JSON(response!)
                        let dataDictionary = json["data"].dictionaryValue
                        
                        let totalData = dataDictionary["total"]?.intValue
                        self.totalItem = totalData ?? 0
                        
                        self.lastPage = dataDictionary["last_page"]!.intValue
                                            
                        if let array = dataDictionary["data"]?.arrayValue {
    //                        print("array: \(array)")
                            for data in array {
                                
                                let product = ProductData(response: data)
                                self.products.append(product)
                                if self.products.isEmpty == false {
                                    self.noneProductView.isHidden = true
                                } else {
                                    self.noneProductView.isHidden = false
                                }
                                DispatchQueue.main.async {
                                    self.productListCollectionView.reloadData()
                                }
                            }
                        }
                        
                    } else {
                        let alertFail = UIAlertController(title: "Failed to load data product, please try again later", message: response!["message"].stringValue, preferredStyle: .alert)
                        let okFail = UIAlertAction(title: "Try Again", style: .default) { (action) in
                            self.getDataFromAPI(keyword: self.searchText ?? "", page: 1, category: self.idCategory, sort: self.sort)
                        }
                        alertFail.addAction(okFail)
                        self.present(alertFail, animated: false, completion: nil)
                    }
                   
                } else {
                    let alertFail = UIAlertController(title: "", message: "Data is nill, please try again later", preferredStyle: .alert)
                    let okFail = UIAlertAction(title: "Try Again", style: .default) { (action) in
                        self.getDataFromAPI(keyword: self.searchText ?? "", page: 1, category: self.idCategory, sort: self.sort)
                    }
                    alertFail.addAction(okFail)
                    self.present(alertFail, animated: false, completion: nil)
                    
                }
            }
        } else if isMostViews == true { //button hidden on see all most view
            self.isAllProduct = false
            self.productByCategory = false
            self.isShopProduct = false
            APIBisanara.shared.getListMostViewProduct(keyword: keyword, sort: sort, category: category) { (status, response) in
                if response != nil {
                    if response!["status"].stringValue == "Success" {
                        
                        // parsong data from json
                        let json = JSON(response!)
                        let dataDictionary = json["data"].dictionaryValue
                        
                        let totalData = dataDictionary["total"]?.intValue
                        self.totalItem = totalData ?? 0
                        
                        self.lastPage = dataDictionary["last_page"]!.intValue
                                            
                        if let array = dataDictionary["data"]?.arrayValue {
    //                        print("array: \(array)")
                            for data in array {
                                
                                let product = ProductData(response: data)
                                self.products.append(product)
                                if self.products.isEmpty == false {
                                    self.noneProductView.isHidden = true
                                } else {
                                    self.noneProductView.isHidden = false
                                }
                                DispatchQueue.main.async {
                                    self.productListCollectionView.reloadData()
                                }
                            }
                        }
                        
                    } else {
                        print("...  status not success")
                        let alertFail = UIAlertController(title: "Failed to load data most view product, please try again later", message: response!["message"].stringValue, preferredStyle: .alert)
                        let okFail = UIAlertAction(title: "Try Again", style: .default) { (action) in
                            self.getDataFromAPI(keyword: self.searchText ?? "", page: 1, category: self.idCategory, sort: self.sort)
                        }
                        alertFail.addAction(okFail)
                        self.present(alertFail, animated: false, completion: nil)
                    }
                   
                } else {
                    print("... response api nill ")
                    let alertFail = UIAlertController(title: "", message: "Data is nill, please try again later", preferredStyle: .alert)
                    let okFail = UIAlertAction(title: "Try Again", style: .default) { (action) in
                        self.getDataFromAPI(keyword: self.searchText ?? "", page: 1, category: self.idCategory, sort: self.sort)
                    }
                    alertFail.addAction(okFail)
                    self.present(alertFail, animated: false, completion: nil)
                    
                }
            }
        } else if isShopProduct == true {
            self.isMostViews = false
            self.isAllProduct = false
            APIBisanara.shared.getListProductByShop(shopSlug: self.shopSlug) { (status, response) in
                if response != nil {
                    if response!["status"].stringValue == "Success" {
                        
                        // parsong data from json
                        let json = JSON(response!)
                        let array = json["data"].arrayValue
                        for data in array {
                            
                            let product = ProductData(response: data)
                            self.products.append(product)
                            if self.products.isEmpty == false {
                                self.noneProductView.isHidden = true
                            } else {
                                self.noneProductView.isHidden = false
                            }
                            DispatchQueue.main.async {
                                self.productListCollectionView.reloadData()
                            }
                        }
                        
                    } else {
                        let alertFail = UIAlertController(title: "Error", message: response!["message"].stringValue, preferredStyle: .alert)
                        let okFail = UIAlertAction(title: "Try Again", style: .default) { (action) in
                            self.getDataFromAPI(keyword: self.searchText ?? "", page: 1, category: self.idCategory, sort: self.sort)
                        }
                        alertFail.addAction(okFail)
                        self.present(alertFail, animated: false, completion: nil)
                    }
                }  else {
                    let alertFail = UIAlertController(title: "Error", message: "get product by shop result from api is nill", preferredStyle: .alert)
                    let okFail = UIAlertAction(title: "Try Again", style: .default) { (action) in
                        self.getDataFromAPI(keyword: self.searchText ?? "", page: 1, category: self.idCategory, sort: self.sort)
                    }
                    alertFail.addAction(okFail)
                    self.present(alertFail, animated: false, completion: nil)
                    
                }
            }
        }
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
    }
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
//        searchBarSearchButtonClicked(productSearchBar)
        productSearchBar.endEditing(true)
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        productSearchBar.resignFirstResponder()
        if let text = productSearchBar.text {
            self.products = []
            DispatchQueue.main.async {
                self.productListCollectionView.reloadData()
            }
            self.searchText = text
            getDataFromAPI(keyword: text, page: 0, category: self.idCategory, sort: self.sort)
            
        }
    }
    
    private func registerCells() {
        productListCollectionView.register(UINib(nibName: ProductCardCollectionViewCell.identifier, bundle: nil), forCellWithReuseIdentifier: ProductCardCollectionViewCell.identifier)
    }
    
    func customNavBar() {
        if title == "" {
            title = "All Products"
        }
        self.navigationItem.largeTitleDisplayMode = .never
        self.view.backgroundColor = blue
        subView.backgroundColor = .white
    }
    
    @IBAction func filterTapped(_ sender: Any) {
        // in product - filter
        let vc = (UIStoryboard(name: "FilterProduct", bundle: nil).instantiateViewController(withIdentifier: "FilterProductVC") as? FilterProductVC)!
        self.navigationController?.pushViewController(vc, animated: true)
        vc.sendSort = self.sort
        vc.sendCategoryId = self.idCategory
    }
    
}
extension AllProductViewController: UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductCardCollectionViewCell.identifier, for: indexPath) as! ProductCardCollectionViewCell
        cell.setup(product: self.products[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if products.indices.contains(indexPath.row) {
            print(".... \(products[indexPath.row].slug ?? "")")
            let controller = ProductDetailViewController.instantiateFromProductStoryboard()
            controller.productSlug = self.products[indexPath.row].slug ?? ""
            self.navigationController?.pushViewController(controller, animated: true)
        } else {
            print(".... indexpath nill")
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print("on scrolling")
        let position = scrollView.contentOffset.y
        if position > (productListCollectionView.contentSize.height-100-scrollView.frame.size.height) {
            if self.page == self.lastPage {
                print("all data loaded")
            } else {
                self.page+=1
                getDataFromAPI(keyword: self.searchText ?? "", page: self.page, category: self.idCategory, sort: self.sort)
                print("all product: \(products.count)")
            }
        }
    }
    
    
}
