//
//  ResetPasswordViewController.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 11/10/21.
//

import UIKit
import Alamofire

class ResetPasswordViewController: UIViewController {

    @IBOutlet weak var whiteView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        whiteView.layer.cornerRadius = 10
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setGradientBackground()
        super.viewWillAppear(animated)
    }
    
    func setGradientBackground() {
        let colorTop =  UIColor(red: 0.0/255.0, green: 159.0/255.0, blue: 253.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 42.0/255.0, green: 42.0/255.0, blue: 114.0/255.0, alpha: 1.0).cgColor
                    
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = self.view.bounds
                
        self.view.layer.insertSublayer(gradientLayer, at:0)
    }
    
    @IBAction func sendMailTapped(_ sender: Any) {
    }
    
    @IBAction func loginTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "unwindToLogin", sender: self)
    }
}
