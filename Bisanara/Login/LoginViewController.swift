//
//  LoginViewController.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 11/10/21.
//

import UIKit
import Alamofire
import SwiftyJSON

class LoginViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var whiteView: UIView!
    @IBOutlet weak var signInBtn: CustomButton!
    
    @IBAction func unwindToLogin(_ sender: UIStoryboardSegue) {
        // segue on reset password to login
    }
    
    var getAuthor = ""
    var getToken = ""
    var passwordView = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        
        self.navigationItem.largeTitleDisplayMode = .never
        self.navigationController?.navigationBar.tintColor = .white
        whiteView.layer.cornerRadius = 10
        
        passwordTextField.isSecureTextEntry = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setGradientBackground()
    }
    
    func setGradientBackground() {
        let colorTop =  UIColor(red: 0.0/255.0, green: 159.0/255.0, blue: 253.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 42.0/255.0, green: 42.0/255.0, blue: 114.0/255.0, alpha: 1.0).cgColor
                    
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = self.view.bounds
                
        self.view.layer.insertSublayer(gradientLayer, at:0)
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
        
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
        emailTextField.endEditing(true)
        passwordTextField.endEditing(true)
        
    }
    
//    @IBAction func viewPassword(sender: AnyObject) {
//        if(passwordView == true) {
//            passwordTextField.isSecureTextEntry = false
//        } else {
//            passwordTextField.isSecureTextEntry = true
//        }
//        passwordView = !passwordView
//    }

    @IBAction func loginTapped(_ sender: Any) {
        
        signInBtn.isEnabled = false
        
        let email = emailTextField.text
        let password = passwordTextField.text
        print("email: \(email!) & password: \(password!)")
        
        if email == "" || password == "" {
            
            signInBtn.isEnabled = true
            
            let alert = UIAlertController(title: "Fail Login", message: "Please insert your email and password", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
        } else {
            
            APIBisanara.shared.login(username: email!, password: password!) { (result, response) in
                let data = ResponseData(response: response!)
                
                if data.message == nil || data.message == "" {
                    
                    // SUCCESS LOGIN
                    let user = data.result!
                    self.getAuthor = user.authorization
                    self.getToken = UserDefaults.standard.value(forKey: "token") as? String ?? ""
                    APIBisanara.shared.setToken(authorization: user.authorization, token: self.getToken, type: "student")
                    if user.authorization != "" {
                        UserDefaults.standard.removeObject(forKey: "authorization")
                        UserDefaults.standard.removeObject(forKey: "UserId")
                        UserDefaults.standard.removeObject(forKey: "UserMail")
                        UserDefaults.standard.removeObject(forKey: "UserName")
                        UserDefaults.standard.removeObject(forKey: "type")
                    }
                    UserDefaults.standard.set(user.authorization, forKey: "authorization")
                    UserDefaults.standard.set(user.userId, forKey: "UserId")
                    UserDefaults.standard.set(user.mail, forKey: "UserMail") // anggi.gusfin.kinlin@gmail.com
                    UserDefaults.standard.set(user.name, forKey: "UserName")
                    UserDefaults.standard.set(user.type, forKey: "type")
                    
                    print("data user: \(UserDefaults.standard.value(forKey: "UserName") as? String ?? ""), email: \(UserDefaults.standard.value(forKey: "UserMail") as? String ?? ""), userId: \(UserDefaults.standard.value(forKey: "UserId") as? String ?? ""), type: \(UserDefaults.standard.value(forKey: "type") as? String ?? "")")
                    
                    self.signInBtn.isEnabled = true
                    
                    let mvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AccountViewController") as? AccountViewController
                    mvc!.data = user
                    mvc!.navigationController?.navigationBar.tintColor = .systemBlue
                    
                    self.performSegue(withIdentifier: "unwindAfterLogin", sender: self)
                    
                } else {
                    print("message api is nill")
                    
                    let alert = UIAlertController(title: "Fail Login", message: data.message, preferredStyle: .alert)
                    let ok = UIAlertAction(title: "OK", style: .default, handler:{ (action) in
                        self.signInBtn.isEnabled = true
                    })
                    
                    alert.addAction(ok)
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    
}
