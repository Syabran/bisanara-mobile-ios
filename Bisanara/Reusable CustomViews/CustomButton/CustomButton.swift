//
//  CustomButton.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 14/07/21.
//

import UIKit

class CustomButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setRadius()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setRadius()
    }
    
    func setRadius() {
        
        let blue = UIColor(red: 73.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0)
        layer.cornerRadius = 8
        layer.borderWidth = 2
        layer.borderColor = blue.cgColor
        clipsToBounds = true
    }
}
