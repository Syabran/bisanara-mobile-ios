//
//  ReviewTableViewCell.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 22/11/21.
//

import UIKit
import Kingfisher
import SwiftyJSON

// NPT USE THIS ONE, USE REVIEW CELL VIEW

class ReviewTableViewCell: UITableViewCell {
    
    static let identifier = String(describing: ReviewTableViewCell.self)

    @IBOutlet weak var NameLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var feedbackLbl: UILabel!
    @IBOutlet weak var typeReviewerLbl: UILabel!
    @IBOutlet weak var mediaStackView: UIStackView!
    @IBOutlet weak var mediaView: UIView!
    
    var reviewId = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(review: Review) {
        NameLbl.text = review.name ?? ""
        emailLbl.text = review.email ?? ""
        feedbackLbl.text = review.reviewComment ?? ""
        typeReviewerLbl.text = review.typeReviewer ?? ""
        reviewId = review.reviewId ?? ""
        
        
        // NOT WORKS YET
        let medias = review.media as Any
        let jsonData = JSON(medias).arrayValue
        var mediaReview = [MediaReview]()
        print("isi media: \(mediaReview)")
        
        for item in jsonData {
            mediaReview.append(MediaReview(response: item))
        }
        
        mediaReview.forEach { (detail) in
            if detail.attachId != "" {
                if let media = UINib(nibName: String(describing: ReviewMediaView.self), bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? ReviewMediaView {
                    mediaStackView.addArrangedSubview(media)
                    
                    media.mediaId = detail.attachId ?? ""
                    
                    let image = detail.path ?? ""
                    media.mediaImageView.kf.setImage(with: image.asUrl)
                    if media.mediaImageView.image == nil {
                        media.mediaImageView.image = UIImage(systemName: "cube.box")
                    }
                    media.mediaImageView.layer.cornerRadius = 8
                }
            } else {
                mediaView.isHidden = true
            }
        }
    }
}
