//
//  ReviewProductModalVC.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 27/04/22.
//

import Foundation
import UIKit

class ReviewProductModalVC: UIViewController {
    
    
    @IBOutlet weak var bigImageView: UIImageView!
    
    var closeButton: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.preferredContentSize = CGSize(width: 414, height: 450.0)
    }
    
    override func updateViewConstraints() {
        self.view.frame.size.height = UIScreen.main.bounds.height - 200
        self.view.frame.origin.y =  200
                self.view.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)
                super.updateViewConstraints()
    }
    
    @IBAction func closeTapped(_ sender: Any) {
        self.closeButton?()
    }
    
}
extension UIView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
            let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            layer.mask = mask
        }
}
