//
//  ReviewMediaView.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 24/03/22.
//

import UIKit

class ReviewMediaView: UIView {

    // for media: image/video review on product
    
    @IBOutlet weak var mediaImageView: UIImageView!
    @IBOutlet weak var cellView: UIView!
    
    var segueViewButton: (() -> Void)?
    var mediaId = ""
    
    @IBAction func viewImgTapped(_ sender: Any) {
        self.segueViewButton?()
    }
}
