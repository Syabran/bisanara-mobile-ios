//
//  ReviewCellView.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 23/03/22.
//

import UIKit

class ReviewCellView: UIView {

    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var feedbackLbl: UILabel!
    @IBOutlet weak var typeReviewerLbl: UILabel!
    @IBOutlet weak var feedbackImageStackView: UIStackView!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var mediaView: UIView!
    
//    var segueButtonCell: (() -> Void)?
    
    var reviewId = ""
}
