//
//  CategoryCardView.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 13/07/21.
//

import UIKit

class CategoryCardView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialSetup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialSetup()
    }
    
    private func initialSetup() {
        layer.shadowColor = UIColor.orange.cgColor
        layer.shadowOffset = .zero
        
        
    }
}
