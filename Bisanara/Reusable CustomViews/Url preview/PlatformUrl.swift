//
//  PlatformUrl.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 13/12/21.
//

import UIKit

class PlatformUrl: UIView {
    
    @IBOutlet weak var platformImage: UIButton!
    
    var url = ""
    
    @IBAction func platformTapped(_ sender: Any) {
        if url != "" {
            UIApplication.shared.open(URL(string: "\(url)")! as URL)
            print(url)
        }
    }
}
