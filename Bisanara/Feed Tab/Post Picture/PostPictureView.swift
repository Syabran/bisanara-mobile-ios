//
//  PostPictureView.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 08/12/21.
//

import UIKit

class PostPictureView: UIView {

    @IBOutlet weak var pictureStackView: UIStackView!
    @IBOutlet weak var pictureImage: UIImageView!
//    @IBOutlet weak var pictureImageBtn: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
    }
    
}
