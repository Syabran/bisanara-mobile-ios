//
//  FeedTabViewController.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 21/10/21.
//

import UIKit

class FeedTabViewController: UIViewController {

    @IBOutlet weak var feedCollectionView: UICollectionView!
    @IBOutlet weak var optionCollectionView: UICollectionView!
    
    var feeds: [Feed] = [
        .init(shopLogo: "https://picsum.photos/id/1070/40/40", shopName: "Angela", postDescription: "New product!", productId: "https://picsum.photos/id/1/80/80", image1: "https://picsum.photos/id/3/110/110", image2: "", image3: ""),
        .init(shopLogo: "https://picsum.photos/id/2/40/40", shopName: "Arend", postDescription: "New product! nxfshniushuiahoisc oascoa hfa fja jjf oa jsoa jfoaao jfoajfja fahchf auhahfa hfancihrf chfwuehmfimhs ", productId: "https://picsum.photos/id/319/80/80", image1: "https://picsum.photos/id/99/110/110", image2: "https://picsum.photos/id/30/110/110", image3: "https://picsum.photos/id/210/110/110"),
        .init(shopLogo: "https://picsum.photos/id/4/40/40", shopName: "Fukuo", postDescription: "New product!", productId: "https://picsum.photos/id/325/80/80", image1: "https://picsum.photos/id/5/110/110", image2: "https://picsum.photos/id/10/110/110"),
        .init(shopLogo: "https://picsum.photos/id/15/40/40", shopName: "ALan", postDescription: "New product! vsdj vpsdj vpa a    feafejpoavpapmaeveapvonvsdnv s gosg jgosi goisj iogj ois  sg", productId: "https://picsum.photos/id/65/80/80", image1: "https://picsum.photos/id/3/110/110"),
        .init(shopLogo: "https://picsum.photos/id/3/40/40", shopName: "PAN", postDescription: "New product!", productId: "https://picsum.photos/id/1/80/80", image1: "https://picsum.photos/id/8/110/110"),
        .init(shopLogo: "https://picsum.photos/id/16/40/40", shopName: "Chole", postDescription: "New product!", productId: "https://picsum.photos/id/319/80/80", image1: "https://picsum.photos/id/3/110/110", image2: "https://picsum.photos/id/3/110/110"),
    ]
    
    var options: [OptionFeed] = [.init(option: "Newest"), .init(option: "Popular")]
    
    var blue = UIColor(red: 73.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customNavBar()
        registerCells()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationItem.largeTitleDisplayMode = .always
        hideBackBar()
        
        // for default select row
        let selectedIndexPath = IndexPath(item: 0, section: 0)
            optionCollectionView.selectItem(at: selectedIndexPath, animated: false, scrollPosition: .left)
    }
    
    func customNavBar() {
        title = "Feed"
        navigationController?.navigationBar.barTintColor = blue // nav bar background color
        
        navigationController?.navigationBar.largeTitleTextAttributes = [.foregroundColor: UIColor.white] // large title color
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white] // normal title color
        navigationController?.navigationBar.tintColor = .white // item tint color
        
        self.view.backgroundColor = blue
        
    }
    private func hideBackBar() {
        self.navigationItem.hidesBackButton = true
    }

    private func registerCells() {
        feedCollectionView.register(UINib(nibName: NewestProductCollectionViewCell.identifier, bundle: nil), forCellWithReuseIdentifier: NewestProductCollectionViewCell.identifier)
        optionCollectionView.register(UINib(nibName: FeedOptionCollectionViewCell.identifier, bundle: nil), forCellWithReuseIdentifier: FeedOptionCollectionViewCell.identifier)
    }
    
}
extension FeedTabViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case feedCollectionView:
            return feeds.count
        case optionCollectionView:
            return options.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch collectionView {
        case feedCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NewestProductCollectionViewCell.identifier, for: indexPath) as! NewestProductCollectionViewCell
                cell.setup(feed: feeds[indexPath.row])
            return cell
        
        case optionCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FeedOptionCollectionViewCell.identifier, for: indexPath) as! FeedOptionCollectionViewCell
                cell.setup(option: options[indexPath.row])
            return cell
        default:
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}
