//
//  NewestProductCollectionViewCell.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 02/11/21.
//

import UIKit

class NewestProductCollectionViewCell: UICollectionViewCell {
    
    static let identifier = String(describing: NewestProductCollectionViewCell.self)

    @IBOutlet weak var shopLabel: UILabel!
    @IBOutlet weak var postDescriptionLabel: UILabel!
    @IBOutlet weak var productStackView: UIStackView!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var image1: UIImageView!
    @IBOutlet weak var image2: UIImageView!
    @IBOutlet weak var image3: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setup(feed: Feed) {
        
        logoImage.kf.setImage(with: feed.shopLogo?.asUrl)
        logoImage.layer.cornerRadius = (logoImage.frame.height) / 2
        
        shopLabel.text = feed.shopName ?? ""
        postDescriptionLabel.text = feed.postDescription ?? ""
        
        image1.kf.setImage(with: feed.image1?.asUrl)
        image2.kf.setImage(with: feed.image2?.asUrl)
        image3.kf.setImage(with: feed.image3?.asUrl)
        
        if feed.image2 == "" && feed.image3 == "" {
            self.image2.image = UIImage(systemName: "")
            self.image3.image = UIImage(systemName: "")
        }
    }
    
    
}
