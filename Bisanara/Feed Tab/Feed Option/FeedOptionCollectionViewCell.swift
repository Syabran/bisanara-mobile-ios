//
//  FeedOptionCollectionViewCell.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 04/11/21.
//

import UIKit

class FeedOptionCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var optionLabel: UILabel!
    @IBOutlet weak var cardView: UIView!
    
    static let identifier = String(describing: FeedOptionCollectionViewCell.self)
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override var isSelected: Bool {
        didSet {
            optionLabel.textColor = isSelected ? .orange : .label
            
            if self.isSelected {
                optionLabel.font = .boldSystemFont(ofSize: 17.0)
            } else {
                optionLabel.font = .systemFont(ofSize: 17.0)
            }
        }
    }
    
    func setup(option: OptionFeed) {
        optionLabel.text = option.option ?? ""
    }
    
}

struct OptionFeed { // private model for option/menu in feed
    var option: String?
}
