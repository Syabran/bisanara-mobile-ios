//
//  AccountViewController.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 18/10/21.
//

import UIKit

class AccountViewController: UIViewController {

    @IBOutlet weak var generalButton: UIButton!
    @IBOutlet weak var generalLbl: UILabel!
    @IBOutlet weak var signOutButton: UIButton!
    @IBOutlet weak var signOutLbl: UILabel!
    @IBOutlet weak var loginLbl: UILabel!
    @IBOutlet weak var accountButton: CustomButton!
    
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var generalView: UIView!
    @IBOutlet weak var signOutView: UIView!
    @IBOutlet weak var loginView: UIView!
    
    @IBOutlet weak var logoShopImage: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    @IBAction func unwindAfterLogin(_ sender: UIStoryboardSegue) {
        print("unwind login...")
        loadData()
    }
    
    
//    var account: Account?
    var data: UserData?
    var userId = ""
    var firstAlert = false
    
    var blue = UIColor(red: 73.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        accountButton.isHidden = true
        
        customNavBar()
        profileView.layer.borderColor = UIColor.lightGray.cgColor
        profileView.layer.borderWidth = 0.5
        
        generalView.layer.cornerRadius = 15
        generalView.layer.borderWidth = 2
        generalView.layer.borderColor = blue.cgColor
        
        signOutView.layer.cornerRadius = 15
        signOutView.layer.borderWidth = 2
        signOutView.layer.borderColor = blue.cgColor
        
        loginView.layer.cornerRadius = 15
        loginView.layer.borderWidth = 2
        loginView.layer.borderColor = blue.cgColor
        
        generalLbl.text = "Edit your general student account."
        generalLbl.textColor = .gray
        signOutLbl.text = "Sign out your account."
        signOutLbl.textColor = .gray
        loginLbl.text = "Login with your email."
        loginLbl.textColor = .gray
        
        self.userId = data?.userId ?? ""
//        print("id account: \(userId)")
        
        logoShopImage.layer.cornerRadius = (logoShopImage.frame.height) / 2
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let tokenLogin = UserDefaults.standard.value(forKey: "authorization") as? String ?? ""
        if tokenLogin == "" {
            firstAlert = UserDefaults.standard.value(forKey: "firstAlertLogin") as? Bool ?? false
            if firstAlert == false {
                presentAlertToLogin()
                firstAlert = true
                UserDefaults.standard.setValue(firstAlert, forKey: "firstAlertLogin")
            }
            
            usernameLabel.textColor = .lightGray
            emailLabel.textColor = .lightGray
            signOutView.isHidden = true
            loginView.isHidden = false
        } else {
            loadData()
            
        }
        
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationItem.largeTitleDisplayMode = .always
        hideBackBar()
    }
    
    func customNavBar() {
        title = "Profile"
        navigationController?.navigationBar.barTintColor = blue // nav bar background color
        
        navigationController?.navigationBar.largeTitleTextAttributes = [.foregroundColor: UIColor.white] // large title color
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white] // normal title color
        navigationController?.navigationBar.tintColor = .white // item tint color
        
        self.view.backgroundColor = blue
        subView.backgroundColor = .white
    }
    
    func loadData() {
        usernameLabel.text = UserDefaults.standard.value(forKey: "UserName") as? String ?? "Username"
        emailLabel.text = UserDefaults.standard.value(forKey: "UserMail") as? String ?? "email@binus.edu"
        usernameLabel.textColor = .black
        emailLabel.textColor = .black
        signOutView.isHidden = false
        loginView.isHidden = true
        
    }
    
    private func hideBackBar() {
        self.navigationItem.hidesBackButton = true
    }
    
    // hidden
    @IBAction func generalTapped(_ sender: Any) {
        
        if userId != "" {
            let vc = UIStoryboard(name: "GeneralProfile", bundle: nil).instantiateViewController(withIdentifier: "GeneralProfileFirstViewController") as? GeneralProfileFirstViewController
            self.navigationController?.pushViewController(vc!, animated: true)
        } else {
//            generalButton.titleLabel?.tintColor = .gray
            presentAlertToLogin()
        }
    }
    
    // hidden
    @IBAction func accountTapped(_ sender: Any) {
        
        if userId != "" {
            let vc = UIStoryboard(name: "AccountProfile", bundle: nil).instantiateViewController(withIdentifier: "AccountProfileViewController") as? AccountProfileViewController
            self.navigationController?.pushViewController(vc!, animated: true)
        } else {
//            accountButton.titleLabel?.tintColor = .gray
            presentAlertToLogin()
        }
    }
    
    @IBAction func loginTapped(_ sender: Any) {
        let mvc = UIStoryboard(name: "LoginAccount", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
        self.navigationController?.pushViewController(mvc!, animated: true)
    }
    
    @IBAction func signOutTapped(_ sender: Any) {
        let alert = UIAlertController(title: "Sign Out Account", message: "Are you sure want to sign out your account?", preferredStyle: .alert)
        let yes = UIAlertAction(title: "Yes", style: .default) { (action) in
            
            APIBisanara.shared.logout { (result, response) in
                if response!["message"].stringValue == "Logout Successfully" {
                    
                    let alert = UIAlertController(title: "Account Logout!", message: response!["message"].stringValue , preferredStyle: .alert)
                    let ok = UIAlertAction(title: "OK", style: .default) { (action) in
                        
                        UserDefaults.standard.removeObject(forKey: "authorization")
                        UserDefaults.standard.removeObject(forKey: "UserId")
                        UserDefaults.standard.removeObject(forKey: "UserMail")
                        UserDefaults.standard.removeObject(forKey: "UserName")
                        UserDefaults.standard.removeObject(forKey: "type")
                        print("token, userid, mail, and username are remove from userdefault")
                        
                        self.userId = ""
                        self.usernameLabel.text = "Username"
                        self.emailLabel.text = "email@binus.edu"
                        self.usernameLabel.textColor = .lightGray
                        self.emailLabel.textColor = .lightGray
                        self.loginView.isHidden = false
                        self.signOutView.isHidden = true
                        
                    }
                    alert.addAction(ok)
                    self.present(alert, animated: true, completion: nil)
                    
                } else {
                    
                    let alert = UIAlertController(title: "Logout Failed!", message: response!["message"].stringValue , preferredStyle: .alert)
                    let tryAgain = UIAlertAction(title: "Try Again", style: .default, handler: { (action) in
                        
                        APIBisanara.shared.logout { (result, response) in
                            if response!["message"].stringValue == "Logout Successfully" {
                                
                                let alert = UIAlertController(title: "Account Logout!", message: response!["message"].stringValue , preferredStyle: .alert)
                                let ok = UIAlertAction(title: "OK", style: .default) { (action) in
                                    UserDefaults.standard.removeObject(forKey: "authorization")
                                    UserDefaults.standard.removeObject(forKey: "UserId")
                                    UserDefaults.standard.removeObject(forKey: "UserMail")
                                    UserDefaults.standard.removeObject(forKey: "UserName")
                                    UserDefaults.standard.removeObject(forKey: "type")
                                    print("token, userid, mail, and username are removed from userdefault")
                                    
                                    self.userId = ""
                                    self.usernameLabel.text = "Username"
                                    self.emailLabel.text = "email@binus.edu"
                                    self.usernameLabel.textColor = .lightGray
                                    self.emailLabel.textColor = .lightGray
                                    self.loginView.isHidden = false
                                    self.signOutView.isHidden = true
                                }
                                alert.addAction(ok)
                                self.present(alert, animated: true, completion: nil)
                                
                            } else {
                                let alert = UIAlertController(title: "Logout Failed!", message: "Please try again later in a few minutes" , preferredStyle: .alert)
                                let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
                                alert.addAction(ok)
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                        
                    })
                    let cancel = UIAlertAction(title: "Cancel", style: .default, handler: nil)
                    
                    alert.addAction(tryAgain)
                    alert.addAction(cancel)
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
        
        let no = UIAlertAction(title: "No", style: .default, handler: nil)
        alert.addAction(yes)
        alert.addAction(no)
        self.present(alert, animated: true, completion: nil)
        
    }
    
//    @IBAction func changePasswordTapped(_ sender: Any) {
//
//        if userId != "" {
//            let vc = UIStoryboard(name: "ForgetPassword", bundle: nil).instantiateViewController(withIdentifier: "ForgetPasswordViewController") as? ForgetPasswordViewController
//            self.navigationController?.pushViewController(vc!, animated: true)
//        } else {
//            changePassButton.titleLabel?.tintColor = .gray
//            presentAlertToLogin()
//        }
//    }
}
