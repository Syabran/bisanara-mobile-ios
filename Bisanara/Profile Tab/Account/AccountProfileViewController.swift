//
//  AccountProfileViewController.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 27/10/21.
//

import UIKit

class AccountProfileViewController: UIViewController {
    
    @IBOutlet weak var subView: UIView!
    
    @IBOutlet weak var accountImage: UIImageView!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var additionalEmailTextField: UITextField!
    
    var account: Account!
    var id: String?
    
    var blue = UIColor(red: 73.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customNavBar()
        
        id = "" //account.id ?? ""
        if id != "" {
            loadData()
        }
        
        accountImage.layer.cornerRadius = (accountImage.frame.height) / 2
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        self.navigationItem.hidesBackButton = false
//    }
    
    func customNavBar() {
        title = "Profile"
        self.navigationItem.largeTitleDisplayMode = .never
        self.view.backgroundColor = blue
        subView.backgroundColor = .white
        
    }
    
    func loadData() {
        usernameTextField.text = account.username
        emailTextField.text = account.email
        additionalEmailTextField.text = account.additionalEmail
    }
    
    @IBAction func saveTapped(_ sender: Any) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AccountViewController") as? AccountViewController
        self.navigationController?.pushViewController(vc!, animated: true)
//        vc!.navigationItem.hidesBackButton = true
    }
    
}

