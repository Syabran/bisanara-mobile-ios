//
//  ForgetPasswordViewController.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 28/10/21.
//

import UIKit

class ForgetPasswordViewController: UIViewController {
    
    
    @IBOutlet weak var currentPassTextField: UITextField!
    @IBOutlet weak var newPassTextField: UITextField!
    @IBOutlet weak var confirmPassTextField: UITextField!
    
    @IBOutlet weak var subView: UIView!
    
    var data: Account!
    var id: String?
    
    var blue = UIColor(red: 73.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customNavBar()
    }
    
    func customNavBar() {
        title = "Change Password"
        self.navigationItem.largeTitleDisplayMode = .never
        self.view.backgroundColor = blue
        subView.backgroundColor = .white
    }
    
    @IBAction func saveTapped(_ sender: Any) {
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AccountViewController") as? AccountViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
}
