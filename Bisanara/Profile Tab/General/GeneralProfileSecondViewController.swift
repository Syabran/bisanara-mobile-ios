//
//  GeneralAccountSecondViewController.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 27/10/21.
//

import UIKit

class GeneralProfileSecondViewController: UIViewController {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var dateOfBirthTextField: UITextField!
    @IBOutlet weak var addressTxtView: UITextView!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var ProvinceTxtField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var postalCodeTextField: UITextField!
    
    @IBOutlet weak var subView: UIView!
    
    
    var data: GeneralDataAccount!
    var idAccount: String? = ""
    
    var blue = UIColor(red: 73.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customNavBar()
        
        if idAccount != "" {
            loadData()
        }
        
        addressTxtView.layer.cornerRadius = 5
        addressTxtView.layer.borderWidth = 0.5
        addressTxtView.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    func customNavBar() {
        title = "General"
        self.navigationItem.largeTitleDisplayMode = .never
        self.navigationController?.navigationBar.backgroundColor = blue
        self.view.backgroundColor = blue
        subView.backgroundColor = .white
    }
    
    func loadData() {
        nameTextField.text = data.fullName
        phoneTextField.text = data.phone
        dateOfBirthTextField.text = data.dateOfBirth
        addressTxtView.text = data.address
        countryTextField.text = data.country
        ProvinceTxtField.text = data.state
        cityTextField.text = data.city
        postalCodeTextField.text = data.postalCode
    }
    
    @IBAction func saveDataTapped(_ sender: Any) {
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AccountViewController") as? AccountViewController {
            self.show(vc, sender: self)
//            vc.navigationItem.hidesBackButton = true
        }       
    }
}
