//
//  GeneralAccountViewController.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 27/10/21.
//

import UIKit

class GeneralProfileFirstViewController: UIViewController {
    
    @IBOutlet weak var nimTextField: UITextField!
    @IBOutlet weak var campusLocationTextField: UITextField!
    @IBOutlet weak var idTypeTextField: UITextField!
    @IBOutlet weak var idNumberTextField: UITextField!
    @IBOutlet weak var idCardImage: UIImageView!
    @IBOutlet weak var subView: UIView!
    
    var data: GeneralDataAccount!
    var idAccount: String? = ""
    
    var blue = UIColor(red: 73.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customNavBar()
        
        placeholderTxtField()
        
        idCardImage.layer.borderColor = UIColor.gray.cgColor
        idCardImage.layer.borderWidth = 0.5
        idCardImage.layer.cornerRadius = 5
        
        if idAccount != "" {
            loadData()
        }
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        self.navigationItem.hidesBackButton = false
//    }
    
    func customNavBar() {
        title = "General"
        self.navigationItem.largeTitleDisplayMode = .never
        self.view.backgroundColor = blue
        subView.backgroundColor = .white
    }
    
    func loadData() {
        nimTextField.text = data.nim
        campusLocationTextField.text = data.campusLocation
        idTypeTextField.text = data.typeId
        idNumberTextField.text = data.numberId
    }
    
    func placeholderTxtField() {
        nimTextField.placeholder = "21098752"
        campusLocationTextField.placeholder = "Anggrek Campus"
        idTypeTextField.placeholder = ""
        idNumberTextField.placeholder = "09192731831"
    }
    
    @IBAction func uploadIdCardTapped(_ sender: Any) {
    }
    
    @IBAction func nextTapped(_ sender: Any) {
        let vc = UIStoryboard(name: "GeneralProfile", bundle: nil).instantiateViewController(withIdentifier: "GeneralProfileSecondViewController") as? GeneralProfileSecondViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}
