//
//  ImageController.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 03/02/22.
//

import Foundation
import UIKit

//  IMAGE EXTENSION
// *if the kingfisher doesnt works

var imageCache = NSCache<AnyObject, AnyObject>()
extension UIImageView {
    func urlLoad(urlString: String) {
        if let image = imageCache.object(forKey: urlString as NSString) as? UIImage {
            self.image = image
            return
        }
        
        guard let url = URL(string: urlString) else { return }
        
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        imageCache.setObject(image, forKey: urlString as NSString)
                        self?.image = image
                    }
                }
            }
        }
    }
}
