//
//  UIViewController+Extension.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 29/09/21.
//

import UIKit

extension UIViewController {
    
    
    // extension to call or segue view controller with more clean
    static var identifier: String {
        return String(describing: self)
    }
    static func instantiateFromProductStoryboard() -> Self {
        let storyboard = UIStoryboard(name: "ProductView", bundle: nil)
        return storyboard.instantiateViewController(identifier: identifier) as! Self
    }
    static func instantiateFromShopStoryboard() -> Self {
        let storyboard = UIStoryboard(name: "ShopView", bundle: nil)
        return storyboard.instantiateViewController(identifier: identifier) as! Self
    }
    static func instantiateFromManageShopStoryboard() -> Self {
        let storyboard = UIStoryboard(name: "ManageShop", bundle: nil)
        return storyboard.instantiateViewController(identifier: identifier) as! Self
    }
    static func instantiateFromManageProductStoryboard() -> Self {
        let storyboard = UIStoryboard(name: "ManageProduct", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: identifier) as! Self
    }
    
    
    // extension to present the alert if user tapped on feature that required an account
    func presentAlertToLogin() {
        let alert = UIAlertController(title: "Login Required!", message: "Login with your Binus Email to manage your shop, product and more!", preferredStyle: .alert)
        let login = UIAlertAction(title: "Login", style: .default) { (action) in
            
            let mvc = UIStoryboard(name: "LoginAccount", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
            self.navigationController?.pushViewController(mvc!, animated: true)
        }
        alert.addAction(login)
        
        let cancel = UIAlertAction(title: "Cancel", style: .default)
        alert.addAction(cancel)
        
        return self.present(alert, animated: true, completion: nil)
    }
    
    // to dismiss or hide the keyboard
}
