//
//  String+Extension.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 13/07/21.
//

import Foundation

extension String {
    
    var asUrl: URL? {
        let removeHTML = self.trimHTMLTags()
        let url = removeHTML?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        return URL(string: url!)
    }
    
    var isValidEmail: Bool {
        get {
            let regEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
            let predicate = NSPredicate(format: "SELF MATCHES %@", argumentArray: [regEx])
            return predicate.evaluate(with: self)
        }
    }
    
    public func trimHTMLTags() -> String? { // to remove html tags
            guard let htmlStringData = self.data(using: String.Encoding.utf8) else {
                return nil
            }
        
            let options: [NSAttributedString.DocumentReadingOptionKey : Any] = [
                .documentType: NSAttributedString.DocumentType.html,
                .characterEncoding: String.Encoding.utf8.rawValue
            ]
        
            let attributedString = try? NSAttributedString(data: htmlStringData, options: options, documentAttributes: nil)
            return attributedString?.string
        }
    
    public func currencyFormatterString() -> String {
        if let value = Double(self) {
            let formatter = NumberFormatter()
            formatter.numberStyle = .currency
//            formatter.maximumFractionDigits = 2
//            formatter.minimumFractionDigits = 2
            if let str = formatter.string(for: value) {
                return str
            }
        }
        return ""
    }
}
