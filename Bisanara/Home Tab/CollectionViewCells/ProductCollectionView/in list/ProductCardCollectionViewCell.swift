//
//  ProductCardCollectionViewCell.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 13/05/22.
//

import UIKit
import Kingfisher

class ProductCardCollectionViewCell: UICollectionViewCell {
    
    static let identifier = "ProductCardCollectionViewCell"
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var nameProductLbl: UILabel!
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var discountLbl: UILabel!
    
    @IBOutlet weak var shopLogoImgView: UIImageView!
    @IBOutlet weak var shopNameLbl: UILabel!
    
    func setup(product: ProductData) {
        productImageView.layer.cornerRadius = 10
        productImageView.kf.setImage(with: (product.cover ?? "").asUrl, placeholder: UIImage(systemName: "cube.box"))
        
        nameProductLbl.text = product.name ?? ""
        categoryLbl.text = product.category?.name ?? ""
//        viewProductLabel.text = "Viewed \(product.viewed ?? "")"
        
        let price = (product.price ?? "").currencyFormatterString()
        if product.discount != "" {
            discountLbl.text = (product.discount ?? "").currencyFormatterString()
            priceLbl.attributedText = "\(price)".strikeThrough() // untuk coret harga asli
            priceLbl.font = UIFont.systemFont(ofSize: 12.0)
            discountLbl.font = UIFont.boldSystemFont(ofSize: 15.0)
            discountLbl.textColor = .red
            priceLbl.textColor = .black
        } else {
            priceLbl.text = price
            priceLbl.font = UIFont.boldSystemFont(ofSize: 15.0)
            priceLbl.textColor = .red
            discountLbl.isHidden = true
        }
        
        shopLogoImgView.kf.setImage(with: (product.shop?.logo ?? "").asUrl, placeholder: UIImage(systemName: "icon-shop"))
        shopLogoImgView.layer.cornerRadius = 5
        shopNameLbl.text = product.shop?.name ?? ""
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
