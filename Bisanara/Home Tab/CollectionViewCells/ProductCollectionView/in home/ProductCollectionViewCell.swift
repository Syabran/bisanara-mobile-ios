//
//  ProductCollectionViewCell.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 13/07/21.
//

import UIKit
import Kingfisher

class ProductCollectionViewCell: UICollectionViewCell {

    static let identifier = "ProductCollectionViewCell"
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var nameProductLabel: UILabel!
    @IBOutlet weak var priceProductLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var shopNameLabel: UILabel!
    
    func setup(product: ProductData) {
        
//        productImageView.urlLoad(urlString: "https://icons.iconarchive.com/icons/google/noto-emoji-food-drink/512/32395-green-salad-icon.png")
        
//        print("cover url: \(product.cover ?? "")")
        productImageView.layer.cornerRadius = 10
        productImageView.kf.setImage(with: (product.cover ?? "").asUrl, placeholder: UIImage(systemName: "cube.box"))
        
        nameProductLabel.text = product.name ?? ""
        categoryLabel.text = product.category?.name ?? ""
        shopNameLabel.text = product.shop?.name ?? ""
        
        
        let price = (product.price ?? "").currencyFormatterString()
        if product.discount != "" {
            discountLabel.text = (product.discount ?? "").currencyFormatterString()
            priceProductLabel.attributedText = "\(price)".strikeThrough() // untuk coret harga asli
            priceProductLabel.font = UIFont.systemFont(ofSize: 12.0)
            discountLabel.font = UIFont.boldSystemFont(ofSize: 15.0)
            discountLabel.textColor = .red
            priceProductLabel.textColor = .black
        } else {
            priceProductLabel.text = price
            priceProductLabel.font = UIFont.boldSystemFont(ofSize: 15.0)
            priceProductLabel.textColor = .red
            discountLabel.isHidden = true
        }
        
        
    }
}


