//
//  CoverCollectionViewCell.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 25/10/21.
//

import UIKit
import Kingfisher

class CoverCollectionViewCell: UICollectionViewCell {
    
    static let identifier = String(describing: CoverCollectionViewCell.self)

    @IBOutlet weak var coverImage: UIImageView!
    
    func setup (cover: Cover) {
        coverImage?.kf.setImage(with: cover.coverImage?.asUrl)
    }

}
