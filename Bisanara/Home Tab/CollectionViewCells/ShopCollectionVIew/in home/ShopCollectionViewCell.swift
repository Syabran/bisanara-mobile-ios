//
//  ShopCollectionViewCell.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 16/07/21.
//

import UIKit
import Kingfisher

class ShopCollectionViewCell: UICollectionViewCell {

    static let identifier = String(describing: ShopCollectionViewCell.self)
    
    @IBOutlet weak var logoView: UIView!
    @IBOutlet weak var logoShopImageView: UIImageView!
    @IBOutlet weak var nameShopLabel: UILabel!
    
//    var getDataLogo : Data? = nil
    
    func setup(shop: ShopData) {
        
        let logo = shop.logo ?? ""
        self.logoShopImageView.kf.setImage(with: logo.asUrl, placeholder: UIImage(named: "icon-shop"))
        
        nameShopLabel.text = shop.name
                
        logoShopImageView.layer.cornerRadius = 5
        logoView.layer.cornerRadius = logoView.bounds.height / 2    //for rounded image
        logoView.layer.borderWidth = 1
        logoView.layer.borderColor = UIColor.lightGray.cgColor
    }

}
