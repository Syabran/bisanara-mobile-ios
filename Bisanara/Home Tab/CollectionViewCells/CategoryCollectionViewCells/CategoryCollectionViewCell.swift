//
//  CategoryCollectionViewCell.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 13/07/21.
//

import UIKit
import Kingfisher

// home tab-collectionviewcells-category collectionviewcells
class CategoryCollectionViewCell: UICollectionViewCell {

    static let identifier = String(describing: CategoryCollectionViewCell.self)
    
    @IBOutlet weak var parentsImgView: UIImageView!
    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var categoryTitleLabel: UILabel!
    @IBOutlet weak var imageView: UIView!
    @IBOutlet weak var categoryView: UIView!
    
    
    func setup(category: ModelCategory) {
        categoryTitleLabel.text = category.name?.trimHTMLTags()
        
        let parent = (category.icon ?? "")
        if parent != "" {
            parentsImgView.kf.setImage(with: parent.asUrl)
            parentsImgView.contentMode = .scaleAspectFit
        } else {
            parentsImgView.isHidden = true
            categoryImageView.image = UIImage(named: "\(category.slug ?? "").svg")
            categoryImageView.contentMode = .scaleAspectFit
            categoryImageView.tintColor = UIColor(red: 0.0/255.0, green: 73.0/255.0, blue: 141.0/255.0, alpha: 1.0)
        }
        
//        imageView.layer.cornerRadius = imageView.bounds.height / 2 //for rounded image
//        imageView.backgroundColor = UIColor(red: 73.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0)
        imageView.layer.cornerRadius = 5
        imageView.layer.borderColor = UIColor.lightGray.cgColor
        imageView.layer.borderWidth = 1

        categoryView.layer.borderWidth = 1
        categoryView.layer.borderColor = UIColor.darkGray.cgColor
        categoryView.layer.cornerRadius = 5
    }
}
