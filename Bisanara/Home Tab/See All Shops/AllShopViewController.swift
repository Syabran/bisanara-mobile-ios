//
//  AllShopViewController.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 05/10/21.
//

import UIKit
import SwiftyJSON
import Alamofire

class AllShopViewController: UIViewController, UISearchBarDelegate {
    
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var shopListCollectionView: UICollectionView!
    @IBOutlet weak var shopSearchBar: UISearchBar!
    @IBOutlet weak var filterButton: CustomButton!
    
    var searchText: String?
    
    var blue = UIColor(red: 73.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0)
    
    var shops: [ShopData] = []
    //    var shops: [Shop] = [] //just for dummy data
    
    var limit = 0
    var totalItem = 0
    var page = 1
    var lastPage = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customNavBar()
        hideKeyboardWhenTappedAround()
        shopSearchBar.delegate = self
        
        // CHANGE CELL
        shopListCollectionView.register(UINib(nibName: ShopCardCollectionViewCell.identifier, bundle: nil), forCellWithReuseIdentifier: ShopCardCollectionViewCell.identifier)
        
        getDataFromAPI(keyword: self.searchText ?? "", page: 1)
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
    }
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
//        searchBarSearchButtonClicked(shopSearchBar)
        shopSearchBar.endEditing(true)
    }
    
    func customNavBar() {
        title = "All Shops"
        self.navigationItem.largeTitleDisplayMode = .never
        self.view.backgroundColor = blue
        subView.backgroundColor = .white
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        shopSearchBar.resignFirstResponder()
        if let text = shopSearchBar.text {
            self.shops = []
            DispatchQueue.main.async {
                self.shopListCollectionView.reloadData()
            }
            self.searchText = text
            getDataFromAPI(keyword: text, page: 0)
        }
    }
    
    func getDataFromAPI(keyword: String, page: Int) {
//        self.shops.removeAll()
        APIBisanara.shared.getListShop(page: page, limit: self.limit, keyword: keyword ) { (status, response) in
            if response != nil {
                if response!["status"].stringValue == "Success" {
                    
                    // parsong data from json
                    let json = JSON(response!)
                    let dataDictionary = json["data"].dictionaryValue
                    
                    let totalData = dataDictionary["total"]?.intValue
                    self.totalItem = totalData ?? 0
                    
                    self.lastPage = dataDictionary["last_page"]!.intValue
                                        
                    if let array = dataDictionary["data"]?.arrayValue {
//                        print("array: \(array)")
                        for data in array {
                            
                            let shop = ShopData(response: data)
                            self.shops.append(shop)
                            DispatchQueue.main.async {
                                self.shopListCollectionView.reloadData()
                            }
                        }
                    }
                    
                } else {
                    let alertFail = UIAlertController(title: "Error", message: response!["message"].stringValue, preferredStyle: .alert)
                    let okFail = UIAlertAction(title: "Try Again", style: .default) { (action) in
                        self.getDataFromAPI(keyword: self.searchText ?? "", page: 1)
                    }
                    alertFail.addAction(okFail)
                    self.present(alertFail, animated: false, completion: nil)
                }
                
            } else {
                let alertFail = UIAlertController(title: "Error", message: "json result from api is nill", preferredStyle: .alert)
                let okFail = UIAlertAction(title: "Try Again", style: .default) { (action) in
                    self.getDataFromAPI(keyword: self.searchText ?? "", page: 1)
                }
                alertFail.addAction(okFail)
                self.present(alertFail, animated: false, completion: nil)
                
            }
        }
    }
    
}

extension AllShopViewController: UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return shops.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ShopCardCollectionViewCell.identifier, for: indexPath) as! ShopCardCollectionViewCell
        cell.setup(shop: shops[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if shops.indices.contains(indexPath.row) {
            print(".... \(shops[indexPath.row].slug ?? "")")
            let controller = ShopDetailViewController.instantiateFromShopStoryboard()
            controller.shopSlug = shops[indexPath.row].slug ?? ""
            navigationController?.pushViewController(controller, animated: true)
        } else {
            print(".... indexpath nill")
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print("on scrolling")
        let position = scrollView.contentOffset.y
        if position > (shopListCollectionView.contentSize.height-50-scrollView.frame.size.height) {
            if self.page == self.lastPage {
                print("all data loaded")
            } else {
                self.page+=1
                self.getDataFromAPI(keyword: self.searchText ?? "", page: self.page)
                print("all shop: \(shops.count)")
            }
        }
    }
    
}
