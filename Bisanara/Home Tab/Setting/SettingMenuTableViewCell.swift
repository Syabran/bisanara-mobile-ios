//
//  SettingMenuTableViewCell.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 08/11/21.
//

import UIKit

class SettingMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var settingTitleLabel: UILabel!
    
    static let identifier = String(describing: SettingMenuTableViewCell.self)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(sett: Setting) {
        settingTitleLabel.text = sett.title
    }
    
}
