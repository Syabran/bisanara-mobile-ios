//
//  ManageShopVC.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 15/11/21.
//

import Foundation
import UIKit
import SwiftyJSON

// MANAGE SHOP
class ShopListVC: UIViewController {
    
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var shopCollectionView: UICollectionView!
    
    var shopList = [ShopData]()
    
//    var userId = ""
    var slug = ""
    var page = 0
    var limit = 0
    
    var blue = UIColor(red: 73.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customNavBar()
        registerCell()
        
        if APIBisanara.shared.Headers["Authorization"] != nil {
            getDataFromAPI()
            
        } else {
            self.tabBarController?.selectedIndex = 2
        }
    }
    
    func getDataFromAPI() {
        if APIBisanara.shared.Headers["Authorization"] != "" {
            APIBisanara.shared.getListShopOwner { (status, response) in
                let json = JSON(response!)
                if json["status"].stringValue == "Success" {
                    
                    // parsing data from json
                    let dataDictionary = json["data"].dictionaryValue
                                        
                    if let array = dataDictionary["data"]?.arrayValue {
    //                        print("array: \(array)")
                        for data in array {
                            
                            let shop = ShopData(response: data)
                            self.shopList.append(shop)
                            DispatchQueue.main.async {
                                self.shopCollectionView.reloadData()
                            }
                        }
                    }
                    
                } else {
                    let alertFail = UIAlertController(title: "Try again later", message: "sorry, please close the app and try re-open Bisanara. thank you", preferredStyle: .alert)
                    let okFail = UIAlertAction(title: "OK", style: .default) { (action) in
//                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
//                        self.navigationController?.pushViewController(vc!, animated: true)
                        self.performSegue(withIdentifier: "unwindToHome", sender: self)
                    }
                    alertFail.addAction(okFail)
                    self.present(alertFail, animated: false, completion: nil)
                }
            }
            
        } else {
            let alertFail = UIAlertController(title: "Login Required", message: "sorry, this feature only for login, you need to login on Profile tab", preferredStyle: .alert)
            let okFail = UIAlertAction(title: "OK", style: .default) { (action) in
//                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
//                self.navigationController?.pushViewController(vc!, animated: true)
                self.performSegue(withIdentifier: "unwindToHome", sender: self)
            }
            alertFail.addAction(okFail)
            self.present(alertFail, animated: false, completion: nil)
            
        }
        
    }
    
    func customNavBar() {
        title = "Manage Shops"
        self.view.backgroundColor = blue
        subView.backgroundColor = .white
        self.navigationItem.largeTitleDisplayMode = .never
    }
    
    private func registerCell() {
        shopCollectionView.register(UINib(nibName: ManageListShopCollectionViewCell.identifier, bundle: nil), forCellWithReuseIdentifier: ManageListShopCollectionViewCell.identifier)
    }
    
//    @IBAction func addShopTapped(_ sender: Any) {
//        let vc = UIStoryboard(name: "ManageShop", bundle: nil).instantiateViewController(withIdentifier: "EditShopVC") as? EditShopVC
//        self.navigationController?.pushViewController(vc!, animated: true)
//    }
}

extension ShopListVC: UICollectionViewDelegate,UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return shopList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ManageListShopCollectionViewCell.identifier, for: indexPath) as! ManageListShopCollectionViewCell
        cell.setup(shop: shopList[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let controller = ShopDetailVC.instantiateFromManageShopStoryboard()
        controller.shopSlug = shopList[indexPath.row].slug ?? ""
        navigationController?.pushViewController(controller, animated: true)
    }
    
}
