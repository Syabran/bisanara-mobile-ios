//
//  AboutBisanaraVC.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 20/12/21.
//

import Foundation
import UIKit
import WebKit

class AboutBisanaraVC: UIViewController {
    
    @IBOutlet weak var forWebView: UIView!
    
    var blue = UIColor(red: 73.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0)
    let webView = WKWebView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customNavBar()
        forWebView.addSubview(webView)
        
        guard let url = URL(string: "https://new.bisanara.com/about") else {
            return
        }
        webView.load(URLRequest(url: url))
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        webView.frame = view.bounds
    }
    
    func customNavBar() {
        title = "About BISANARA"
        self.view.backgroundColor = blue
        self.navigationItem.largeTitleDisplayMode = .never
    }
}
