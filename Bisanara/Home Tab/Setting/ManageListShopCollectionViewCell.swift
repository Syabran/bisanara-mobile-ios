//
//  ManageListShopCollectionViewCell.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 15/11/21.
//

import UIKit

class ManageListShopCollectionViewCell: UICollectionViewCell {

    static let identifier = String(describing: ManageListShopCollectionViewCell.self)
    
    @IBOutlet weak var shopNameLbl: UILabel!
    @IBOutlet weak var shopImage: UIImageView!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var activeShopLbl: UILabel!
    @IBOutlet weak var view: UIView!
    
    var blue = UIColor(red: 73.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setup(shop: ShopData) {
        
        self.view.layer.borderColor = UIColor.gray.cgColor
        self.view.layer.borderWidth = 1
        self.view.layer.cornerRadius = 10
        
        shopNameLbl.text = shop.name ?? ""
        
        activeShopLbl.text = "Active" // TEMPORARY
        if activeShopLbl.text == "Active" {
            activeShopLbl.textColor = blue
        } else {
            activeShopLbl.textColor = .red
        }
        
        descLbl.text = shop.description ?? ""
        addressLbl.text = shop.address ?? ""
        shopImage.urlLoad(urlString: shop.logo ?? "")
        
    }

}
