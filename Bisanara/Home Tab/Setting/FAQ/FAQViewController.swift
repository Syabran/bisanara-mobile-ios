//
//  FAQViewController.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 20/12/21.
//

import Foundation
import UIKit
import SwiftyJSON

class FAQViewController: UIViewController {
    
    @IBOutlet weak var faqListStackView: UIStackView!
    
    var blue = UIColor(red: 73.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0)
    
    var dataFAQ = [FAQ]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customNavBar()
        getFAQ()
    }
    
    func getFAQ() {
        APIBisanara.shared.getFAQ() { (status, response) in
            if status == true {
                let json = JSON(response!)
                
                if json["status"].stringValue == "success" {
                    let data = json["data"].arrayValue
                    for item in data {
                        let faq = FAQ(response: item)
                        
                        DispatchQueue.main.async {
                            self.faqListStackView.reloadInputViews()
                            
                            if let card = UINib(nibName: String(describing: FAQView.self), bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? FAQView {
                                self.faqListStackView.addArrangedSubview(card)
                                
                                card.questionLbl.text = (faq.question ?? "").trimHTMLTags()
                                card.answerLbl.text = (faq.answer ?? "").trimHTMLTags()
                                
                                card.eachFAQStackView.layer.cornerRadius = 10
                                card.eachFAQStackView.layer.borderColor = UIColor.lightGray.cgColor
                                card.eachFAQStackView.layer.borderWidth = 1
                                
                                card.questionView.layer.cornerRadius = 10
                                card.questionView.layer.borderWidth = 1
                                card.questionView.layer.borderColor = UIColor.lightGray.cgColor
                                
                                card.answerView.isHidden = true
                                
                                card.segueArrowBtn = {
                                    if card.arrowBtn.currentImage == card.img {
                                        card.arrowBtn.setImage(UIImage(systemName: "chevron.up"), for: .normal)
                                        card.answerView.isHidden = false
                                    } else {
                                        card.arrowBtn.setImage(UIImage(systemName: "chevron.down"), for: .normal)
                                        card.answerView.isHidden = true
                                    }
                                }
                                
                                card.segueTitleBtn = {
                                    if card.arrowBtn.currentImage == card.img {
                                        card.arrowBtn.setImage(UIImage(systemName: "chevron.up"), for: .normal)
                                        card.answerView.isHidden = false

                                    } else {
                                        card.arrowBtn.setImage(UIImage(systemName: "chevron.down"), for: .normal)
                                        card.answerView.isHidden = true
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func customNavBar() {
        title = "Frequently Asked Question"
        self.view.backgroundColor = blue
        self.navigationItem.largeTitleDisplayMode = .never
    }

}
