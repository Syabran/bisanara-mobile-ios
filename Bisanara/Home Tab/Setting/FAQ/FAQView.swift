//
//  FAQView.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 12/05/22.
//

import UIKit

class FAQView: UIView {

    @IBOutlet weak var questionView: UIView!
    @IBOutlet weak var questionLbl: UILabel!
    @IBOutlet weak var arrowBtn: UIButton!
    @IBOutlet weak var answerView: UIView!
    @IBOutlet weak var answerLbl: UILabel!
    @IBOutlet weak var eachFAQStackView: UIStackView!
    
    var segueArrowBtn: (() -> Void)?
    var segueTitleBtn: (() -> Void)?
    
    var img = UIImage(systemName: "chevron.down") //chevron pr arrow

    @IBAction func arrowTapped(_ sender: Any) {
        self.segueArrowBtn?()
    }
    
    
    @IBAction func titleTapped(_ sender: Any) {
        self.segueTitleBtn?()
    }
}
