//
//  SettingsViewController.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 08/11/21.
//

import UIKit

class SettingsViewController: UIViewController {
    
    var blue = UIColor(red: 73.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customNavBar()
    }
    
    func customNavBar() {
//        title = "Settings"
        self.view.backgroundColor = blue
        self.navigationItem.largeTitleDisplayMode = .never
    }
    
}
