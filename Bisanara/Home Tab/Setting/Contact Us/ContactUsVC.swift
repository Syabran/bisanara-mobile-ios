//
//  ContactUsVC.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 20/12/21.
//

import Foundation
import UIKit

class ContactUsVC: UIViewController {
    
    @IBOutlet weak var messageTxtView: UITextView!
    
    var blue = UIColor(red: 73.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customNavBar()
        
        messageTxtView.layer.borderWidth = 0.5
        messageTxtView.layer.borderColor = UIColor.lightGray.cgColor
        messageTxtView.layer.cornerRadius = 5
        messageTxtView.text = ""
    }
    
    func customNavBar() {
        title = "About BISANARA"
        self.view.backgroundColor = blue
        self.navigationItem.largeTitleDisplayMode = .never
    }
}
