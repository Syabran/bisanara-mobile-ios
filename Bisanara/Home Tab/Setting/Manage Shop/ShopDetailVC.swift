//
//  ManageProductVC.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 15/11/21.
//

import Foundation
import UIKit
import SwiftyJSON
import Kingfisher

// FOR SHOP OWNER
class ShopDetailVC: UIViewController, UIPickerViewDelegate {
    
    @IBOutlet weak var bannerImage: UIImageView!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var businessNameLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var generalView: UIView!
    @IBOutlet weak var cpEmailLbl: UILabel!
    @IBOutlet weak var contactNumberLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    
    @IBOutlet weak var shopLinkStackView: UIStackView!
    @IBOutlet weak var noneSosmedView: UIView!
    
    @IBOutlet weak var memberListStackView: UIStackView!
    @IBOutlet weak var noneMemberView: UIView!
    
    @IBOutlet weak var productBtn: CustomButton!
    
    var imagePicked: UIImageView?
    var isLogo = false
    var isBanner = false
    var getDataBanner: Data? = nil
    var getDataLogo: Data? = nil
    
    var blue = UIColor(red: 73.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0)
    
//    var shopDetail = [ShopDetail]()
    var shopSlug = ""
    var shopId = ""
    
    let userId = UserDefaults.standard.value(forKey: "UserId") as? String ?? ""
    var tnc = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.getDataBanner != nil {
            setGradientBackground()
        }
//        getDataShop()
        getDataTNC()
        customNavBar()
        customView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        for view in shopLinkStackView.arrangedSubviews{
              shopLinkStackView.removeArrangedSubview(view)
              view.removeFromSuperview()
        }
        for view in memberListStackView.arrangedSubviews{
              memberListStackView.removeArrangedSubview(view)
              view.removeFromSuperview()
        }
        getDataShop()
    }
    
    private func customView() {
        // for general view
        generalView.layer.borderWidth = 0.5
        generalView.layer.borderColor = UIColor.lightGray.cgColor
        generalView.layer.cornerRadius = 5
        logoImage.layer.cornerRadius = 5
    }
    
    func setGradientBackground() {
        let colorTop =  UIColor(red: 0.0/255.0, green: 73.0/255.0, blue: 141.0/255.0, alpha: 1.0).cgColor
        let colorMid =  UIColor(red: 0.0/255.0, green: 38.0/255.0, blue: 77.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 0.0/255.0, green: 23.0/255.0, blue: 45.0/255.0, alpha: 1.0).cgColor
                    
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorMid, colorBottom]
        gradientLayer.locations = [0.0, 0.5, 1.0]
        gradientLayer.frame = self.bannerImage.bounds
                
        self.bannerImage.layer.insertSublayer(gradientLayer, at:0)
    }
    
    func getDataShop() {
        APIBisanara.shared.getDetailShopOwner(slug: self.shopSlug) { (status, response) in
            let json = JSON(response!)
            
            if json["status"].stringValue == "success" {
                let shop = ShopData(response: json["data"])
                
                self.shopId = shop.id_shop ?? ""
                self.businessNameLbl.text = shop.name ?? ""
                self.descriptionLbl.text = shop.description ?? ""
                self.addressLbl.text = shop.address ?? ""
                self.contactNumberLbl.text = shop.phone ?? ""
                self.cpEmailLbl.text = shop.email ?? ""
                
                let logo = shop.logo ?? ""
                self.logoImage.kf.setImage(with: logo.asUrl)
                
                let banner = shop.banner ?? ""
//                self.bannerImage.urlLoad(urlString: banner)
                self.bannerImage.kf.setImage(with: banner.asUrl)
                
                let data = json["data"].dictionaryValue
                
                // get owner data
                if let owner = data["owners"]?.arrayValue {
                    for data in owner {
                        let owner = Owner(response: data)
//                        self.member.append(owner)
                        DispatchQueue.main.async {
                            self.memberListStackView.reloadInputViews()
                            
                            if let card = UINib(nibName: String(describing: MemberShopView.self), bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? MemberShopView {
                                self.memberListStackView.addArrangedSubview(card)
                                
                                card.nameLbl.text = owner.name ?? ""
                                card.addressLbl.text = owner.address ?? ""
                                card.phoneLbl.text = owner.phoneNumber ?? ""
                                card.statusLbl.text = owner.statusDesc ?? ""
                                if card.statusLbl.text == "confirmed" {
                                    card.statusLbl.textColor = .systemGreen
                                } else { card.statusLbl.textColor = .orange }
                                
                                card.memberView.layer.cornerRadius = 5
                                card.memberView.layer.borderWidth = 0.5
                                card.memberView.layer.borderColor = UIColor.lightGray.cgColor
                                
                                card.segueButtonConfirm = {
                                    print("user id: \(self.userId) & \(owner.userId ?? "")")
                                    if self.userId == owner.userId ?? "" {
                                        
                                        let alertTnc = UIAlertController(title: "TNC", message: self.tnc ,preferredStyle: .alert)
                                        let ok = UIAlertAction(title: "Agree", style: .default) { (action) in
                                            
                                            // confirm agree tnc
                                            APIBisanara.shared.memberConfirmation(slug: self.shopSlug) { status in
                                                if status == true {
                                                    let alertSuccess = UIAlertController(title: "", message: "Successfully confirm member", preferredStyle: .alert)
                                                    let okSuccess = UIAlertAction(title: "OK", style: .default) { (action) in
                                                        self.getDataShop()                                                    }
                                                    alertSuccess.addAction(okSuccess)
                                                    self.present(alertSuccess, animated: true, completion: nil)
                                                }
                                            }
                                        }
                                        
                                        let cancel = UIAlertAction(title: "cancel", style: .default)
                                        alertTnc.addAction(cancel)
                                        alertTnc.addAction(ok)
                                        self.present(alertTnc, animated: true, completion: nil)
                                    }
                                }
                            }
                            self.noneMemberView.isHidden = true
                        }
                    }
                }
                
                // get social media data
                if let socialMedia = data["social_media"]?.arrayValue {
                    for data in socialMedia {
                        let sosmed = ShopSosmed(response: data)
//                        self.link.append(sosmed)
                        
                        DispatchQueue.main.async {
                            self.shopLinkStackView.reloadInputViews()
                            
                            if let card = UINib(nibName: String(describing: OtherPlatformLinkView.self), bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? OtherPlatformLinkView {
                                self.shopLinkStackView.addArrangedSubview(card)
                                
                                let iconSosmed = sosmed.logo ?? ""
                                card.iconImage.kf.setImage(with: iconSosmed.asUrl)
                                
                                card.sosmedId = sosmed.id ?? ""
                                card.urlLbl.text = sosmed.url ?? ""
                                card.clickLbl.text = "\(sosmed.click ?? "") click"
                                
                                card.platformView.layer.cornerRadius = 5
                                card.platformView.layer.borderWidth = 0.5
                                card.platformView.layer.borderColor = UIColor.lightGray.cgColor
                                
                                card.segueButtonEditSosmed = {
                                    let vc = UIStoryboard(name: "ManageShop", bundle: nil).instantiateViewController(withIdentifier: "LinkShopFormVC") as? SosmedFormVC
                                    self.navigationController?.pushViewController(vc!, animated: true)
                                    vc!.shopSlug = self.shopSlug
                                    vc!.sosmedId = card.sosmedId
                                }
                            }
                            self.noneSosmedView.isHidden = true
                        }
                    }
                }
                
            }
        }
        
        statusLbl.text = "confirmed"
        if statusLbl.text == "confirmed" {
            statusLbl.textColor = .systemGreen
        } else {
            statusLbl.textColor = .red
        }
        
    }
    
    func getDataTNC() {
        APIBisanara.shared.getTncShopOwner { (status, response) in
            if response != nil {
                if response!["status"].stringValue == "success" {
                    let json = JSON(response!)
                    let tnc = json["data"].stringValue
                    self.tnc = tnc.trimHTMLTags()!
                }
            }
        }
    }
    
    func customNavBar() {
        self.view.backgroundColor = blue
        self.navigationItem.largeTitleDisplayMode = .never
    }
    
    @IBAction func editBannerTapped(_ sender: Any) {
        if self.bannerImage != nil {
            self.imagePicked = self.bannerImage
            self.isBanner = true
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
                imagePicker.allowsEditing = true
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func editLogoTapped(_ sender: Any) {
        if self.logoImage != nil {
            self.imagePicked = self.logoImage
            self.isLogo = true
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
                imagePicker.allowsEditing = true
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
    }
    
    
    // EDIT GENERAL DATA
    @IBAction func GeneralTapped(_ sender: Any) {
        let vc = UIStoryboard(name: "ManageShop", bundle: nil).instantiateViewController(withIdentifier: "GeneralShopFormVC") as? GeneralShopFormVC
//        vc?.dataShop = self.shopDetail
        self.navigationController?.pushViewController(vc!, animated: true)
        vc!.urlSlug = self.shopSlug
    }
    
    // ADD SOCIAL MEDIA
    @IBAction func addLinkTapped(_ sender: Any) {
        let vc = UIStoryboard(name: "ManageShop", bundle: nil).instantiateViewController(withIdentifier: "LinkShopFormVC") as? SosmedFormVC
        self.navigationController?.pushViewController(vc!, animated: true)
        vc!.shopSlug = self.shopSlug
    }
    
    @IBAction func manageProductTapped(_ sender: Any) {
        let vc = UIStoryboard(name: "ManageProduct", bundle: nil).instantiateViewController(withIdentifier: "ProductListVC") as? ProductListVC
        self.navigationController?.pushViewController(vc!, animated: true)
        vc!.shopSlug = self.shopSlug
    }
    
}

extension ShopDetailVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    // for logo image picker
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerEditedImage")] as? UIImage {
            self.imagePicked?.image = image
            if self.isBanner == true {
                self.getDataBanner = image.jpegData(compressionQuality: 0.5)
                print("data banner: \(String(describing: self.getDataBanner))")
                
                if self.getDataBanner != nil {
                    APIBisanara.shared.changeBannerShop(slug: self.shopSlug, file: self.getDataBanner) { (status) in
                        if status == true {
                            let alertSuccess = UIAlertController(title: "Success!", message: "Success save your banner image",preferredStyle: .alert)
                            let ok = UIAlertAction(title: "OK", style: .default)
                            alertSuccess.addAction(ok)
                            self.present(alertSuccess, animated: true, completion: nil)
                            
                        } else {
                            print("failed save banner")
                            
                        }
                    }
                } else { print("data banner nil")}
                self.isBanner =  false
                
            } else if self.isLogo == true {
                self.getDataLogo = image.jpegData(compressionQuality: 0.5)
                print("data logo: \(String(describing: self.getDataLogo))")
                
                if self.getDataLogo != nil {
                    APIBisanara.shared.changeLogoShop(slug: self.shopSlug, file: self.getDataLogo) { (status) in
                        if status == true {
                            let alertSuccess = UIAlertController(title: "Success!", message: "Success save your logo image",preferredStyle: .alert)
                            let ok = UIAlertAction(title: "OK", style: .default)
                            alertSuccess.addAction(ok)
                            self.present(alertSuccess, animated: true, completion: nil)
                            
                        } else {
                            print("failed save logo")
                            
                        }
                    }
                } else { print("data logo nil")}
                self.isLogo = false
            }
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
}
