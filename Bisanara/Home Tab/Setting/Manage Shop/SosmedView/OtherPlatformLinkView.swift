//
//  OtherPlatformLinkView.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 09/12/21.
//

import UIKit

class OtherPlatformLinkView: UIView {

    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var urlLbl: UILabel!
    @IBOutlet weak var clickLbl: UILabel!
    @IBOutlet weak var platformView: UIView!
    
    var segueButtonEditSosmed: (() -> Void)?
    
    var sosmedId = ""
    
    
    
    @IBAction func editTapped(_ sender: Any) {
        self.segueButtonEditSosmed?()
    }
}
