//
//  WriteReviewVC.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 22/11/21.
//

import UIKit
import SwiftyJSON

// LIST OF REVIEW PRODUCT
// storyboard at: home tab-setting-manage shop-manage product storyboard
class ReviewProductVC: UIViewController {
    
    @IBOutlet weak var reviewScrollView: UIScrollView!
    @IBOutlet weak var reviewsStackView: UIStackView!
    @IBOutlet weak var makeReviewBtn: UIButton!
    @IBOutlet weak var makeReportBtn: UIButton!
    @IBOutlet weak var MakeReviewReportView: UIView!
    @IBOutlet weak var noneReviewView: UIView!
    
    var productId = ""
    var productSlug = ""
    var page = 1
    var lastPage = 0
    
    var isOwner: Bool?
    
    var blue = UIColor(red: 73.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customNavBar()
        getReviewsData()
        customView()
        scrollViewDidScroll(reviewScrollView)
    }
    
    func customView() {
        self.makeReportBtn.tintColor = .white
        self.makeReportBtn.layer.cornerRadius = 20
        self.makeReviewBtn.tintColor = .white
        self.makeReviewBtn.layer.cornerRadius = 20
        
        if self.isOwner == true {
            self.MakeReviewReportView.isHidden = true
        } else { self.MakeReviewReportView.isHidden = false }
    }
    
    func getReviewsData() {
        APIBisanara.shared.getReviewByProduct(productSlug: productSlug, page: page) { (status, response) in
            if response != nil {
                if response!["status"].stringValue == "success" {
                    let json = JSON(response!)
                    let dataDict = json["data"].dictionaryValue
                    
                    if let dataArr = dataDict["data"]?.arrayValue {
                        for data in dataArr {
                            let review = Review(response: data)
                            
                            DispatchQueue.main.async {
                                self.reviewsStackView.reloadInputViews()
                                if let cell = UINib(nibName: String(describing: ReviewCellView.self), bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? ReviewCellView { // cell in Reusable CustomView
                                    self.reviewsStackView.addArrangedSubview(cell)
                                    
                                    cell.reviewId = review.reviewId ?? ""
                                    cell.nameLbl.text = review.name ?? ""
                                    cell.emailLbl.text = review.email ?? ""
                                    cell.feedbackLbl.text = review.reviewComment ?? ""
                                    cell.typeReviewerLbl.text = review.typeReviewer ?? ""
                                    
                                    cell.cardView.layer.cornerRadius = 5
                                    cell.cardView.layer.borderWidth = 0.5
                                    cell.cardView.layer.borderColor = UIColor.lightGray.cgColor
                                    
                                    let mediaArr = data["media"].arrayValue
                                    for media in mediaArr {
                                        let media = MediaReview(response: media)
                                        
                                        if media.attachId != "" {
                                            DispatchQueue.main.async {
                                                self.reviewsStackView.reloadInputViews()
                                                if let mediaView = UINib(nibName: String(describing: ReviewMediaView.self), bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? ReviewMediaView {
                                                    cell.feedbackImageStackView.addArrangedSubview(mediaView)
                                                    
                                                    mediaView.mediaId = media.attachId ?? ""
                                                    
                                                    let image = media.path ?? ""
                                                    mediaView.mediaImageView.kf.setImage(with: image.asUrl)
                                                    if mediaView.mediaImageView.image == nil {
                                                        mediaView.mediaImageView.image = UIImage(systemName: "cube.box")
                                                    }
                                                    
                                                    mediaView.mediaImageView.layer.cornerRadius = 8
                                                    
                                                    mediaView.segueViewButton = {
                                                        // to see the image with modal view
                                                        let cellImg = UIStoryboard(name: "ReviewProduct", bundle: nil).instantiateViewController(withIdentifier: "ReviewProductModalVC") as? ReviewProductModalVC
                                                        self.present(cellImg!, animated: true, completion: nil)
                                                        
                                                        cellImg?.bigImageView.kf.setImage(with: image.asUrl, placeholder: UIImage(systemName: "cube.box"))
                                                        
                                                        cellImg?.closeButton = {
                                                            cellImg?.dismiss(animated: true)
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            cell.mediaView.isHidden =  true
                                        }
                                    }
                                }
                                self.noneReviewView.isHidden = true
                            }
                        }
                    }
                    
                    let lastPage = dataDict["last_page"]?.intValue
                    self.lastPage = lastPage!
                    print("lastpage= \(self.lastPage)")
                    
                } else {
                    let alertFail = UIAlertController(title: "", message: "Sorry, Reviews data product is failed to load, try again later or kindly contact us", preferredStyle: .alert)
                    let okFail = UIAlertAction(title: "Try Again", style: .default) { (action) in
                        self.getReviewsData()
                    }
                    let cancel = UIAlertAction(title: "cancel", style: .default)
                    alertFail.addAction(okFail)
                    alertFail.addAction(cancel)
                    self.present(alertFail, animated: false, completion: nil)
                }
                
            } else {
                let alertFail = UIAlertController(title: "", message: "Sorry, Reviews data product is failed to load, try again later or kindly contact us", preferredStyle: .alert)
                let okFail = UIAlertAction(title: "Try Again", style: .default) { (action) in
                    self.getReviewsData()
                }
                let cancel = UIAlertAction(title: "cancel", style: .default)
                alertFail.addAction(okFail)
                alertFail.addAction(cancel)
                self.present(alertFail, animated: false, completion: nil)
            }
        }
    }
    
    func customNavBar() {
        title = "Product Review"
        self.view.backgroundColor = blue
        self.navigationItem.largeTitleDisplayMode = .never
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print("on scrolling")
        if page == lastPage {
            print("all review loaded")
        } else {
            self.page+=1
            getReviewsData()
            print("page=\(self.page)")
        }
    }
    
    @IBAction func reviewTapped(_ sender: Any) {
        let vc = UIStoryboard(name: "ProductView", bundle: nil).instantiateViewController(withIdentifier: "WriteReviewVC") as? WriteReviewVC
        self.navigationController?.pushViewController(vc!, animated: true)
        vc?.productId = self.productId
    }
    
    @IBAction func reportTapped(_ sender: Any) {
        let vc = UIStoryboard(name: "ProductView", bundle: nil).instantiateViewController(withIdentifier: "CreateReportVC") as? CreateReportVC
        self.navigationController?.pushViewController(vc!, animated: true)
        vc?.productId = self.productId
    }
}
