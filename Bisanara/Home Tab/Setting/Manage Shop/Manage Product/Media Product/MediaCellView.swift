//
//  MediaCellView.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 24/03/22.
//

import UIKit

class MediaCellView: UIView {
    
    @IBOutlet weak var mediaImageView: UIImageView!
    @IBOutlet weak var markImageView: UIImageView!
    @IBOutlet weak var coverLbl: UILabel!
    @IBOutlet weak var cellView: UIView!
    
    var mediaId = ""
    var productId = ""
    var segueDetailMedia: (() -> Void)?
    
    @IBAction func mediaCellTapped(_ sender: Any) {
        self.segueDetailMedia?()
    }
}
