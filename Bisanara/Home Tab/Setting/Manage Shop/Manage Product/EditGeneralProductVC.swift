//
//  EditGeneralProductVC.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 25/11/21.
//

import UIKit
import SwiftyJSON

class EditGeneralProductVC: UIViewController {
    
    @IBOutlet weak var categoryTxtField: UITextField!
    @IBOutlet weak var subCategoryTxtField: UITextField!
    @IBOutlet weak var nameTxtField: UITextField!
    @IBOutlet weak var priceTxtField: UITextField!
    @IBOutlet weak var descriptionTxtView: UITextView!
    @IBOutlet weak var specTxtView: UITextView!
    @IBOutlet weak var conditionTxtField: UITextField!
    @IBOutlet weak var saveView: UIView!
    @IBOutlet weak var addView: UIView!
    private var pickerCategory = UIPickerView()
    private var pickerSubCategory = UIPickerView()
    private var pickerCondition = UIPickerView()
    
    var dataCategory: [ModelCategory]? = []
    var categoryName: [String]? = []
    var dataSubCategory: [ModelCategory]? = []
    var subCategoryName: [String]? = []
    var dataCondition = ["new", "used"]
    
    var productSlug = ""
    var getParentCategory = "0"
    var sendCategoryId = 0
    
    private var dispatchGroup = DispatchGroup()
    
    var blue = UIColor(red: 73.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        descriptionTxtView.layer.borderWidth = 0.5
        descriptionTxtView.layer.borderColor = UIColor.lightGray.cgColor
        descriptionTxtView.layer.cornerRadius = 5
        descriptionTxtView.text = ""
        
        specTxtView.layer.borderWidth = 0.5
        specTxtView.layer.borderColor = UIColor.lightGray.cgColor
        specTxtView.layer.cornerRadius = 5
        specTxtView.text = ""
        
        getDataFromAPI()
        getCategory() // picker
//        getSubCategory()
        customNavBar()
        hideKeyboardWhenTappedAround()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        print("slug: \(productSlug)")
        addPickerCategory()
        addPickerCondition()
    }
    
    func getDataFromAPI() {
        if self.productSlug != "" {
            self.addView.isHidden = true
            APIBisanara.shared.getDetailProductOwner(productSlug: self.productSlug) { (status, response) in
                if status == true {
                    let json = JSON(response!)
                    
                    if json["status"].stringValue == "success" {
                        let dataDict = json["data"].dictionaryValue
                        
                        let product = ProductOwner(response: dataDict["product"]!)
                        
                        print("name:\(product.name ?? "") & id:\(product.productId ?? "")")
                        self.nameTxtField.text = product.name ?? ""
                        self.priceTxtField.text = product.price ?? ""
                        self.conditionTxtField.text = product.condition ?? ""
//                        self.transactionIdLbl.text = product.productTransactionId ?? ""
                        
                        let desc = product.description ?? ""
                        self.descriptionTxtView.text = desc.trimHTMLTags()
                        
                        let spec = product.spec ?? ""
                        self.specTxtView.text = spec.trimHTMLTags()
                        
                        let productDict = dataDict["product"]?.dictionaryValue
                        
                        // get category data
                        let category = ProductCategory(response: productDict!["category"]!)
                        DispatchQueue.main.async {
                            self.subCategoryTxtField.text = category.name ?? ""
                            self.sendCategoryId = category.categoryProductId ?? 0
                            self.getParentCategory = category.idParentCategory ?? ""
                            
                            // get parents name
                            self.getCategory()
                        }
                    }
                    
                } else {
                    let error = JSON(response!)
                    print("status false api: \(error)")
                }
            }
        } else {
            print("product slug nil, \(self.productSlug)")
            self.saveView.isHidden = true
        }
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
    }
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    func customNavBar() {
        title = ""
        self.view.backgroundColor = blue
        self.navigationItem.largeTitleDisplayMode = .never
    }
    
    // FOR PICKER PARENT CATEGORY
    func getCategory() {
        dataCategory = []
        categoryName = []
        dispatchGroup.enter()
        APIBisanara.shared.getCategories(idCategory: "0") { (status, response) in
            
            if status == true {
                for item in response!["data"].arrayValue {
                    self.dataCategory?.append(ModelCategory(response: item))
                    self.categoryName?.append(item["name"].stringValue)
                    
                    let parent = item["id_category"].intValue
                    if self.getParentCategory == "\(parent)" {
                        self.categoryTxtField.text = item["name"].stringValue
                        self.getSubCategory()
                        print("\(self.getParentCategory) === \(parent)")
                    }
                }
            }
        }
        dispatchGroup.leave()
    }
    func addPickerCategory() {
        pickerCategory.delegate = self
        pickerCategory.dataSource = self
        categoryTxtField.inputView = pickerCategory
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let done = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(doneTappedCategory))
        toolbar.items = [flexible, done]
        
        categoryTxtField.inputView = pickerCategory
        categoryTxtField.inputAccessoryView = toolbar
    }
    @objc func doneTappedCategory() {
        self.view.endEditing(true)
        
        for item in dataCategory! {
            if categoryTxtField.text == item.name?.trimHTMLTags() {
                getParentCategory = "\(item.id!)"
                print("category picked: \(getParentCategory)")
                
                getSubCategory()
            }
        }
        self.subCategoryTxtField.text = ""
        addPickerSubCategory()
    }
    
    // FOR PICKER SUB CATEGORY
    func getSubCategory() {
        self.dataSubCategory = [ModelCategory]()
        self.subCategoryName = [String]()
        
        if self.getParentCategory != "0" {
            dispatchGroup.enter()
            APIBisanara.shared.getCategories(idCategory: self.getParentCategory) { (status, response) in
                
                if status == true {
                    for item in response!["data"].arrayValue {
                        self.dataSubCategory?.append(ModelCategory(response: item))
                        self.subCategoryName?.append(item["name"].stringValue)
                    }
                }
            }
            dispatchGroup.leave()
            
        } else {
            let alertFailed = UIAlertController(title: "", message: "please select first the category above to get sub category", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default) { (action) in
                self.getSubCategory()
            }
            alertFailed.addAction(ok)
            self.present(alertFailed, animated: true, completion: nil)
        }
    }
    func addPickerSubCategory() {
        pickerSubCategory.delegate = self
        pickerSubCategory.dataSource = self
        subCategoryTxtField.inputView = pickerSubCategory
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let done = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(doneTappedSubCategory))
        toolbar.items = [flexible, done]
        
        subCategoryTxtField.inputView = pickerSubCategory
        subCategoryTxtField.inputAccessoryView = toolbar
    }
    @objc func doneTappedSubCategory() {
        self.view.endEditing(true)
        
        for item in dataSubCategory! {
            if subCategoryTxtField.text == item.name?.trimHTMLTags() {
                sendCategoryId = 0
                sendCategoryId = item.id!
                print("sub category picked: \(sendCategoryId)")
            }
            if sendCategoryId == 0 {
                let alertFailed = UIAlertController(title: "", message: "please select Sub Category again", preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style: .default) { action in
                    self.subCategoryTxtField.text = ""
                }
                alertFailed.addAction(ok)
                self.present(alertFailed, animated: true, completion: nil)
            }
        }
    }
    
    // FOR PICKER CONDITION
    func addPickerCondition() {
        pickerCondition.delegate = self
        pickerCondition.dataSource = self
        conditionTxtField.inputView = pickerCondition
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let done = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(doneTappedCondition))
        toolbar.items = [flexible, done]
        
        conditionTxtField.inputView = pickerCondition
        conditionTxtField.inputAccessoryView = toolbar
    }
    @objc func doneTappedCondition() {
        self.view.endEditing(true)
        
        for item in dataCondition {
            if conditionTxtField.text == item {
                print("condition picked: \(conditionTxtField.text ?? "")")
            }
        }
    }
    
    @IBAction func addTapped(_ sender: Any) {
        if nameTxtField.text != "" && priceTxtField.text != "" && categoryTxtField.text != "" && subCategoryTxtField.text != "" && descriptionTxtView.text != "" && specTxtView.text != "" && conditionTxtField.text != "" {
            if self.sendCategoryId != 0 {
                if productSlug == "" {
                    // ADD NEW PRODUCT
                    APIBisanara.shared.addNewProduct(name: self.nameTxtField.text ?? "", price: self.priceTxtField.text ?? "", spec: self.specTxtView.text ?? "", desc: self.descriptionTxtView.text ?? "", category: "\(self.sendCategoryId)", condition: self.conditionTxtField.text ?? "") { (response) in
                        
                        let status = response!["status"].stringValue
                        if status == "Success" || status == "success" {
                            let alertSuccess = UIAlertController(title: "Added!", message: "Your new product are added", preferredStyle: .alert)
                            let okSuccess = UIAlertAction(title: "OK", style: .default) { (action) in
                                
                                // if go to detail after add new product
                                self.performSegue(withIdentifier: "unwindToListProductOwner", sender: self)
                            }
                            alertSuccess.addAction(okSuccess)
                            self.present(alertSuccess, animated: true, completion: nil)
                            
                        } else {
                            print("failed add product")
                            let alertFailed = UIAlertController(title: "Failed!", message: "sorry, failed to add new product, please try again later or kindly contact us", preferredStyle: .alert)
                            let ok = UIAlertAction(title: "OK", style: .default)
                            alertFailed.addAction(ok)
                            self.present(alertFailed, animated: true, completion: nil)
                        }
                    }
                    
                } else {
                    print("product slug not nill")
                }
            } else {
                let alertFailed = UIAlertController(title: "", message: "please choose again category of the product", preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style: .default) { action in
                    print("data product not fill in")
                }
                alertFailed.addAction(ok)
                self.present(alertFailed, animated: true, completion: nil)
            }
            
        } else {
            let alertFailed = UIAlertController(title: "", message: "please fill all needed data to add or update the product", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default) { action in
                print("data product not fill in")
            }
            alertFailed.addAction(ok)
            self.present(alertFailed, animated: true, completion: nil)
        }
    }
    @IBAction func saveTapped(_ sender: Any) {
        if nameTxtField.text != "" && priceTxtField.text != "" && categoryTxtField.text != "" && subCategoryTxtField.text != "" && descriptionTxtView.text != "" && specTxtView.text != "" && conditionTxtField.text != "" {
            if self.sendCategoryId != 0 {
                if productSlug != "" {
                    // UPDATE PRODUCT
                    APIBisanara.shared.updateProduct(slug: self.productSlug, name: self.nameTxtField.text ?? "", price: self.priceTxtField.text ?? "", spec: self.specTxtView.text ?? "", desc: self.descriptionTxtView.text ?? "", category: "\(self.sendCategoryId)", condition: self.conditionTxtField.text ?? "") { (response) in
                        
                        let status = response!["status"].stringValue
                        if status == "Success" || status == "success" {
                            let alertSuccess = UIAlertController(title: "Updated!", message: "Your product succesfully updated", preferredStyle: .alert)
                            let okSuccess = UIAlertAction(title: "OK", style: .default) { (action) in
                                
                                self.performSegue(withIdentifier: "unwindToProductOwner", sender: self)
                                
                                // if go to detail after update product
        //                        let vc = UIStoryboard(name: "ManageProduct", bundle: nil).instantiateViewController(withIdentifier: "ProductDetailVC") as? ProductDetailVC
                            }
                            alertSuccess.addAction(okSuccess)
                            self.present(alertSuccess, animated: true, completion: nil)
                            
                        } else {
                            print("failed update product")
                            let alertFailed = UIAlertController(title: "Failed!", message: "sorry, failed to update the product, please try again later or kindly contact us", preferredStyle: .alert)
                            let ok = UIAlertAction(title: "OK", style: .default)
                            alertFailed.addAction(ok)
                            self.present(alertFailed, animated: true, completion: nil)
                        }
                    }
                    
                } else {
                    print("product slug nill")
                }
            } else {
                let alertFailed = UIAlertController(title: "", message: "please choose again category of the product", preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style: .default) { action in
                    print("data product not fill in")
                }
                alertFailed.addAction(ok)
                self.present(alertFailed, animated: true, completion: nil)
            }
            
        } else {
            let alertFailed = UIAlertController(title: "", message: "please fill all needed data to add or update the product", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default) { action in
                print("data product not fill in")
            }
            alertFailed.addAction(ok)
            self.present(alertFailed, animated: true, completion: nil)
        }
    }
    
}

extension EditGeneralProductVC: UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView {
        case pickerCategory:
            return categoryName?.count ?? 0
            
        case pickerSubCategory:
            return subCategoryName?.count ?? 0
            
        case pickerCondition:
            return dataCondition.count
            
        default: return 0
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView {
        case pickerCategory:
            let name = categoryName?[row] ?? ""
            return name.trimHTMLTags()
            
        case pickerSubCategory:
            let name = subCategoryName?[row] ?? ""
            return name.trimHTMLTags()
            
        case pickerCondition:
            return dataCondition[row]
            
        default: return "Choose 1"
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == pickerCategory {
            let name = categoryName?[row]
            categoryTxtField.text = name?.trimHTMLTags()
            
        } else if pickerView == pickerSubCategory {
            if getParentCategory != "0" {
                let name = subCategoryName?[row]
                subCategoryTxtField.text = name?.trimHTMLTags()
            }
            
        } else if pickerView == pickerCondition {
            conditionTxtField.text = dataCondition[row]
        }
    }
}
