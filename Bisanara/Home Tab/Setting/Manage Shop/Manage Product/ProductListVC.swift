//
//  ProductListVC.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 22/11/21.
//

import UIKit
import SwiftyJSON

// MANAGE PRODUCT
class ProductListVC: UIViewController {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var productListCollectionView: UICollectionView!
    @IBOutlet weak var productSearchBar: UISearchBar!
    @IBOutlet weak var filterButton: CustomButton!
    
    @IBAction func unwindToListProductOwner(_ sender: UIStoryboardSegue) {
        getDataFromAPI()
    }
    
    var productList = [ProductOwner]()
    
    var shopSlug = ""
    var productSlug = ""
    var limit = 0
    
    var blue = UIColor(red: 73.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customNavBar()
        registerCell()
        getDataFromAPI()
        
        headerView.isHidden = true
    }
    
    func getDataFromAPI() {
        self.productList = [ProductOwner]()
        APIBisanara.shared.getListProductOwner(shopSlug: self.shopSlug) { (status, response) in
            let json = JSON(response!)
            if json["status"].stringValue == "Success" {
                
                // parsing data
                let dataDict = json["data"].dictionaryValue
                if let productDict = dataDict["product"]?.dictionaryValue {
                    if let array = productDict["data"]?.arrayValue {
                        for data in array {
                            let product = ProductOwner(response: data)
                            self.productList.append(product)
                            DispatchQueue.main.async {
                                self.productListCollectionView.reloadData()
                            }
                        }
                    }
                }
            } else {
                let alertFail = UIAlertController(title: "Error", message: "sorry, please try again later or kindly contact us", preferredStyle: .alert)
                let okFail = UIAlertAction(title: "OK", style: .default) { (action) in
                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
                    self.navigationController?.pushViewController(vc!, animated: true)
                }
                alertFail.addAction(okFail)
                self.present(alertFail, animated: false, completion: nil)
            }
        }
    }
    
    func customNavBar() {
        title = "Manage Products"
        self.view.backgroundColor = blue
        self.navigationItem.largeTitleDisplayMode = .never
    }
    
    private func registerCell() {
        productListCollectionView.register(UINib(nibName: ManageListProductCollectionViewCell.identifier, bundle: nil), forCellWithReuseIdentifier: ManageListProductCollectionViewCell.identifier)
    }
    
    @IBAction func addProductTapped(_ sender: Any) {
        let vc = UIStoryboard(name: "ManageProduct", bundle: nil).instantiateViewController(withIdentifier: "EditGeneralProductVC") as? EditGeneralProductVC
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}

extension ProductListVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ManageListProductCollectionViewCell.identifier, for: indexPath) as! ManageListProductCollectionViewCell
        cell.setup(product: productList[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let controller = ProductDetailVC.instantiateFromManageProductStoryboard()
        controller.productSlug = productList[indexPath.row].productSlug ?? ""
        navigationController?.pushViewController(controller, animated: true)
    }
}
