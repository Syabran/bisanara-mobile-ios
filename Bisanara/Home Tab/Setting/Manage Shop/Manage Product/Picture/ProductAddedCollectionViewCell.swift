//
//  ProductImageCollectionViewCell.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 19/04/22.
//

import UIKit
import Kingfisher

class ProductAddedCollectionViewCell: UICollectionViewCell {

    @IBOutlet var productImage: UIImageView!
    @IBOutlet var cellView: UIView!
    
    static let identifier = String(describing: "ImageProductAddedCollectionViewCell")
    
    var segueButtonDelete: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setup(item: ProductMedia) {
        print("isi media: \(item.pathUrl ?? "")")
        productImage.kf.setImage(with: (item.pathUrl ?? "").asUrl, placeholder: UIImage(systemName: "cube.box"))
    }

    @IBAction func deleteTapped(_ sender: Any) {
        self.segueButtonDelete?()
    }
}
