//
//  ProductImageCollectionViewCell.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 19/04/22.
//

import UIKit
import AVKit
import Kingfisher
import AVFoundation

class ProductImageCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var playImage: UIImageView!
    @IBOutlet weak var mediaView: UIView!
    
    static let identifier = String(describing: "ProductImageCollectionViewCell")
    
    var videoPlayer: AVPlayer? = nil
    let avPlayerVC = AVPlayerViewController()
    var path: URL?
    var isVideo: Bool?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setup(item: ProductMedia) {
        path = (item.pathUrl ?? "").asUrl
        print("isi media: \(String(describing: path)), type: \(item.mediaType ?? "")")
        
        if item.mediaType == "mp4" || item.mediaType == "avi" || item.mediaType == "mov" || item.mediaType == "mkv" || item.mediaType == "webm" || item.mediaType == "wmv" {
            print("videoooo")
            isVideo = true
            videoPlayer = AVPlayer(url: path!)
            avPlayerVC.player = videoPlayer
            
            if let thumbnail = getThumbnailImage(forUrl: path!) {
                playImage.isHidden = false
                productImage.image = thumbnail
            }
            
        } else {
            print("imageeeeee")
            isVideo = false
            playImage.isHidden = true
            productImage.kf.setImage(with: self.path, placeholder: UIImage(systemName: "cube.box"))
            
        }
    }
    
    func getThumbnailImage(forUrl url: URL) -> UIImage? {
        let asset: AVAsset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)

        do {
            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(value: 1, timescale: 60), actualTime: nil)
            return UIImage(cgImage: thumbnailImage)
        } catch let error {
            print(error)
        }

        return nil
    }
}
