//
//  ImageProductAddedCollectionViewCell.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 22/04/22.
//

import UIKit
import Kingfisher

class ImageProductAddedCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var cellView: UIView!
    
    static let identifier = String(describing: "ImageProductAddedCollectionViewCell")
    
    var segueButtonDelete: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setup(item: ProductMedia) {
        print("isi media: \(item.pathUrl ?? "")")
        productImage.kf.setImage(with: (item.pathUrl ?? "").asUrl, placeholder: UIImage(systemName: "cube.box"))
    }

    @IBAction func deleteTapped(_ sender: Any) {
        self.segueButtonDelete?()
    }
    
}
