//
//  ProductDetailVC.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 22/11/21.
//

import UIKit
import SwiftyJSON
import Kingfisher

class ProductDetailVC: UIViewController {
    
    @IBOutlet weak var nameProductLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var categoryView: UIView!
    @IBOutlet weak var categoryImg: UIImageView!
    
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var specificationLbl: UILabel!
    @IBOutlet weak var conditionLbl: UILabel!
    
    @IBOutlet weak var imageStackView: UIStackView!
    @IBOutlet weak var noneImageView: UIView!
    
    @IBOutlet weak var linkStackView: UIStackView!
    @IBOutlet weak var noneSosmedView: UIView!
    @IBOutlet weak var statusSwitch: UISwitch!
    
    @IBOutlet weak var reviewStackView: UIStackView!
    @IBOutlet weak var noneReviewView: UIView!
    @IBOutlet weak var moreReview: UIView!
    
    @IBOutlet weak var deleteProductView: UIView!
    
    @IBAction func unwindToProductOwner(_ sender: UIStoryboardSegue) {
//        self.getDataFromAPI()
        print("back to product")
    }
    
    var mediaProduct = [JSON?]()
    var sosmed = [JSON?]()
    var dataReview = [JSON?]()
    
    var productSlug = ""
    var productId = ""
    var statusProduct = ""
    var categoryId = 0
    
    var blue = UIColor(red: 73.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customNavBar()
//        getDataFromAPI()
        
        deleteProductView.layer.borderColor = UIColor.lightGray.cgColor
        deleteProductView.layer.borderWidth = 0.5
    }
    
    override func viewWillAppear(_ animated: Bool) {
        for view in imageStackView.arrangedSubviews{
              imageStackView.removeArrangedSubview(view)
              view.removeFromSuperview()
        }
        for view in linkStackView.arrangedSubviews{
              linkStackView.removeArrangedSubview(view)
              view.removeFromSuperview()
        }
        for view in reviewStackView.arrangedSubviews{
              reviewStackView.removeArrangedSubview(view)
              view.removeFromSuperview()
        }
        getDataFromAPI()
    }
    
    func customNavBar() {
        self.view.backgroundColor = blue
        self.navigationItem.largeTitleDisplayMode = .never
    }

    func getDataFromAPI() {
        noneImageView.isHidden = false
        noneSosmedView.isHidden = false
        noneReviewView.isHidden = false
        
        if self.productSlug != "" {
            APIBisanara.shared.getDetailProductOwner(productSlug: self.productSlug) { (status, response) in
                if status == true {
                    let json = JSON(response!)
                    
                    if json["status"].stringValue == "success" {
                        let dataDict = json["data"].dictionaryValue
                        
                        let product = ProductOwner(response: dataDict["product"]!)
                        
//                        print("name:\(product.name ?? "") & id:\(product.productId ?? "")")
                        self.productId = product.productId ?? ""
                        self.nameProductLbl.text = product.name ?? ""
                        self.priceLbl.text = (product.price ?? "").currencyFormatterString()
                        self.conditionLbl.text = product.condition ?? ""
//                        self.transactionIdLbl.text = product.productTransactionId ?? ""
                        
                        let desc = product.description ?? ""
                        self.descriptionLbl.text = desc.trimHTMLTags()
                        
                        let spec = product.spec ?? ""
                        self.specificationLbl.text = spec.trimHTMLTags()
                        
                        
                        let status = product.status ?? ""
                        if status == "7" {
                            self.statusLbl.text = "Publish" // hex: 4990E2
                            self.statusLbl.textColor = UIColor(red: 73.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0)
                            self.statusSwitch.isOn = true
                            
                        } else if status == "9" {
                            self.statusLbl.text = "Draft" // hex: 22884D
                            self.statusLbl.textColor = UIColor(red: 34.0/255.0, green: 136.0/255.0, blue: 77.0/255.0, alpha: 1.0)
                            self.statusSwitch.isOn = false
                            
                        } else if status == "2" {
                            self.statusLbl.text = "Waiting" // hex: AF1D1D
                            self.statusLbl.textColor = UIColor(red: 175.0/255.0, green: 29.0/255.0, blue: 29.0/255.0, alpha: 1.0)
                            self.statusSwitch.isOn = false
                        }
                        
                        let productDict = dataDict["product"]?.dictionaryValue
                        
                        // GET CATEGORY
                        let category = ProductCategory(response: productDict!["category"]!)
                        DispatchQueue.main.async {
                            let categoryName = category.name ?? ""
                            self.categoryLbl.text = categoryName.trimHTMLTags()
                            self.categoryId = category.categoryProductId ?? 0
                            self.categoryImg.image = UIImage(named: "\(category.slug ?? "").svg")
                            self.categoryImg.tintColor = .white
                            
                            self.categoryView.layer.cornerRadius = self.categoryView.frame.size.height/2
                            self.categoryView.backgroundColor = self.blue
                        }
                        
                        // GET MEDIA
                        self.mediaProduct = productDict!["media"]!.arrayValue
                        for data in self.mediaProduct {
                            let media = ProductMedia(response: data!)
                            DispatchQueue.main.async {
                                self.imageStackView.reloadInputViews()
                                
                                if let cell = UINib(nibName: String(describing: MediaCellView.self), bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? MediaCellView {
                                    
                                    self.imageStackView.addArrangedSubview(cell)
                                    
                                    cell.cellView.layer.cornerRadius = 5
                                    cell.cellView.layer.borderWidth = 0.5
                                    cell.cellView.layer.borderColor = UIColor.lightGray.cgColor
                                    
                                    cell.mediaId = media.mediaId ?? ""
                                    cell.productId = media.productId ?? ""
                                    let image = media.pathUrl ?? ""
                                    cell.mediaImageView.kf.setImage(with: image.asUrl, placeholder: UIImage(systemName: "cube.box"))
                                    
                                    let isCover = media.isCover ?? ""
                                    if isCover == "1" {
                                        cell.markImageView.image = UIImage(systemName: "checkmark.circle.fill")
                                        cell.markImageView.tintColor = .systemBlue
                                        cell.coverLbl.text = "Cover"
                                    } else if isCover == "0" {
                                        cell.markImageView.image = UIImage(systemName: "xmark.circle.fill")
                                        cell.markImageView.tintColor = .systemRed
                                        cell.coverLbl.text = "Not Cover"
                                    }
                                    
                                    cell.segueDetailMedia = {
                                        let vc = UIStoryboard(name: "ManageProduct", bundle: nil).instantiateViewController(withIdentifier: "ImageProductDetailVC") as? ImageProductDetailVC
                                        self.navigationController?.pushViewController(vc!, animated: true)
                                        
                                        vc?.productMediaId = cell.mediaId
                                        vc?.urlMedia = image
                                        vc?.isCover = isCover
                                        
                                    }
                                }
                                self.noneImageView.isHidden = true
                            }
                        }
                        
                        // EACH SOSMED
                        self.sosmed = productDict!["sosmed"]!.arrayValue
                        for data in self.sosmed {
                            let sosmedData = SosMedProduct(response: data!)
                            
                            DispatchQueue.main.async {
                                self.linkStackView.reloadInputViews()

                                if let card = UINib(nibName: String(describing: OtherPlatformLinkView.self), bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? OtherPlatformLinkView {
                                    
                                    self.linkStackView.addArrangedSubview(card)

                                    card.sosmedId = sosmedData.sosmedId ?? ""
                                    card.urlLbl.text = sosmedData.url ?? ""
                                    card.clickLbl.text = sosmedData.counter ?? ""
                                    
                                    let type = sosmedData.socialMedia?.icon ?? ""
                                    card.iconImage.kf.setImage(with: type.asUrl)

                                    card.platformView.layer.cornerRadius = 5
                                    card.platformView.layer.borderWidth = 0.5
                                    card.platformView.layer.borderColor = UIColor.lightGray.cgColor
                                    
                                    card.segueButtonEditSosmed = {
                                        let vc = UIStoryboard(name: "ManageShop", bundle: nil).instantiateViewController(withIdentifier: "LinkShopFormVC") as? SosmedFormVC
                                        self.navigationController?.pushViewController(vc!, animated: true)
                                        vc!.productSlug = self.productSlug
                                        vc!.productId = sosmedData.productId ?? ""
                                        vc!.sosmedId = sosmedData.productSosmedId ?? ""
                                    }
                                }
                                self.noneSosmedView.isHidden = true
                            }
                        }
                        
                        // GET REVIEWS
                        let reviewsDict = dataDict["reviews"]?.dictionaryValue
                        self.dataReview = reviewsDict!["data"]!.arrayValue
                        
                        self.moreReview.isHidden = true
                        for data in self.dataReview {
                            let review = Review(response: data!)
                            
                            DispatchQueue.main.async {
                                self.reviewStackView.reloadInputViews()
                                if let cell = UINib(nibName: String(describing: ReviewCellView.self), bundle: nil).instantiate(withOwner: nil, options: nil)[0] as?  ReviewCellView {
                                    
                                    self.moreReview.isHidden = false
                                    self.reviewStackView.addArrangedSubview(cell)
                                    
                                    cell.reviewId = review.reviewId ?? ""
                                    cell.nameLbl.text = review.name ?? ""
                                    cell.emailLbl.text = review.email ?? ""
                                    cell.feedbackLbl.text = review.reviewComment ?? ""
                                    cell.typeReviewerLbl.text = review.typeReviewer ?? ""
                                    
                                    cell.cardView.layer.cornerRadius = 5
                                    cell.cardView.layer.borderWidth = 0.5
                                    cell.cardView.layer.borderColor = UIColor.lightGray.cgColor
                                    
                                    // MEDIA REVIEW
                                    let mediaArray = data!["media"].arrayValue
//                                        var widthMedia: CGFloat = 0
//                                        cell.feedbackImageStackView.widthAnchor.constraint(equalToConstant: widthMedia).isActive = true
//                                        cell.feedbackImageStackView.leadingAnchor.constraint(equalTo: cell.mediaView.leadingAnchor, constant: 60).isActive = true
                                    for media in mediaArray {
//                                            widthMedia+=50
                                        let media = MediaReview(response: media)
                                        
                                        if media.attachId != "" {
                                            DispatchQueue.main.async {
                                                cell.feedbackImageStackView.reloadInputViews()
                                                if let mediaView = UINib(nibName: String(describing: ReviewMediaView.self), bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? ReviewMediaView {
                                                    cell.feedbackImageStackView.addArrangedSubview(mediaView)
                                                    
                                                    mediaView.mediaId = media.attachId ?? ""
                                                    
                                                    let image = media.path ?? ""
                                                    mediaView.mediaImageView.kf.setImage(with: image.asUrl)
                                                    if mediaView.mediaImageView.image == nil {
                                                        mediaView.mediaImageView.image = UIImage(systemName: "cube.box")
                                                    }
                                                    
                                                    mediaView.mediaImageView.layer.cornerRadius = 8
                                                    
                                                    mediaView.segueViewButton = {
                                                        // to see the image with modal view
                                                        let cellImg = UIStoryboard(name: "ReviewProduct", bundle: nil).instantiateViewController(withIdentifier: "ReviewProductModalVC") as? ReviewProductModalVC
                                                        self.present(cellImg!, animated: true, completion: nil)
                                                        
                                                        cellImg?.bigImageView.kf.setImage(with: image.asUrl, placeholder: UIImage(systemName: "cube.box"))
                                                        
                                                        cellImg?.closeButton = {
                                                            cellImg?.dismiss(animated: true)
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            cell.mediaView.isHidden =  true
                                        }
                                    }
                                }
                                self.noneReviewView.isHidden = true
                            }
                        }
                    }
                    
                } else {
                    let error = JSON(response!)
                    print("status false api: \(error)")
                }
            }
        } else {
            print("product slug nil, \(self.productSlug)")
            
        }
    }
    
    @IBAction func switchStatusTapped(_ sender: Any) {
        if statusLbl.text == "Draft" {
            statusLbl.text = "Publish" // hex: 4990E2
            statusLbl.textColor = UIColor(red: 73.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0)
            statusSwitch.isOn = true
            statusProduct = "7"
            
        } else if statusLbl.text == "Publish" {
            statusLbl.text = "Draft" // hex: 22884D
            statusLbl.textColor = UIColor(red: 34.0/255.0, green: 136.0/255.0, blue: 77.0/255.0, alpha: 1.0)
            statusSwitch.isOn = false
            statusProduct = "9"
            
        } else if statusLbl.text == "Waiting" {
            statusLbl.text = "Publish" // hex: 4990E2
            statusLbl.textColor = UIColor(red: 73.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0)
            statusSwitch.isOn = true
            statusProduct = "7"
        }
        
        APIBisanara.shared.changeStatusProduct(slug: self.productSlug, statusProduct: self.statusProduct) { status in
            if status == true {
                let alertSuccess = UIAlertController(title: "Success!", message: "Success save product status to \(self.statusLbl.text ?? "")", preferredStyle: .alert)
                let okSuccess = UIAlertAction(title: "OK", style: .default)
                alertSuccess.addAction(okSuccess)
                self.present(alertSuccess, animated: true, completion: nil)
                
            } else {
                let alertFailed = UIAlertController(title: "Failed!", message: "sorry, failed to save status, please try again later or kindly contact us", preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style: .default) { action in
                    print("failed save sosmed")
                }
                alertFailed.addAction(ok)
                self.present(alertFailed, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func editProductTapped(_ sender: Any) {
        let vc = UIStoryboard(name: "ManageProduct", bundle: nil).instantiateViewController(withIdentifier: "EditGeneralProductVC") as? EditGeneralProductVC
        vc!.productSlug = self.productSlug
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func addNewImage(_ sender: Any) {
        let vc = UIStoryboard(name: "ManageProduct", bundle: nil).instantiateViewController(withIdentifier: "AddImageProductVC") as? AddImageProductVC
        self.navigationController?.pushViewController(vc!, animated: true)
        vc!.productSlug = self.productSlug
    }
    
    @IBAction func addNewSosmed(_ sender: Any) {
        let vc = UIStoryboard(name: "ManageShop", bundle: nil).instantiateViewController(withIdentifier: "LinkShopFormVC") as? SosmedFormVC
        self.navigationController?.pushViewController(vc!, animated: true)
        vc!.productSlug = self.productSlug
        vc!.productId = self.productId
    }
    
    @IBAction func deleteProduct(_ sender: Any) {
        APIBisanara.shared.deleteProductOwner(slug: self.productSlug, productId: self.productId) { status in
            if status == true {
                let alertSuccess = UIAlertController(title: "", message: "Successfully delete your Product", preferredStyle: .alert)
                let okSuccess = UIAlertAction(title: "OK", style: .default) { (action) in
//                    self.navigationController?.popViewController(animated: true) // go back to the previous vc
                    self.performSegue(withIdentifier: "unwindToListProductOwner", sender: self)
                    print("succes delete product")
                }
                alertSuccess.addAction(okSuccess)
                self.present(alertSuccess, animated: true, completion: nil)
                
            } else {
                let alertFailed = UIAlertController(title: "", message: "sorry, failed to delete data, please try again later or kindly contact us", preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style: .default) { action in
                    print("failed delete product")
                }
                alertFailed.addAction(ok)
                self.present(alertFailed, animated: true, completion: nil)
                
            }
        }

    }
    
    @IBAction func reviewTapped(_ sender: Any) {
        let vc = UIStoryboard(name: "ManageProduct", bundle: nil).instantiateViewController(withIdentifier: "ReviewProductVC") as? ReviewProductVC
        self.navigationController?.pushViewController(vc!, animated: true)
        vc?.productSlug = self.productSlug
        vc?.isOwner = true
    }
    
}
