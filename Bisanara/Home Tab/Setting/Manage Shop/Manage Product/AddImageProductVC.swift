//
//  AddImageProductVC.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 29/03/22.
//

import Foundation
import UIKit
import SwiftyJSON
import OpalImagePicker

class AddImageProductVC: UIViewController, OpalImagePickerControllerDelegate {
    
    @IBOutlet weak var imageCollectionView: UICollectionView!
    
    var productSlug = ""
    var imageArray: [UIImage]? = []
    
    var blue = UIColor(red: 73.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customNavBar()
        registerCells()
    }
    
    func customNavBar() {
        title = "Image Product Detail"
        self.view.backgroundColor = blue
        self.navigationItem.largeTitleDisplayMode = .never
    }
    
    private func registerCells() {
        imageCollectionView.register(UINib(nibName: ImageProductAddedCollectionViewCell.identifier, bundle: nil), forCellWithReuseIdentifier: ImageProductAddedCollectionViewCell.identifier)
    }
    
    @IBAction func addTapped(_ sender: Any) {
        // ADD MORE IMAGE/VIDEO
        let imagePicker = OpalImagePickerController()
        imagePicker.imagePickerDelegate = self
        imagePicker.selectionTintColor = UIColor.white.withAlphaComponent(0.7)
        imagePicker.selectionImageTintColor = UIColor.black
        imagePicker.selectionImage = UIImage(systemName: "checkmark")
        imagePicker.statusBarPreference = UIStatusBarStyle.lightContent
        imagePicker.maximumSelectionsAllowed = 10
        
        let config = OpalImagePickerConfiguration()
        config.maximumSelectionsAllowedMessage = NSLocalizedString("You cannot select that many images, sorry", comment: "")
        imagePicker.configuration = config
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage]) {
        self.imageArray?.append(contentsOf: images)
        print("image array: \(String(describing: imageArray))")
        DispatchQueue.main.async {
            self.imageCollectionView.reloadData()
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerDidCancel(_ picker: OpalImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addMediaTapped(_ sender: Any) {
        // ADD MEDIA OR SAVE TO API
        if productSlug != "" {
            if self.imageArray != [] {
                APIBisanara.shared.addMediaProduct(slug: self.productSlug, media: self.imageArray) { response in
                    if response != nil {
                        let json = JSON(response!)
                        
                        let data = json["data"].dictionaryValue
                        let media = data["media"]?.arrayValue
                        print("isi media: \(String(describing: media))")
                        
                        let message = json["message"].stringValue
                        let alertSuccess = UIAlertController(title: "", message: "\(message)",preferredStyle: .alert)
                        let ok = UIAlertAction(title: "OK", style: .default) { (action) in
                            self.performSegue(withIdentifier: "unwindToProductOwner", sender: self)
                        }
                        alertSuccess.addAction(ok)
                        self.present(alertSuccess, animated: true, completion: nil)
                    }
                }
            } else {
                print("media nill")
                let alertFailed = UIAlertController(title: "Failed to Add", message: "Add at least 1 media/image",preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style: .default)
                alertFailed.addAction(ok)
                self.present(alertFailed, animated: true, completion: nil)
            }
        }
    }
}

extension AddImageProductVC: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case imageCollectionView:
            print("total image: \(imageArray?.count ?? 0  )")
            return imageArray?.count ?? 0
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageProductAddedCollectionViewCell.identifier, for: indexPath) as! ImageProductAddedCollectionViewCell

        cell.productImage.image = self.imageArray?[indexPath.row]
        cell.cellView.layer.cornerRadius = 5
        cell.cellView.layer.borderWidth = 0.5
        cell.cellView.layer.borderColor = UIColor.lightGray.cgColor
//        cell.segueButtonDelete = {
//            self.imageCollectionView.deleteItems(at: [indexPath])
//            self.imageCollectionView.reloadData()
//        }
        
        return cell
    }
}
