//
//  ManageListProductCollectionViewCell.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 22/11/21.
//

import UIKit

class ManageListProductCollectionViewCell: UICollectionViewCell {
    
    static let identifier = String(describing: ManageListProductCollectionViewCell.self)

    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var view: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setup(product: ProductOwner) {
        self.productNameLbl.text = product.name ?? ""
        self.priceLbl.text = (product.price ?? "").currencyFormatterString()
        self.categoryLbl.text = (product.category?.name ?? "").trimHTMLTags()
        self.categoryLbl.textColor = .orange
        
        self.view.layer.borderColor = UIColor.gray.cgColor
        self.view.layer.borderWidth = 1
        self.view.layer.cornerRadius = 10
    }

}
