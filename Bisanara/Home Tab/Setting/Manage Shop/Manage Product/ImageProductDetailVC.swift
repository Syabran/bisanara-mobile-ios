//
//  ImageProductDetailVC.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 25/03/22.
//

import Foundation
import UIKit
import SwiftyJSON
import Kingfisher

class ImageProductDetailVC: UIViewController {
    
    @IBOutlet weak var mediaImageView: UIImageView!
    @IBOutlet weak var setCoverButton: UIButton!
    
    var productMediaId = ""
    var urlMedia = ""
    var isCover = "" // 1 means its cover, 0 means not cover
    
    var blue = UIColor(red: 73.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customNavBar()
        if isCover == "1" {
            setCoverButton.setTitle("Set as Not Cover", for: .normal)
            
        } else if isCover == "0" {
            setCoverButton.setTitle("Set as Cover Default", for: .normal)
        }
        
        if urlMedia == "" {
            mediaImageView.image = UIImage(systemName: "cube.box")
        } else {
            mediaImageView.kf.setImage(with: (urlMedia).asUrl, placeholder: UIImage(systemName: "cube.box"))
        }
    }
    
    func customNavBar() {
        title = "Image Product Detail"
        self.view.backgroundColor = blue
        self.navigationItem.largeTitleDisplayMode = .never
    }
    
    @IBAction func setCoverTapped(_ sender: Any) {
        APIBisanara.shared.setImageAsCoverProduct(productMediaId: productMediaId) { (status, response) in
            if status == true {
                let json = JSON(response!)
                
                if json["status"].stringValue == "success" {
                    if self.isCover == "1" {
                        let alertSuccess = UIAlertController(title: "", message: "Successfully set your image as Not cover/default image product", preferredStyle: .alert)
                        let okSuccess = UIAlertAction(title: "OK", style: .default) { (action) in
                            self.performSegue(withIdentifier: "unwindToProductOwner", sender: self)
                            print("success set as Not cover product")
                        }
                        alertSuccess.addAction(okSuccess)
                        self.present(alertSuccess, animated: true, completion: nil)
                        
                    } else {
                        let alertSuccess = UIAlertController(title: "", message: "Successfully set your image as cover/default image product", preferredStyle: .alert)
                        let okSuccess = UIAlertAction(title: "OK", style: .default) { (action) in
                            self.navigationController?.popViewController(animated: true) // go back to the previous vc
                            print("success set as cover product")
                        }
                        alertSuccess.addAction(okSuccess)
                        self.present(alertSuccess, animated: true, completion: nil)
                    }
                    
                    
                } else {
                    let alertSuccess = UIAlertController(title: "", message: "Sorry, failed to save image product detail, please try again later or kindly contact us", preferredStyle: .alert)
                    let okSuccess = UIAlertAction(title: "OK", style: .default) { (action) in
                        self.navigationController?.popViewController(animated: true) // go back to the previous vc
                        print("failed set as cover product")
                    }
                    alertSuccess.addAction(okSuccess)
                    self.present(alertSuccess, animated: true, completion: nil)
                }
            }
        }
    }
    
    @IBAction func removeImageTapped(_ sender: Any) {
        APIBisanara.shared.deleteImageProduct(productMediaId: productMediaId) { (status, response) in
            if status == true {
                let json = JSON(response!)
                
                if json["status"].stringValue == "success" {
                    let alertSuccess = UIAlertController(title: "", message: "Successfully Delete your image product", preferredStyle: .alert)
                    let okSuccess = UIAlertAction(title: "OK", style: .default) { (action) in
                        self.performSegue(withIdentifier: "unwindToListProductOwner", sender: self)
                        print("success delete image product")
                    }
                    alertSuccess.addAction(okSuccess)
                    self.present(alertSuccess, animated: true, completion: nil)
                    
                } else {
                    let alertSuccess = UIAlertController(title: "", message: "sorry failed to Delete your image product, try again later or kindly contact us", preferredStyle: .alert)
                    let okSuccess = UIAlertAction(title: "OK", style: .default) { (action) in
                        self.navigationController?.popViewController(animated: true) // go back to the previous vc
                        print("failed delete image product")
                    }
                    alertSuccess.addAction(okSuccess)
                    self.present(alertSuccess, animated: true, completion: nil)
                }
            }
        }
    }
}
