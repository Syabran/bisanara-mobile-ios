//
//  LinkShopFormVC.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 19/11/21.
//

import Foundation
import UIKit
import SwiftyJSON

class SosmedFormVC: UIViewController {
    
    @IBOutlet weak var sosmedTypeTxtField: UITextField!    
    @IBOutlet weak var urlTxtView: UITextView!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    
    var sosmedShopData: ShopSosmed?
    var shopSlug = ""
    
    var sosmedProductData: SosMedProduct?
    var productSlug = ""
    var productId = "" // product id it self
    
    var sosmedId = "" // product or shop social_media_id
    var sendSosmedType = "" // type id sosmed: 1,2,3
    
    private var pickerSosmed = UIPickerView()
    var dataSosmed: [ModelSocialMedia]? = []
    var sosmed: [String]? = []
    
    private var dispatchGroup = DispatchGroup()
    
    var blue = UIColor(red: 73.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getDataSocMedia()
        customNavBar()
        getData()
        hideKeyboardWhenTappedAround()
        
        urlTxtView.layer.borderColor = UIColor.lightGray.cgColor
        urlTxtView.layer.borderWidth = 0.5
        urlTxtView.layer.cornerRadius = 5
        
        print("slug: \(productSlug) dan \(shopSlug)")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        addPickerSosmed()
    }
    
    private func getData() {
        if sosmedId == "" {
            // ADD SOSMED
            
            deleteButton.isHidden = true
            addButton.setTitle("Add", for: .normal)
            self.urlTxtView.text = "example: https://www.instagram.com/bisanara"
            
        } else {
            // UPDATE SOSMED
            if productSlug != "" {
                // get data product
                print("product slugnya: \(productSlug)")
                APIBisanara.shared.getDetailProductOwner(productSlug: self.productSlug) { (status, response) in
                    if status == true {
                        let json = JSON(response!)
                        
                        if json["status"].stringValue == "success" {
                            let dataDict = json["data"].dictionaryValue
                            let productDict = dataDict["product"]?.dictionaryValue
                            
                            // get sosmed data
                            if let sosmed = productDict!["sosmed"]?.arrayValue {
                                for data in sosmed {
                                    self.sosmedProductData = SosMedProduct(response: data)
                                    
                                    print("get sosmed data: \(self.sosmedProductData?.productSosmedId ?? "") dan \(self.sosmedId)")
                                    
                                    if self.sosmedId == self.sosmedProductData?.productSosmedId ?? "" {
                                        self.sosmedTypeTxtField.text = self.sosmedProductData?.socialMedia?.name ?? ""
                                        self.sendSosmedType = self.sosmedProductData?.sosmedId ?? ""
                                        self.urlTxtView.text = self.sosmedProductData?.url ?? ""
                                    }
                                }
                                // product owner cant update sosmed
                                self.addButton.isHidden = true
                            }
                        }
                        
                    } else {
                        let error = JSON(response!)
                        print("status false api: \(error)")
                    }
                }
                
            } else if shopSlug != "" {
                APIBisanara.shared.getDetailShopOwner(slug: self.shopSlug) { (status, response) in
                    let json = JSON(response!)
                    
                    if json["status"].stringValue == "success" {
                        let data = json["data"].dictionaryValue
                        
                        // get social media data
                        if let socialMedia = data["social_media"]?.arrayValue {
                            for data in socialMedia {
                                self.sosmedShopData = ShopSosmed(response: data)
                                
                                print("hasil id: \(self.sosmedId) = \(self.sosmedShopData?.id ?? "")")
                                
                                if self.sosmedId == self.sosmedShopData?.id ?? "" {
                                    self.sosmedTypeTxtField.text = self.sosmedShopData?.name ?? ""
                                    self.sendSosmedType = self.sosmedShopData?.sosmedId ?? ""
                                    self.urlTxtView.text = self.sosmedShopData?.url ?? ""
                                }
                            }
                        }
                        
                    }
                }
            }
        }
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
    }
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    func customNavBar() {
        self.view.backgroundColor = blue
        self.navigationItem.largeTitleDisplayMode = .never
    }
    
    func getDataSocMedia() {
        dispatchGroup.enter()
        APIBisanara.shared.getSocialMedia { (status, response) in
            
            if status == true {
                for item in response!["data"].arrayValue {
                    self.dataSosmed?.append(ModelSocialMedia(response: item))
                    self.sosmed?.append(item["name"].stringValue)
                }
            }
        }
        dispatchGroup.leave()
    }
    func addPickerSosmed() {
        pickerSosmed.delegate = self
        pickerSosmed.dataSource = self
        sosmedTypeTxtField.inputView = pickerSosmed
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let done = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(doneTappedSosmed))
        toolbar.items = [flexible, done]
        
        sosmedTypeTxtField.inputView = pickerSosmed
        sosmedTypeTxtField.inputAccessoryView = toolbar
    }
    @objc func doneTappedSosmed() {
        self.view.endEditing(true)
        
        for item in dataSosmed! {
            if sosmedTypeTxtField.text == item.name {
                sendSosmedType = item.id!
            }
        }
    }
    
    @IBAction func saveTapped(_ sender: Any) {
        if sendSosmedType != "" && urlTxtView.text != "" {
            if self.productSlug != "" {
                // ADD SOSMED PRODUCT
                APIBisanara.shared.addSosmedProduct(slug: self.productSlug, productId: self.productId, url: self.urlTxtView.text ?? "", sosmedType: self.sendSosmedType) { (response) in
                    let json = JSON(response!)
                    if json["status"].stringValue == "success" || json["status"].stringValue == "Success" {
                        
                        let alertSuccess = UIAlertController(title: "Success!", message: "Success save your Product Social Media", preferredStyle: .alert)
                        let okSuccess = UIAlertAction(title: "OK", style: .default) { (action) in
            //                let vc = UIStoryboard(name: "ManageShop", bundle: nil).instantiateViewController(withIdentifier: "ShopDetailVC") as? ShopDetailVC
            //                vc?.arrayLink = self.arrayLink
                            self.navigationController?.popViewController(animated: true) // go back to the previous vc
                        }
                        alertSuccess.addAction(okSuccess)
                        self.present(alertSuccess, animated: true, completion: nil)
                        
                    } else {
                        let message = json["message"].stringValue
                        let alertFailed = UIAlertController(title: "Failed!", message: "sorry \(message), please try again later or kindly contact us", preferredStyle: .alert)
                        let ok = UIAlertAction(title: "OK", style: .default) { action in
                            print("failed save sosmed")
                        }
                        alertFailed.addAction(ok)
                        self.present(alertFailed, animated: true, completion: nil)
                    }
                }
                
            } else if self.shopSlug != "" {
                // ADD OR UPDATE SOSMED SHOP
                APIBisanara.shared.addUpdateSosmedShop(slug: self.shopSlug, sosmedId: self.sosmedId, url: self.urlTxtView.text ?? "", sosmedType: self.sendSosmedType) { (status) in
                    if status == true {
                        let alertSuccess = UIAlertController(title: "Success!", message: "Success save your Shop Social Media", preferredStyle: .alert)
                        let okSuccess = UIAlertAction(title: "OK", style: .default) { (action) in
            //                let vc = UIStoryboard(name: "ManageShop", bundle: nil).instantiateViewController(withIdentifier: "ShopDetailVC") as? ShopDetailVC
            //                vc?.arrayLink = self.arrayLink
                            self.navigationController?.popViewController(animated: true) // go back to the previous vc
                        }
                        alertSuccess.addAction(okSuccess)
                        self.present(alertSuccess, animated: true, completion: nil)
                        
                    } else {
                        let alertFailed = UIAlertController(title: "Failed!", message: "sorry, failed to save data, please try again later or kindly contact us", preferredStyle: .alert)
                        let ok = UIAlertAction(title: "OK", style: .default) { action in
                            print("failed save sosmed")
                        }
                        alertFailed.addAction(ok)
                        self.present(alertFailed, animated: true, completion: nil)
                    }
                }
            } else {
                print("product or shop id is nill")
            }
            
        } else {
            let alertFailed = UIAlertController(title: "", message: "please fill social media type and the url", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default) { action in
                print("data sosmed not fill")
            }
            alertFailed.addAction(ok)
            self.present(alertFailed, animated: true, completion: nil)
        }
    }
    
    @IBAction func deleteTapped(_ sender: Any) {
        if productSlug != "" {
            print("delete prod klikk")
            if self.sosmedId != "" {
                APIBisanara.shared.deleteProductSosmed(slug: self.productSlug, productSosmedId: self.sosmedId) { (status) in
                    if status == true {
                        let alertSuccess = UIAlertController(title: "", message: "Successfully delete your product Social Media", preferredStyle: .alert)
                        let okSuccess = UIAlertAction(title: "OK", style: .default) { (action) in
                            self.navigationController?.popViewController(animated: true) // go back to the previous vc
                        }
                        alertSuccess.addAction(okSuccess)
                        self.present(alertSuccess, animated: true, completion: nil)
                        
                    } else {
                        let alertFailed = UIAlertController(title: "", message: "sorry, failed to delete data, please try again later or kindly contact us", preferredStyle: .alert)
                        let ok = UIAlertAction(title: "OK", style: .default) { action in
                            print("failed delete sosmed")
                        }
                        alertFailed.addAction(ok)
                        self.present(alertFailed, animated: true, completion: nil)
                    }
                }
                
            } else { print("sosmed id kosong") }
            
        } else if shopSlug != "" {
            print("delete shop klikk")
            APIBisanara.shared.deleteShopSosmed(slug: self.shopSlug, sosmedId: self.sosmedId) { status in
                if status == true {
                    let alertSuccess = UIAlertController(title: "", message: "Successfully delete your Shop Social Media", preferredStyle: .alert)
                    let okSuccess = UIAlertAction(title: "OK", style: .default) { (action) in
                        self.navigationController?.popViewController(animated: true) // go back to the previous vc
                    }
                    alertSuccess.addAction(okSuccess)
                    self.present(alertSuccess, animated: true, completion: nil)
                    
                } else {
                    let alertFailed = UIAlertController(title: "", message: "sorry, failed to delete data, please try again later or kindly contact us", preferredStyle: .alert)
                    let ok = UIAlertAction(title: "OK", style: .default) { action in
                        print("failed delete sosmed")
                    }
                    alertFailed.addAction(ok)
                    self.present(alertFailed, animated: true, completion: nil)
                }
            }
            
        }
    }
        
}

extension SosmedFormVC: UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return sosmed?.count ?? 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return sosmed?[row] ?? ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        sosmedTypeTxtField.text = sosmed?[row]
    }
}
