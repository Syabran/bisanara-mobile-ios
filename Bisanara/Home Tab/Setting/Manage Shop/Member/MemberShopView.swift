//
//  MemberShopView.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 13/12/21.
//

import UIKit

class MemberShopView: UIView {
    
    @IBOutlet weak var memberImage: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var memberView: UIView!
    
    var id = ""
    
    var segueButtonConfirm: (() -> Void)?
    
    @IBAction func confirmButton(_ sender: Any) {
        self.segueButtonConfirm?()
    }
    
}
