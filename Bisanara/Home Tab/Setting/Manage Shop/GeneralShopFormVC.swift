//
//  GeneralShopForm.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 19/11/21.
//

import Foundation
import UIKit
import SwiftyJSON

class GeneralShopFormVC: UIViewController {
    
    @IBOutlet weak var cpEmailTxtField: UITextField!
    @IBOutlet weak var cpEmailSecondaryTxtField: UITextField!
    @IBOutlet weak var cpPhoneTxtField: UITextField!
    @IBOutlet weak var cpPhoneSecondaryTxtField: UITextField!
    @IBOutlet weak var addressTextView: UITextView!
    @IBOutlet weak var postalCodeTxtField: UITextField!
    @IBOutlet weak var descTxtView: UITextView!
    
    var dataShop: ShopData!
    var urlSlug = ""
    var idShop = ""
    
    var blue = UIColor(red: 73.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customNavBar()
        customView()
        getDataFromAPI()
    }
    
    private func customView() {
        addressTextView.layer.borderColor = UIColor.lightGray.cgColor
        addressTextView.layer.borderWidth = 0.5
        addressTextView.layer.cornerRadius = 5
        
        descTxtView.layer.borderColor = UIColor.lightGray.cgColor
        descTxtView.layer.borderWidth = 0.5
        descTxtView.layer.cornerRadius = 5
    }
    
    func customNavBar() {
        self.view.backgroundColor = blue
        self.navigationItem.largeTitleDisplayMode = .never
    }
    
    func getDataFromAPI() {
        APIBisanara.shared.getDetailShopOwner(slug: self.urlSlug) { (status, response) in
            let json = JSON(response!)
            
            if json["status"].stringValue == "success" {
                let data = json["data"].dictionaryValue
                
                self.idShop = data["id_shop"]?.stringValue ?? ""
                self.descTxtView.text = data["description"]?.stringValue ?? ""
                self.addressTextView.text = data["address"]?.stringValue ?? ""
                self.postalCodeTxtField.text = data["postal_code"]?.stringValue ?? ""
                self.cpPhoneTxtField.text = data["phone_number"]?.stringValue ?? ""
                self.cpPhoneSecondaryTxtField.text = data["phone_number_secondary"]?.stringValue ?? ""
                self.cpEmailTxtField.text = data["email_primary"]?.stringValue ?? ""
                self.cpEmailSecondaryTxtField.text = data["email_secondary"]?.stringValue ?? ""
                
            }
        }
        
    }
    
    
    @IBAction func saveTapped(_ sender: Any) {
        if cpEmailTxtField.text != "" && cpEmailSecondaryTxtField.text != "" && cpPhoneTxtField.text != "" && cpPhoneSecondaryTxtField.text != "" && addressTextView.text != "" && postalCodeTxtField.text != "" && descTxtView.text != "" {
            
            APIBisanara.shared.updateShopGeneral(slug: self.urlSlug, number1: cpPhoneTxtField.text ?? "", number2: cpPhoneSecondaryTxtField.text ?? "", mail1: cpEmailTxtField.text ?? "", mail2: cpEmailSecondaryTxtField.text ?? "", desc: descTxtView.text ?? "", address: addressTextView.text ?? "", postalCode: postalCodeTxtField.text ?? "") { (status) in
                
                if status == true {
                    let alertSuccess = UIAlertController(title: "Success!", message: "Success save your data",preferredStyle: .alert)
                    let ok = UIAlertAction(title: "OK", style: .default) { (action) in
                        self.navigationController?.popViewController(animated: true) // go back to the previous vc
                    }
                    alertSuccess.addAction(ok)
                    self.present(alertSuccess, animated: true, completion: nil)
                }
            }
        } else {
            let alertFailed = UIAlertController(title: "", message: "please fill all needed data to save data shop", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default) { action in
                print("data shop not fill in")
            }
            alertFailed.addAction(ok)
            self.present(alertFailed, animated: true, completion: nil)
        }
    }
    
}
