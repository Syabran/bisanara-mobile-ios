//
//  AllCategoryViewController.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 29/06/21.
//

import UIKit

class AllCategoryViewController: UIViewController {
    
    @IBOutlet weak var categoriesCollectionView: UICollectionView!
    
    var categories = [ModelCategory]()
    var idCategoryParent = 0
    var idCategoryChild = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customNavBar()
        getDataCategories()
        
        //register cells
        categoriesCollectionView.register(UINib(nibName: CategoryCollectionViewCell.identifier, bundle: nil), forCellWithReuseIdentifier: CategoryCollectionViewCell.identifier)
    }
    
    func getDataCategories() {
        // ALL CATEGORIES
        if self.idCategoryParent != 0 {
            APIBisanara.shared.getCategories(idCategory: "\(self.idCategoryParent)") { (status, response) in
                if response != nil {
                    if response!["status"].stringValue == "Success" {
                        for item in response!["data"].arrayValue {
                            self.categories.append(ModelCategory(response: item))
                            
                            DispatchQueue.main.async {
                                self.categoriesCollectionView.reloadData()
                            }
                        }
                    } else {
                        let alertFail = UIAlertController(title: "Error", message: "sorry, data category is failed, please try again later", preferredStyle: .alert)
                        let okFail = UIAlertAction(title: "Try Again", style: .default) { (action) in
                            self.getDataCategories()
                        }
                        alertFail.addAction(okFail)
                        self.present(alertFail, animated: false, completion: nil)
                    }
                    
                } else {
                    let alertFail = UIAlertController(title: "Error", message: "sorry categories networking fail, try reopen the App or please contact us, thank you", preferredStyle: .alert)
                    let okFail = UIAlertAction(title: "Ok", style: .default)
                    alertFail.addAction(okFail)
                    
                    let cancel = UIAlertAction(title: "cancel", style: .default)
                    alertFail.addAction(cancel)
                    
                    self.present(alertFail, animated: false, completion: nil)
                    
                }
                
            }
        } else { print("category id is nil") }
    }
    
    func customNavBar() {
        
        let blue = UIColor(red: 73.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0)
        if title == "" {
            title = "All Categories"
        }
        self.navigationItem.largeTitleDisplayMode = .never
        self.view.backgroundColor = blue
    }
}

extension AllCategoryViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CategoryCollectionViewCell.identifier, for: indexPath) as! CategoryCollectionViewCell
        cell.setup(category: categories[indexPath.row])
        cell.categoryTitleLabel.textColor = UIColor.label
        return cell

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let controller = AllProductViewController.instantiateFromProductStoryboard()
        controller.title = self.categories[indexPath.row].name ?? ""
        controller.idCategory = self.categories[indexPath.row].id ?? 0
        controller.isAllProduct = true
        controller.productByCategory = true
        
        print("... title: \(String(describing: controller.title)), \(controller.idCategory)")
        navigationController?.pushViewController(controller, animated: true)
    }
}
