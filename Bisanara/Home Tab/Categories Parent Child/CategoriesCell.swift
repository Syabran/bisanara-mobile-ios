//
//  CategoriesCell.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 29/06/21.
//

import UIKit

class CategoriesCell: UICollectionViewCell {
    
    @IBOutlet weak var categoriesImageView: UIImageView!
    
    @IBOutlet weak var categoriesLabel: UILabel!
}
