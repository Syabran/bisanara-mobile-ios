//
//  HomeViewController.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 09/07/21.
//

import UIKit
import SwiftyJSON

class HomeViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var productCollectionView: UICollectionView!
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    @IBOutlet weak var shopCollectionView: UICollectionView!
    @IBOutlet weak var coverCollectionView: UICollectionView!
    @IBOutlet weak var mostViewCollectionView: UICollectionView!
    @IBOutlet var searchBar: UISearchBar!
    
    @IBOutlet weak var coverPageControl: UIPageControl!
    @IBOutlet weak var categoriesView: UIView!
    @IBOutlet weak var shopView: UIView!
    @IBOutlet weak var mostViewProductView: UIView!
    
    @IBAction func unwindToHome(_ sender: UIStoryboardSegue) {
        //
    }
    
    var mostViews = [ProductData]()
    var categories = [ModelCategory]()
    var products = [ProductData]()
    var shops = [ShopData]()
    
    var guestToken = ""
    var loginToken = ""
    var type = ""
    
    var searchText: String?
    var page = 1
    var limit = 0
    var sort = ""
    var sendCategory = 0
    
//    var getToken = APIBisanara.shared.loginHeaders["Authorization"] ?? ""
//    var typeAccount = APIBisanara.shared.loginHeaders["type"] ?? ""
    
    // cover is hidden
    var covers: [Cover] = [
        .init(id: "cv1", coverImage: "https://picsum.photos/id/1/414/128"),
        .init(id: "cv2", coverImage: "https://picsum.photos/id/12/414/128"),
        .init(id: "cv3", coverImage: "https://picsum.photos/id/14/414/128"),
        .init(id: "cv4", coverImage: "https://picsum.photos/id/16/414/128")
    ]
    
    var timer: Timer?
    var currentCellIndex = 0
    var blue = UIColor(red: 73.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getTokenGuest()
        getDataProductAndShop()
        getDataCategories()
        
        customNavBar()
//        searchBarCustom()
//        setTextFieldTintColor(to: .darkText, for: searchBar)
        
        registerCells()
        coverPageControl.numberOfPages = covers.count
//        startTimer()
//        categoryCollectionView.backgroundColor = blue
//        shopCollectionView.backgroundColor = blue
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.loginToken = UserDefaults.standard.value(forKey: "authorization") as? String ?? ""
        self.guestToken = UserDefaults.standard.value(forKey: "token") as? String ?? ""
        self.type = UserDefaults.standard.value(forKey: "type") as? String ?? ""
        print("token login: \(self.loginToken), token guest: \(self.guestToken), type: \(self.type) ")
        
        if self.loginToken == "" {
            mostViewProductView.isHidden = false
        } else {
            mostViewProductView.isHidden = true
        }
        
        setGradientBackground()
        hideBackBar()
        
    }
    
    // TOKEN FOR GUEST OR NOT LOGIN
    func getTokenGuest() {
        if APIBisanara.shared.Headers["token"] == "" {
            
            // GET NEW GUEST TOKEN
            APIBisanara.shared.getGuestData { (status, response) in
                if status == true {
                    if response!["status"].stringValue == "success" {
                        let json = JSON(response!)
                        let guest = json["data"].dictionaryValue
                        
                        let type = guest["type"]?.stringValue
                        let token = guest["token"]?.stringValue
                        
                        if token != nil {
                            APIBisanara.shared.setToken(authorization: "", token: token, type: type)
                            UserDefaults.standard.set(token, forKey: "token")
                            UserDefaults.standard.set(type, forKey: "type")
                            print(" new guest success...")
                        } else {
                            print("token guest nil, failure")
                        }
                    }
                }
            }
        } else {
            print("guest token not nill")
        }
    }
    
    func getDataCategories() {
        // ALL CATEGORIES
        APIBisanara.shared.getCategories(idCategory: "0") { (status, response) in
            if response != nil {
                if response!["status"].stringValue == "Success" {
                    for item in response!["data"].arrayValue {
                        self.categories.append(ModelCategory(response: item))
                        DispatchQueue.main.async {
                            self.categoryCollectionView.reloadData()
                        }
                    }
                } else {
                    let alertFail = UIAlertController(title: "Error", message: "sorry, status category is failed, please try again later", preferredStyle: .alert)
                    let okFail = UIAlertAction(title: "Try Again", style: .default) { (action) in
                        self.getDataCategories()
                    }
                    alertFail.addAction(okFail)
                    self.present(alertFail, animated: false, completion: nil)
                }
                
            } else {
                let alertFail = UIAlertController(title: "Error", message: "sorry categories networking fail, try reopen the App or please contact us, thank you", preferredStyle: .alert)
                let okFail = UIAlertAction(title: "Ok", style: .default)
                alertFail.addAction(okFail)
                
                let cancel = UIAlertAction(title: "cancel", style: .default)
                alertFail.addAction(cancel)
                
                self.present(alertFail, animated: false, completion: nil)
                
            }
            
        }
    }
    
    func getDataProductAndShop() {
        // ALL SHOP LIST
        APIBisanara.shared.getListShop(page: self.page, limit: 8, keyword: self.searchText ?? "" ) { (status, response) in
            if response != nil {
                if response!["status"].stringValue == "Success" {
                    
                    // parsing data from json
                    let json = JSON(response!)
                    let dataDictionary = json["data"].dictionaryValue
                                        
                    if let array = dataDictionary["data"]?.arrayValue {
//                        print("array: \(array)")
                        for data in array {
                            
                            let shop = ShopData(response: data)
                            self.shops.append(shop)
                            DispatchQueue.main.async {
                                self.shopCollectionView.reloadData()
                            }
                        }
                    }
                    
                } else {
                    let alertFail = UIAlertController(title: "", message: "sorry, status data shop is failed, try again later or please contact us", preferredStyle: .alert)
                    let okFail = UIAlertAction(title: "Try Again", style: .default) { (action) in
                        self.getDataProductAndShop()
                        self.getTokenGuest()
                    }
                    alertFail.addAction(okFail)
                    self.present(alertFail, animated: false, completion: nil)
                }
                
            } else {
                let alertFail = UIAlertController(title: "", message: "sorry shop list networking fail, try again or please contact us, thank you", preferredStyle: .alert)
                let okFail = UIAlertAction(title: "try Again", style: .default) { (action) in
                    self.getDataProductAndShop()
                }
                let cancel = UIAlertAction(title: "cancel", style: .default)
                alertFail.addAction(cancel)
                alertFail.addAction(okFail)
                self.present(alertFail, animated: false, completion: nil)
                
            }
        }
        
        // ALL PRODUCT LIST
        APIBisanara.shared.getListProduct(limit: 12, page: self.page, keyword: self.searchText ?? "", sort: self.sort, category: self.sendCategory) { (status, response) in
            if response != nil {
                if response!["status"].stringValue == "Success" {
                    
                    // parsing data from json
                    let json = JSON(response!)
                    let dataDictionary = json["data"].dictionaryValue
                                        
                    if let array = dataDictionary["data"]?.arrayValue {
                        print("count product: \(array.count)")
                        for data in array {
                            
                            let product = ProductData(response: data)
                            self.products.append(product)
                            DispatchQueue.main.async {
                                self.productCollectionView.reloadData()
                            }
                        }
                    }
                    
                } else {
                    let alertFail = UIAlertController(title: "", message: "Sorry, status data product is failed or not success, try again later or please kindly contact us", preferredStyle: .alert)
                    let okFail = UIAlertAction(title: "Try Again", style: .default) { (action) in
                        self.getDataProductAndShop()
                        self.getTokenGuest()
                    }
                    alertFail.addAction(okFail)
                    self.present(alertFail, animated: false, completion: nil)
                }
               
            } else {
                let alertFail = UIAlertController(title: "", message: "sorry product list networking fail, try again or please contact us, thank you", preferredStyle: .alert)
                let okFail = UIAlertAction(title: "try Again", style: .default) { (action) in
                    self.getDataProductAndShop()
                }
                let cancel = UIAlertAction(title: "cancel", style: .default)
                alertFail.addAction(cancel)
                alertFail.addAction(okFail)
                self.present(alertFail, animated: false, completion: nil)
                
            }
        }
        
        // ALL MOST VIEWS LIST
        APIBisanara.shared.getListMostViewProduct(keyword: self.searchText ?? "", sort: self.sort, category: self.sendCategory) { (status, response) in
            if response != nil {
                if response!["status"].stringValue == "Success" {
                    
                    // parsing data from json
                    let json = JSON(response!)
                    let dataDictionary = json["data"].dictionaryValue
                                        
                    if let array = dataDictionary["data"]?.arrayValue {
                        print("count most view: \(array.count)")
                        for data in array {
                            
                            let product = ProductData(response: data)
                            self.mostViews.append(product)
                            DispatchQueue.main.async {
                                self.mostViewCollectionView.reloadData()
                            }
                        }
                    }
                    
                } else {
                    let alertFail = UIAlertController(title: "", message: "Sorry, status data most views is failed or not success, try again later or please kindly contact us", preferredStyle: .alert)
                    let okFail = UIAlertAction(title: "Try Again", style: .default) { (action) in
                        self.getDataProductAndShop()
                        self.getTokenGuest()
                    }
                    alertFail.addAction(okFail)
                    self.present(alertFail, animated: false, completion: nil)
                }
               
            } else {
                let alertFail = UIAlertController(title: "", message: "sorry most views list networking fail, try again or please contact us, thank you", preferredStyle: .alert)
                let okFail = UIAlertAction(title: "try Again", style: .default) { (action) in
                    self.getDataProductAndShop()
                }
                let cancel = UIAlertAction(title: "cancel", style: .default)
                alertFail.addAction(cancel)
                alertFail.addAction(okFail)
                self.present(alertFail, animated: false, completion: nil)
                
            }
        }
    }
    
    func customNavBar() {
        navigationController?.navigationBar.barTintColor = blue // nav bar background color
        
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white] // normal title color
        navigationController?.navigationBar.tintColor = .white // item tint color
        
        title = "Bisanara"
        // left title
        self.view.backgroundColor = blue
    }
    
    private func hideBackBar() {
        self.navigationItem.hidesBackButton = true
    }
    
    func searchBarCustom() {
        navigationItem.titleView = searchBar
        searchBar.placeholder = "Search any product or shop"
        searchBar.searchTextField.backgroundColor = .white // for the cursor color
    }
    
    func setTextFieldTintColor(to color: UIColor, for view: UIView) {
        if view is UITextField {
            view.tintColor = color
        }
        for subview in view.subviews {
            setTextFieldTintColor(to: color, for: subview)
        }
    }
    
    func startTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(changeCover), userInfo: nil, repeats: true)
    }
    
    @objc func changeCover() { // automatically change the image cover
        self.coverCollectionView.isPagingEnabled = false
        
        if currentCellIndex < covers.count - 1 {
            currentCellIndex += 1
            self.coverCollectionView.scrollToItem(at: IndexPath(item: currentCellIndex, section: 0), at: .centeredHorizontally, animated: true)
            coverPageControl.currentPage = currentCellIndex // moving the dot of page control
            
        } else {
            currentCellIndex = 0
            self.coverCollectionView.scrollToItem(at: IndexPath(item: currentCellIndex, section: 0), at: .centeredHorizontally, animated: false)
            coverPageControl.currentPage = currentCellIndex
        }
        
    }
    
    func setGradientBackground() {
        let colorTop =  UIColor(red: 0.0/255.0, green: 159.0/255.0, blue: 253.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 42.0/255.0, green: 42.0/255.0, blue: 114.0/255.0, alpha: 1.0).cgColor
                    
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
//        gradientLayer.frame = self.view.bounds
                
//        self.view.layer.insertSublayer(gradientLayer, at:0)
    }

    private func registerCells() { // register each collectionview
        productCollectionView.register(UINib(nibName: ProductCollectionViewCell.identifier, bundle: nil), forCellWithReuseIdentifier: ProductCollectionViewCell.identifier)
        categoryCollectionView.register(UINib(nibName: CategoryCollectionViewCell.identifier, bundle: nil), forCellWithReuseIdentifier: CategoryCollectionViewCell.identifier)
        shopCollectionView.register(UINib(nibName: ShopCollectionViewCell.identifier, bundle: nil), forCellWithReuseIdentifier: ShopCollectionViewCell.identifier)
        mostViewCollectionView.register(UINib(nibName: ProductCollectionViewCell.identifier, bundle: nil), forCellWithReuseIdentifier: ProductCollectionViewCell.identifier)
        coverCollectionView.register(UINib(nibName: CoverCollectionViewCell.identifier, bundle: nil), forCellWithReuseIdentifier: CoverCollectionViewCell.identifier)
    }
    
    
    @IBAction func settingsTapped(_ sender: Any) {
        let vc = UIStoryboard(name: "Setting", bundle: nil).instantiateViewController(withIdentifier: "SettingsViewController") as? SettingsViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    //hidden
    @IBAction func seeAllCategoryTapped(_ sender: Any) {
        let vc = UIStoryboard(name: "HomeTab", bundle: nil).instantiateViewController(withIdentifier: "AllCategoryViewController") as? AllCategoryViewController
        vc?.categories = self.categories
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func seeAllMostViewTapped(_ sender: Any) {
        let vc = UIStoryboard(name: "ProductView", bundle: nil).instantiateViewController(withIdentifier: "AllProductViewController") as? AllProductViewController
        vc?.isMostViews = true
        UserDefaults.standard.removeObject(forKey: "sortProduct")
        UserDefaults.standard.removeObject(forKey: "parentCategoryTxt")
        UserDefaults.standard.removeObject(forKey: "subCategoryTxt")
        UserDefaults.standard.removeObject(forKey: "idParentCategory")
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func seeAllProductTapped(_ sender: Any) {
        let vc = UIStoryboard(name: "ProductView", bundle: nil).instantiateViewController(withIdentifier: "AllProductViewController") as? AllProductViewController
        vc?.isAllProduct = true
        UserDefaults.standard.removeObject(forKey: "sortProduct")
        UserDefaults.standard.removeObject(forKey: "parentCategoryTxt")
        UserDefaults.standard.removeObject(forKey: "subCategoryTxt")
        UserDefaults.standard.removeObject(forKey: "idParentCategory")
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func seeAllShopTapped(_ sender: Any) {
        let vc = UIStoryboard(name: "HomeTab", bundle: nil).instantiateViewController(withIdentifier: "AllShopViewController") as? AllShopViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch collectionView {
        case mostViewCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductCollectionViewCell.identifier, for: indexPath) as! ProductCollectionViewCell
            cell.setup(product: mostViews[indexPath.row])
            return cell
            
        case productCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductCollectionViewCell.identifier, for: indexPath) as! ProductCollectionViewCell
            cell.setup(product: products[indexPath.row])
            return cell
            
        case categoryCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CategoryCollectionViewCell.identifier, for: indexPath) as! CategoryCollectionViewCell
            cell.setup(category: categories[indexPath.row])
            return cell
            
        case shopCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ShopCollectionViewCell.identifier, for: indexPath) as! ShopCollectionViewCell
            cell.setup(shop: shops[indexPath.row])
            return cell
            
        case coverCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CoverCollectionViewCell.identifier, for: indexPath) as! CoverCollectionViewCell
            cell.setup(cover: covers[indexPath.row])
            return cell
        default: return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case mostViewCollectionView:
            return mostViews.count
        case productCollectionView:
            return products.count
        case categoryCollectionView:
            return categories.count
        case shopCollectionView:
            return shops.count
        case coverCollectionView:
            return covers.count
        default: return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == categoryCollectionView {
            let vc = UIStoryboard(name: "HomeTab", bundle: nil).instantiateViewController(withIdentifier: "AllCategoryViewController") as? AllCategoryViewController
            vc?.idCategoryParent = self.categories[indexPath.row].id ?? 0
            vc?.title = categories[indexPath.row].name ?? ""
            self.navigationController?.pushViewController(vc!, animated: true)
            
        } else if collectionView == shopCollectionView {
            let controller = ShopDetailViewController.instantiateFromShopStoryboard()
            controller.shopSlug = shops[indexPath.row].slug ?? ""
            print("shop slug: \(shops[indexPath.row].slug ?? "")")
            navigationController?.pushViewController(controller, animated: true)
            
        } else if collectionView == productCollectionView {
            let controller = ProductDetailViewController.instantiateFromProductStoryboard()
            controller.productSlug = products[indexPath.row].slug ?? ""
            debugPrint("slugg: \(controller.productSlug)")
            navigationController?.pushViewController(controller, animated: true)
            
        } else if collectionView == mostViewCollectionView {
            let controller = ProductDetailViewController.instantiateFromProductStoryboard()
            controller.productSlug = mostViews[indexPath.row].slug ?? ""
            navigationController?.pushViewController(controller, animated: true)
        }
    }
}
