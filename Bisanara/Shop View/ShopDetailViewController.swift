//
//  ShopDetailViewController.swift
//  Bisanara
//
//  Created by Muhammad Syabran on 06/10/21.
//

import UIKit
import SwiftyJSON
import Kingfisher

// SHOP HOME
class ShopDetailViewController: UIViewController {

    @IBOutlet weak var bannerShopImage: UIImageView!
    @IBOutlet weak var bannerBackgroundView: UIView!
    
    @IBOutlet weak var generalView: UIView!
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var logoShopImageView: UIImageView!
    @IBOutlet weak var nameShopLbl: UILabel!
    @IBOutlet weak var numberShopLbl: UILabel!
    @IBOutlet weak var emailShopLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var descShopLbl: UILabel!
    
    @IBOutlet weak var sosmedViewStackView: UIStackView!
    @IBOutlet weak var sosmedView: UIView!
    @IBOutlet weak var listSosmedStackView: UIStackView!
    @IBOutlet weak var noneSosmedView: UIView!
    
    @IBOutlet weak var productsView: UIView!
    @IBOutlet weak var productListCollectionView: UICollectionView!
    @IBOutlet weak var noneProductView: UIView!
    
    var shopSlug = ""
    var shopId = ""
    
    var products: [ProductData] = []
    var arraySosmed = [Sosmed]()
    
    var blue = UIColor(red: 73.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customNavBar()
        customView()
        getShopFromAPI()
        getProductByShopSlug()
        setGradientBackground()
        
        productListCollectionView.register(UINib(nibName: ProductCardCollectionViewCell.identifier, bundle: nil), forCellWithReuseIdentifier: ProductCardCollectionViewCell.identifier)
    }
    
    func customView() {
        bannerShopImage.layer.borderColor = UIColor.lightGray.cgColor
        bannerShopImage.layer.borderWidth = 1
        detailView.layer.borderColor = UIColor.gray.cgColor
        detailView.layer.borderWidth = 1
        detailView.layer.cornerRadius = 10
        sosmedViewStackView.layer.borderColor = UIColor.lightGray.cgColor
        sosmedViewStackView.layer.borderWidth = 1
    }
    
    func setGradientBackground() {
        let colorTop =  UIColor(red: 0.0/255.0, green: 73.0/255.0, blue: 141.0/255.0, alpha: 1.0).cgColor
        let colorMid =  UIColor(red: 0.0/255.0, green: 38.0/255.0, blue: 77.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 0.0/255.0, green: 23.0/255.0, blue: 45.0/255.0, alpha: 1.0).cgColor
                    
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorMid, colorBottom]
        gradientLayer.locations = [0.0, 0.5, 1.0]
        gradientLayer.frame = self.bannerBackgroundView.bounds
                
        self.bannerBackgroundView.layer.insertSublayer(gradientLayer, at:0)
    }
    
    private func getShopFromAPI() {
        APIBisanara.shared.getDetailShop(slug: self.shopSlug) { (status, response) in
            let json = JSON(response!)
            
            if json["status"].stringValue == "success" {
                let shop = ShopData(response: json["data"])
                
                self.shopId = shop.id_shop ?? ""
                self.nameShopLbl.text = shop.name ?? ""
                self.numberShopLbl.text = shop.phone ?? ""
                if self.numberShopLbl.text == "" {
                    self.numberShopLbl.text = "-"
                }
                self.emailShopLbl.text = shop.email ?? ""
                if self.emailShopLbl.text == "" {
                    self.emailShopLbl.text = "-"
                }
                self.descShopLbl.text = (shop.description ?? "").trimHTMLTags()
                self.descShopLbl.numberOfLines = 0
                if self.descShopLbl.text == "" {
                    self.descShopLbl.text = "-"
                    self.descShopLbl.numberOfLines = 1
                }
                self.addressLbl.text = shop.address ?? ""
                if self.addressLbl.text == "" {
                    self.addressLbl.text = "-"
                }
                
                // get img with kingfisher
                let logo = shop.logo ?? ""
                self.logoShopImageView.kf.setImage(with: logo.asUrl, placeholder: UIImage(named: "icon-shop"))
                
                let banner = shop.banner ?? ""
//                self.bannerShopImage.urlLoad(urlString: banner)
                self.bannerShopImage.kf.setImage(with: banner.asUrl)
                
                let data = json["data"].dictionaryValue
                
                // get social media data
                if let socialMedia = data["social_media"]?.arrayValue {
                    if socialMedia.isEmpty == false {
                        for data in socialMedia {
                            let sosmed = ShopSosmed(response: data)
                            
                            DispatchQueue.main.async {
                                self.listSosmedStackView.reloadInputViews()

                                if let card = UINib(nibName: String(describing: PlatformUrl.self), bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? PlatformUrl {

                                    self.listSosmedStackView.addArrangedSubview(card)

                                    if sosmed.name == "facebook" {
                                        let img = UIImage(named: "facebook")
                                        card.platformImage.setBackgroundImage(img, for: .normal)
                                    } else if sosmed.name == "instagram" {
                                        let img = UIImage(named: "instagram")
                                        card.platformImage.setBackgroundImage(img, for: .normal)
                                    } else if sosmed.name == "twitter" {
                                        let img = UIImage(named: "twitter")
                                        card.platformImage.setBackgroundImage(img, for: .normal)
                                    } else if sosmed.name == "line" {
                                        let img = UIImage(named: "line")
                                        card.platformImage.setBackgroundImage(img, for: .normal)
                                    } else if sosmed.name == "whatsapp" || sosmed.name == "whatsapps" {
                                        let img = UIImage(named: "whatsapp")
                                        card.platformImage.setBackgroundImage(img, for: .normal)
                                    }
                                    card.url = sosmed.url ?? ""

                                    card.platformImage.layer.cornerRadius = 5
                                }
                                self.noneSosmedView.isHidden = true
                            }
                        }
                    } else {
                        self.noneSosmedView.isHidden = false
                        self.sosmedView.isHidden = true
                    }
                }
            }
        }
    }
    
    private func getProductByShopSlug() {
        APIBisanara.shared.getListProductByShop(shopSlug: self.shopSlug) { (status, response) in
            if response != nil {
                if response!["status"].stringValue == "Success" {
                    
                    // parsong data from json
                    let json = JSON(response!)
                    let array = json["data"].arrayValue
                    if array.isEmpty == false {
                        self.productsView.isHidden = false
                        self.noneProductView.isHidden = true
                        
                        for data in array {
                            
                            let product = ProductData(response: data)
                            self.products.append(product)
                            DispatchQueue.main.async {
                                self.productListCollectionView.reloadData()
                            }
                        }
                    } else {
                        self.productsView.isHidden = true
                        self.noneProductView.isHidden = false
                    }
                }
            }
        }
    }
    
    func customNavBar() {
        title = "Shop"
        self.view.backgroundColor = blue
        self.navigationItem.largeTitleDisplayMode = .never
    }
    
    @IBAction func seeAllTapped(_ sender: Any) {
        let vc = UIStoryboard(name: "ProductView", bundle: nil).instantiateViewController(withIdentifier: "AllProductViewController") as? AllProductViewController
        vc?.isShopProduct = true
        vc?.shopSlug = self.shopSlug
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}

extension ShopDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductCardCollectionViewCell.identifier, for: indexPath) as! ProductCardCollectionViewCell
        cell.setup(product: products[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let controller = ProductDetailViewController.instantiateFromProductStoryboard()
        controller.productSlug = products[indexPath.row].slug ?? ""
        navigationController?.pushViewController(controller, animated: true)
    }
}
